from django.conf.urls import patterns, include, url

urlpatterns = patterns('validator.views',
    url(
        r"^validate/$",
        "validate", name='validate'
        ),
)
