import json

from django.views.generic import TemplateView, View
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User


class Validate(View):
    def get(self, request):
        fieldId = request.GET.get("fieldId")
        fieldValue = request.GET.get("fieldValue")

        if fieldId == "id_username" and User.objects.filter(username=fieldValue).exists():
            return HttpResponse(json.dumps([fieldId, False]),
                content_type="application/javascript")

        if fieldId == "id_email" and User.objects.filter(email=fieldValue).exists():
            return HttpResponse(json.dumps([fieldId, False]),
                content_type="application/javascript")

        else:
            return HttpResponse(json.dumps([fieldId, True]),
                content_type="application/javascript")

validate = Validate.as_view()
