#coding: UTF-8
import json

from django.views.generic import View, ListView
from django.shortcuts import render, get_object_or_404
from django.http import Http404, HttpResponse
from django.contrib.auth.models import User
from django.db.models import Q

from core.utils import json_defaults, get_or_none, DiggPaginator, QuerySetDiggPaginator
from core.models import Banner

from expert.models import Issue

from models import TargetQA, OrganizationOrder
from django.core.context_processors import request


class TargetGroupDetails(View):

    def get(self, request, group_id):

        issue = get_object_or_404(Issue, id=group_id)
        if issue.is_root_node():
            title = u''
            if group_id == 1:
                title = u'Помощь психолога для родителей'
            if group_id == 2:
                title = u'Психолог для взрослых'
            if group_id == 3:
                title = u'Психологическая помощь подросткам, детям'
            if group_id == 4:
                title = u'Семейный психолог. Консультация семейного психолога'
            if group_id == 5:
                title = u'Психолог в организации'
            return render(
                request, "target_group__details.html",
                {
                    'title': title,
                    'group':issue,
                    'questions': TargetQA.objects.filter(
                    Q(type__parent__parent=issue) | 
                    Q(type__parent=issue) | Q(type=issue)
                    ).order_by('-datetime')[:6],
                    'left_banner': get_or_none(Banner, path=request.path, place='L'),
                    'right_banner': get_or_none(Banner, path=request.path, place='R')
                }
                )
        else:
            raise Http404

    def post(self, request, group_id):
        msg = u'Ваше сообщение отправлено'
        if request.POST.get('email', False):
            oo = OrganizationOrder(
                email = request.POST.get('email')
            )
            oo.country = request.POST.get('country', '')
            oo.city = request.POST.get('city', '')
            oo.company = request.POST.get('company', '')
            oo.contact = request.POST.get('contact_person', '')
            oo.msg = request.POST.get('message', '')
            oo.save()
        else:
            msg = u'Произошли ошибки во время отправки заявки'
        issue = get_object_or_404(Issue, id=group_id)
        if issue.is_root_node():
            return render(request, "target_group__details.html", {
                'group':issue,
                'post_msg': msg,
                'left_banner': get_or_none(Banner, path=request.path, place='L'),
                'right_banner': get_or_none(Banner, path=request.path, place='R')
            })

class TargetGroupQuestions(ListView):
    context_object_name = 'questions'
    paginate_by = 5
    template_name = "target_group__question_list.html"

    def get_queryset(self, *args, **kwargs):
        issue = get_object_or_404(Issue, id=int(self.args[0]))
        return TargetQA.objects.filter(Q(type=issue) | Q(type__parent=issue))

    def get_context_data(self, **kwargs):
        ctx = super(TargetGroupQuestions, self).get_context_data(**kwargs)
        ctx['group'] = Issue.objects.get(id=int(self.args[0]))
        return ctx

#class TargetGroupQuestions(View):
#
#    def get(self, request, issue_id):
#        issue = get_object_or_404(Issue, id=issue_id)
#        return HttpResponse(json.dumps(
#            TargetQA.objects.filter(Q(type=issue) | Q(type__parent=issue)),
#            default=json_defaults),
#            content_type="application/javascript"
#            )


class TargetGroupExperts(ListView):

    #@todo: optimize get_queryset

    context_object_name = "experts"
    template_name = "target_group_experts.html"
    paginate_by = 12
    paginator_class = QuerySetDiggPaginator

    def get_paginator(self, queryset, per_page, orphans=0, allow_empty_first_page=True):
        return QuerySetDiggPaginator(queryset, per_page, padding=3, body=7, tail=1)

    def get_queryset(self, *args, **kwargs):
        category = int(self.args[0])
        return User.objects.filter(Q(expertblank__issues=category) |
            Q(expertblank__issues__parent=category) |
            Q(expertblank__issues__parent__parent=category)
            ).filter(expertblank__is_tested=False).distinct("username")

    def get_context_data(self, *args, **kwargs):
        context = super(TargetGroupExperts, self).get_context_data(**kwargs)
        context['group'] = Issue.objects.get(id=int(self.args[0]))
        return context


class QuestionsDetails(View):

    def get(self, request, question_id):
        question = get_object_or_404(TargetQA, id=question_id)
        return render(
            request, "target_group__question_detalis.html", 
            {
                "question": question,
                "group": question.type
            })


target_group_details = TargetGroupDetails.as_view()
target_group_question_list = TargetGroupQuestions.as_view()
target_group_expert_list = TargetGroupExperts.as_view()
question_details = QuestionsDetails.as_view()
