#coding: UTF-8
from django.contrib.admin import site, ModelAdmin

from django.contrib import admin
from models import TargetQA, OrganizationOrder

from forms import TargetQAOverrideForm


class TargetQAAdmin(ModelAdmin):

    form = TargetQAOverrideForm
    list_display = ('expert',)


admin.site.register(TargetQA, TargetQAAdmin)
admin.site.register(OrganizationOrder)
