#coding: UTF-8
from django.db import models
from django.contrib.auth.models import User
from django.core.mail import send_mail
import django_settings
from djangosphinx.models import SphinxSearch

from core.utils import JsonMixin

from expert.models import Issue

# Create your models here.

class OrganizationOrder(models.Model):
    """
    Заявка со страницы Организациям
    """
    country = models.CharField(max_length=10, verbose_name=u'Страна', null=True, blank=True)
    city = models.CharField(max_length=100, verbose_name=u'Город', null=True, blank=True)
    company = models.CharField(max_length=300, verbose_name=u'Название компании', null=True, blank=True)
    contact = models.CharField(max_length=300, verbose_name=u'Контактное лицо', null=True, blank=True)
    email = models.EmailField(max_length=300, verbose_name=u'Электронная почта')
    msg = models.TextField(verbose_name=u'Сообщение', null=True, blank=True)

    def save(self, *args, **kwargs):
        super(OrganizationOrder, self).save(*args, **kwargs)
        email = django_settings.get('AdminEmail')
        msg = u'Страна: %s\nГород: %s\nКомпания: %s\nКонтактное лицо: %s\nEmail: %s\nСообщение: %s'%(self.country,
            self.city,
            self.company,
            self.contact,
            self.email,
            self.msg)
        send_mail(u'Заявка со страницы Организациям', msg, 'info@localhost', [email,])

    def __unicode__(self):
        return u'Заявка по страницы Организациям #%d'%self.pk

    class Meta:
        verbose_name = u'Заявка со страницы Организациям'
        verbose_name_plural = u'Заявки со страницы Организациям'

class TargetQA(models.Model, JsonMixin):

    """
    Вопрос-ответ для таргет группы
    """
    expert = models.ForeignKey(User, verbose_name=u'эксперт')
    type = models.ForeignKey(
        Issue, verbose_name=u'категория вопроса', null=True, blank=True)
    datetime = models.DateTimeField(verbose_name=u'время')
    question = models.TextField(verbose_name=u'текст вопроса')
    answer = models.TextField(verbose_name=u'текст ответа')

    search = SphinxSearch(
                          index="questions",
                          weights={
                                   'question': 100,
                                   'answer': 50
                                  }
                          )
