from django.conf.urls import patterns, include, url


urlpatterns = patterns('target.views',
    url("^questions/(\d+)/$", "question_details", name='question_details'),
    url("^(\d+)/$", "target_group_details", name='target_group_details'),
    url("^(\d+)/questions/$", "target_group_question_list",
        name='target_group_question_list'),
    url("^(\d+)/experts/$", "target_group_expert_list",
        name='target_group_expert_list'),
)
