
from django.contrib.admin import site, ModelAdmin, StackedInline, TabularInline

from models import (
    ExpertBlank, Issue, Institution, StudyCenter, ExpertQuestion, ExpertAnswer,
    Association, Course, Consultation, Recal,
    Specialization, ModerateInstitution, ModerateLoginData, ModerateIssues, WorkDay, WorkHour, PersonalCard, ModeratePrivateData,
    DiplomScan)

from forms import ExpertBlankOverrideForm

from mptt.admin import MPTTModelAdmin
from feincms.admin.tree_editor import TreeEditor, ajax_editable_boolean

class SpecializationAdmin(ModelAdmin):

    model = Specialization
    list_display = ("name",)


class CourseInline(StackedInline):
    model = Course
    extra = 1


class InstitutionInline(StackedInline):
    model = Institution
    inlines = [CourseInline,]
    extra = 1

class StudycenterInline(StackedInline):

    model = StudyCenter
    inlines = [CourseInline,]
    extra = 1

class AssociationInline(StackedInline):

    model = Association
    extra = 1

class ExpertBlankAdmin(ModelAdmin):

    form = ExpertBlankOverrideForm
    save_on_top = True
    list_display = ("username","last_name", "first_name", "middle_name",'approved','sex')
    search_fields = ("username", "last_name", "first_name", "middle_name",)
    exclude = ('expert', 'approved', )
    inlines = [InstitutionInline, StudycenterInline, AssociationInline]
    filter_horizontal = ('issues','scans')

    class Media:
        css = {'all': ('resize_issues_widget.css',)}


class RecalAdmin(ModelAdmin):

    list_display = ('expert',)


class IssueAdmin(ModelAdmin):

    list_display = ('name',)


class MyTreeEditorAdmin(TreeEditor):
    active_toggle = ajax_editable_boolean('active', 'active')


class ConsultationAdmin(ModelAdmin):

    list_display = ("expert", "client") 

class ModerateInstitutionAdmin(ModelAdmin):
    list_display = ('user', 'approved')
    fields = ['user', 'approved']

class WorkHourAdmin(StackedInline):
    model= WorkDay.workperoids.through

class WorkDayAdmin(ModelAdmin):
    model = WorkDay
#    filter_horizontal = ['workperoids',]
#    inlines = [WorkHourAdmin]
#    fields = ['oneday', 'workperoids']

site.register(ExpertBlank, ExpertBlankAdmin)
site.register(Issue, MyTreeEditorAdmin)
site.register(Consultation, ConsultationAdmin)
site.register(Recal, RecalAdmin)
site.register(ModerateInstitution, ModerateInstitutionAdmin)
site.register(ModerateLoginData, ModerateInstitutionAdmin)
site.register(ModerateIssues, ModerateInstitutionAdmin)
site.register(ModeratePrivateData)
site.register(Specialization, SpecializationAdmin)
site.register(WorkDay, WorkDayAdmin)
#site.register(WorkHour)
site.register(PersonalCard)
site.register(DiplomScan)
site.register(ExpertQuestion)
site.register(ExpertAnswer)
