#coding: UTF-8
import random
import string
import datetime

from django.contrib.auth.models import User
from django.core.files.base import ContentFile
from django.db import models
from django.core.cache import cache
from django.conf import settings
from pybb.models import TZ_CHOICES
from sorl.thumbnail.fields import ImageField
from ckeditor.fields import RichTextField

from choices import (
    SPECIALIZATIONS, DEGREES, MEMBERSHEEP_STATUS, CITIES, COUNTRIES,
    CONSULTATION_TYPES, CONSULTATION_STATUSES
    )

from issues import Issue
from times import TimeGap, TimeTable

class WorkHour(models.Model):
    start_time = models.TimeField(verbose_name=u'Начало рабочего периода')
    end_time = models.TimeField(verbose_name=u'Конец рабочего периода')

    def is_allday(self):
        return True if self.start_time == datetime.time(0,0) and self.end_time == datetime.time(23,59) else False

    def __unicode__(self):
        return u'{} -> {}'.format(self.start_time, self.end_time)

DAY_OF_WEEK = (
    (0, u'Понедельник'),
    (1, u'Вторник'),
    (2, u'Среда'),
    (3, u'Четверг'),
    (4, u'Пятница'),
    (5, u'Суббота'),
    (6, u'Воскресенье'),
)

class WorkDay(models.Model):
    workperoids = models.ManyToManyField(WorkHour, verbose_name=u'Рабочие часы')
    user = models.ForeignKey(User, verbose_name=u'Пользователь')
    week = models.IntegerField(verbose_name=u'Неделя года', blank=True, null=True)
    dayofweek = models.IntegerField(choices=DAY_OF_WEEK, verbose_name=u'День недели', blank=True, null=True)
    oneday = models.DateField(verbose_name=u'Конкретный день', blank=True, null=True)
    updated = models.DateTimeField(verbose_name=u'Последнее обновление', auto_now=True)
    holliday = models.BooleanField(default=False, verbose_name=u'Выходной день')

    def sort_workperoids(self):
        intervals = []
        for wp in self.workperoids.order_by('start_time'):
            if not intervals:
                intervals.append([wp.start_time, wp.end_time])
            else:
                if wp.start_time < intervals[-1][1] < wp.end_time or wp.start_time == intervals[-1][1]:
                    intervals[-1][1] = wp.end_time
                else:
                    if wp.start_time > intervals[-1][1]:
                        intervals.append([wp.start_time, wp.end_time])
        self.workperoids.clear()
        for interval in intervals:
            wh,cr = WorkHour.objects.get_or_create(
                start_time = interval[0],
                end_time=interval[1]
            )
            self.workperoids.add(wh)
        self.save()

    @classmethod
    def get_periods(cls, user, date=None, week=None, dow=None):
        if date:
            wds = cls.objects.filter(user=user).filter(
                models.Q(oneday=date)|models.Q(dayofweek=date.weekday())|models.Q(week=int(date.strftime('%W')))
            ).order_by('-updated')
            if wds.count() > 0:
                return wds[0].workperoids.all()
        if week:
            wds = cls.objects.filter(user=user).filter(models.Q(week=week)).order_by('-updated')
            if wds.count() > 0:
                return wds[0].workperoids.all()
        if dow != None:
            wds = cls.objects.filter(user=user).filter(models.Q(dayofweek=dow)).order_by('-updated')
            if wds.count() > 0:
                return wds[0].workperoids.all()
        return cls.objects.none()

    @classmethod
    def get_wd(cls, user, date=None, week=None, dow=None):
        if date:
            wds = cls.objects.filter(user=user).filter(
                models.Q(oneday=date)|models.Q(dayofweek=date.weekday())|models.Q(week=int(date.strftime('%W')))
            ).order_by('-updated')
            if wds.count() > 0:
                return wds[0]
        if week:
            wds = cls.objects.filter(user=user).filter(models.Q(week=week)).order_by('-updated')
            if wds.count() > 0:
                return wds[0]
        if dow:
            wds = cls.objects.filter(user=user).filter(models.Q(dayofweek=dow)).order_by('-updated')
            if wds.count() > 0:
                return wds[0]
        return cls.objects.none()

    @classmethod
    def is_worked(cls, user, date):
        wds = cls.objects.filter(user=user).filter(
            models.Q(oneday=date)|models.Q(dayofweek=date.weekday())|models.Q(week=int(date.strftime('%W')))
        ).order_by('-updated')
        if wds.count() > 0:
            return not wds[0].holliday
        return True

    @classmethod
    def is_worked_in_morning(cls, user, date, is_holiday, is_workday):
        rt = False
        if WorkDay.is_worked(user, date):
            if WorkDay.get_periods(user, date).count() > 0:
                if WorkDay.get_periods(user, date).filter(
                    start_time__lt=datetime.time(14, 00)
                ).count() > 0:
                    rt = True
            else:
                rt = True
        if is_holiday ^ is_workday:
            if is_holiday:
                if date.weekday() not in [5,6]:
                    rt = False
            if is_workday:
                if date.weekday() not in xrange(0, 5):
                    rt = False
        return rt

    @classmethod
    def is_worked_in_day(cls, user, date, is_holiday, is_workday):
        rt = False
        if WorkDay.is_worked(user, date):
            if WorkDay.get_periods(user, date).count() > 0:
                if WorkDay.get_periods(user, date).filter(
                    start_time__gte=datetime.time(14, 00),
                    start_time__lt=datetime.time(19, 00)
                ).count() > 0:
                    rt = True
            else:
                rt = True
        if is_holiday ^ is_workday:
            if is_holiday:
                if date.weekday() not in [5,6]:
                    rt = False
            if is_workday:
                if date.weekday() not in xrange(0, 5):
                    rt = False
        return rt

    @classmethod
    def is_worked_in_evening(cls, user, date, is_holiday, is_workday):
        rt = False
        if WorkDay.is_worked(user, date):
            if WorkDay.get_periods(user, date).count() > 0:
                if WorkDay.get_periods(user, date).filter(
                    start_time__gte=datetime.time(19, 00)
                ).count() > 0:
                    rt = True
            else:
                rt = True
        if is_holiday ^ is_workday:
            if is_holiday:
                if date.weekday() not in [5,6]:
                    rt = False
            if is_workday:
                if date.weekday() not in xrange(0, 5):
                    rt = False
        return rt

    class Meta:
        verbose_name = u'Рабочий день'
        verbose_name_plural = u'Рабочие дни'

    def __unicode__(self):
        return u'Рабочий день пользователя {}'.format(self.user.username)

class Specialization(models.Model):

    class Meta:
        verbose_name = u"специализация эксперта"
        verbose_name_plural = u"специализации экспертов"

    name = models.CharField(
        verbose_name=u"название специализации", max_length=255)
    description = RichTextField(
        verbose_name=u"описание специализации")

    def __unicode__(self):
        return self.name


class Course(models.Model):
    """
    Вузовские доп. курсы
    """

    class Meta:
        verbose_name = u"вузовский курс"
        verbose_name_plural = u"вузовские курсы"

    institution = models.ForeignKey("Institution")

    course_date_start = models.DateField(
        verbose_name=u"дата начала")
    course_date_stop = models.DateField(
        verbose_name=u"дата завершения")

class Institution(models.Model):
    """
    Модель вуза
    """

    class Meta:
        verbose_name = u"учебное заведение"
        verbose_name_plural = u"учебные заведения"

    expertblank = models.ForeignKey("ExpertBlank")
    instatution_name = models.CharField(
        max_length=255,
        verbose_name=u"Высшее учебное заведение",
        help_text="МГУ им. М.В. Ломоносова"
    )
    institution_specialization = models.CharField(
        max_length=255,
        verbose_name=u"специальность",
        help_text="Педагогика и психология девиантного поведения")
    faculty = models.CharField(
        max_length=255,
        verbose_name=u"факультет",
        blank=True,
        help_text="Психологии")

    instatution_start = models.IntegerField(
        verbose_name=u"Год поступления",
        blank=True,
        null=True,
        help_text="1985")

    instatution_stop = models.IntegerField(
        verbose_name=u"Год окончания",
        blank=True,
        null=True,
        help_text="1991"
    )
    degree = models.CharField(
        max_length=255,
        choices=DEGREES,
        verbose_name=u"ученая степень",
        blank=True
    )

    @property
    def specialization(self):
        return self.institution_specialization

    def __unicode__(self):
        return self.instatution_name


class StudyCenter(models.Model):

    class Meta:
        verbose_name = u"центр подготовки"
        verbose_name_plural = u"центры подготовки"

    expertblank = models.ForeignKey("ExpertBlank")
    center_name = models.CharField(
        max_length=255,
        verbose_name=u"название учебного центра",
        blank=True
    )
    diploma_details = models.TextField(verbose_name=u"сведения о дипломе",
    blank=True)


class Association(models.Model):
    """
    Ассоциация экспертов
    """

    class Meta:
        verbose_name = u"ассоциация психологов"
        verbose_name_plural = u"ассоциации психологов"

    expertblank = models.ForeignKey("ExpertBlank")
    association_name = models.CharField(
        max_length=255, verbose_name=u"Название профессионального объединения",
        blank=True)
    status = models.CharField(
        max_length=255,
        choices=MEMBERSHEEP_STATUS, verbose_name=u"статус", blank=True)
import time
def make_upload_path(instance, filename):
    return 'upload/{}.{}'.format(time.time(), filename.split('.')[-1])

def make_upload_path_avatar(instance, filename):
    return u'avatars/{}.{}'.format(time.time(), filename.split('.')[-1])

class DiplomScan(models.Model):
    scan = models.ImageField(upload_to=make_upload_path, verbose_name=u'Скан диплома')
    text = models.TextField(verbose_name=u'Комментарий к диплому', blank=True, null=True)

    def get_url(self):
        return u'http://31.186.100.202/' + unicode(self.scan)

    class Meta:
        verbose_name = u'Скан диплома'
        verbose_name_plural = u'Сканы дипломов'

    def __unicode__(self):
        return u'Скан диплома #{}'.format(self.pk)

class DescriptionCharField(models.CharField):
    description = u''

    __metaclass__ = models.SubfieldBase

    def __init__(self, *args, **kwargs):
        self.description = kwargs.get('description', u'')
        super(DescriptionCharField, self).__init__(*args, **kwargs)

class ExpertBlank(models.Model):
    """
    Бланк, который заполняет экспертт при регистрации
    """

    class Meta:
        verbose_name = u"анкета эксперта"
        verbose_name_plural = u"анкеты экспертов"

    sex = models.BooleanField(verbose_name=u'Мужчина', default=True)
    free_consult = models.BooleanField(verbose_name=u'Проводит бесплатную консультацию', default=False)

    scans = models.ManyToManyField(DiplomScan, blank=True, null=True)

    is_tested = models.BooleanField(default=False, verbose_name=u'Тестовый специалист')
    send_me_q = models.BooleanField(default=False, verbose_name=u'Получать вопросы от клиентов')

    first_name = models.CharField(
        max_length=255,
        verbose_name=u"имя",
        help_text="Иван"
    )

    middle_name = models.CharField(
        max_length=255,
        verbose_name=u"отчество (при наличии)",
        help_text="Иванович",
        blank=True,
        null=True
    )

    last_name = models.CharField(
        max_length=255,
        verbose_name=u"фамилия",
        help_text="Иванов"
    )

    birthday = models.DateField(
        verbose_name=u"дата рождения",
#        input_formats=["%d.%m.%Y", ],
        help_text="25.03.1972",
        null=True
    )

    personal = models.TextField(
        verbose_name=u"""О себе""",
        help_text=u""
    )

    email = models.EmailField(
        verbose_name=u"электронная почта",
        help_text="example@example.com"
    )

    phone_number = models.CharField(
        max_length=20,
        verbose_name=u"номер телефона",
        help_text="+ 7 911 911 91 91")

    country = models.CharField(
        max_length=255, verbose_name=u"страна проживания",
#        blank=True,
        choices=COUNTRIES)

    fix = models.IntegerField(verbose_name=u'Ставка за консультацию', default=800,
    help_text=u'800')

    consultation_time = models.IntegerField(verbose_name=u'Продолжительность консультации (мин)', default=50,
    help_text=u'50')

    @property
    def country_by_code(self):
        return dict(COUNTRIES)[self.country]

    city = models.CharField(
        max_length=255, verbose_name=u"город проживания",
    help_text=u'Москва')

    def get_scans(self):
        return self.scans.distinct()

    @property
    def institutions(self):
        return self.institution_set.all()

    @property
    def studycenters(self):
        return self.studycenter_set.all()

    @property
    def associations(self):
        return self.association_set.all()

    issues = models.ManyToManyField(
        Issue, verbose_name=u"консультации по вопросам", help_text="")

    avatar = ImageField(
        upload_to=make_upload_path_avatar,
        verbose_name=u"фотография",
        help_text=u'Поддерживаются форматы jpeg, png - это основные форматы\
                изображений в интернете, размер одного загружаемого файла до 5 Mb.'
        )

    expert = models.OneToOneField(
        User, blank=True, null=True, verbose_name=u"эксперт")

    approved = models.BooleanField(
        default=False, verbose_name=u"одобрена")

    datetime = models.DateTimeField(auto_now=True, verbose_name=u'дата подачи')

    username = models.CharField(
        max_length=255,
        verbose_name=u"псевдоним",
        help_text=u'Ivan'
    )

    def last_seen(self):
        return cache.get('seen_{}'.format(self.expert.username))

    def online(self):
        if self.last_seen():
            now = datetime.datetime.now()
            if now > self.last_seen() + datetime.timedelta(
                seconds=settings.USER_ONLINE_TIMEOUT):
                return False
            else:
                return True
        else:
            return False

    def get_offset_for_adults(self):
        if self.for_parents():
            return 30
        else:
            return 0

    def get_offset_for_teenagers(self):
        if self.for_adults():
            return 30 + self.get_offset_for_adults()
        else:
            return self.get_offset_for_adults()

    def get_offset_for_families(self):
        if self.for_teenagers():
            return 30 + self.get_offset_for_teenagers()
        else:
            return self.get_offset_for_teenagers()

    def get_offset_for_organizations(self):
        if self.for_families():
            return 30 + self.get_offset_for_families()
        else:
            return self.get_offset_for_families()

    def for_parents(self):
        return True if self.issues.filter(id__in=[1,]).count() > 0 else False

    def for_adults(self):
        return True if self.issues.filter(id__in=[2,]).count() > 0 else False

    def for_teenagers(self):
        return True if self.issues.filter(id__in=[3,]).count() > 0 else False

    def for_families(self):
        return True if self.issues.filter(id__in=[4,]).count() > 0 else False

    def for_organizations(self):
        return True if self.issues.filter(id__in=[5,]).count() > 0 else False

    @property
    def slogan(self):
        return self.expert.profile.signature

    @property
    def age(self):
        import datetime
        if self.birthday > datetime.date.today().replace(year = self.birthday.year):
            return datetime.date.today().year - self.birthday.year - 1
        else:
            return datetime.date.today().year - self.birthday.year

    def __unicode__(self):
        return u"Aнкета эксперта"

#import gatekeeper
#gatekeeper.register(ExpertBlank, import_unmoderated=True)

RECAL_CHOICES = (
    (u'0', u'Отрицательный'),
    (u'1', u'Положительный'),
    (u'2', u'Нейтральный')
)

class Recal(models.Model):

    class Meta:
        verbose_name = u"отзыв об эксперте"
        verbose_name_plural = u"отзывы об экспертах"

    client = models.ForeignKey(
        User, verbose_name=u'клиент', related_name='recal_client')
    expert = models.ForeignKey(User, verbose_name=u'эксперт',
        related_name='recal_expert')

    text = models.TextField(verbose_name=u'текст отзыва')
    datetime = models.DateTimeField(auto_now=True)
    approved = models.BooleanField(
        default=True, verbose_name=u'виден на сайте')
    type = models.CharField(verbose_name=u'Тип отзыва', choices=RECAL_CHOICES, default=u'1', max_length=2)
    hide_author = models.BooleanField(default=False, verbose_name=u'Не выводить имя автора')

    @classmethod
    def is_permited_to_add_recal(cls, client, expert):
        """
        @todo: not imlemented
        """
        return True

    @property
    def photo(self):
        return self.expert.expertblank.avatar

    def __unicode__(self):
        return u'Отзыв об эксперте %s' % self.expert.last_name


class Consultation(models.Model):

    expert = models.ForeignKey(User, related_name="consultation_expert")
    client = models.ForeignKey(User, related_name="consultation_client")
    type_tag = models.CharField(
        max_length=255, choices=CONSULTATION_TYPES, blank=True)
    status = models.CharField(
        max_length=255, choices=CONSULTATION_STATUSES, default="assigned")

    issue = models.ForeignKey(Issue, null=True, blank=True)
    spend_time = models.IntegerField(default=0, verbose_name=u'Потраченное время консультации')
    stop_time = models.DateTimeField(blank=True, null=True, verbose_name=u'Время, когда консультация была остановлена')

    dtstart = models.DateTimeField(blank=True)
    length = models.IntegerField(default=0) #length in seconds
    secret_code = models.CharField(
    max_length=50, default="",
    blank=True, null=True
    )
    created = models.DateTimeField(u'Дата создания', default=datetime.datetime.now)
    comment = models.TextField(blank=True, null=True)

    def is_new_client(self):
        if Consultation.objects.filter(
            client=self.client,
            expert=self.expert,
            status='done'
        ).count () == 1:
            return True
        return False

    @classmethod
    def get_filter_by_date(cls, date=None, client=None, expert=None):
        c = cls.objects
        if client:
            c = c.filter(client=client)
        if expert:
            c = c.filter(expert=expert)
        min_date = datetime.datetime.combine(date, datetime.time.min)
        max_date = datetime.datetime.combine(date, datetime.time.max)

        c = c.filter(dtstart__range=(min_date, max_date))
        return c

    @classmethod
    def is_approved_by_day(cls, date=None, client=None, expert=None):
        c = cls.get_filter_by_date(date, client, expert)
        c = c.filter(status='approved')
        if c.count() > 0:
            return True
        return False

    @classmethod
    def is_canceled_by_day(cls, date=None, client=None, expert=None):
        c = cls.get_filter_by_date(date, client, expert)
        c = c.filter(status='canceled')
        if c.count() > 0:
            return True
        return False

    @classmethod
    def is_assigned_by_day(cls, date=None, client=None, expert=None):
        c = cls.get_filter_by_date(date, client, expert)
        c = c.filter(status='assigned')
        if c.count() > 0:
            return True
        return False

    @models.permalink
    def get_absolute_url(self):
        return (
                'expert.views.consultation_details',
                [self.secret_code]
                )

    def save(self, *args, **kwargs):
        if self.secret_code == "":
            self.generate_secret_code()
        super(Consultation, self).save(*args, **kwargs)

    def generate_secret_code(self):
        self.secret_code = ''.join(
        random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for x in range(50))

    @property
    def type(self):
        return dict(CONSULTATION_TYPES)[self.type_tag]

from django.db.models.signals import m2m_changed
def parent_add_to_m2m(sender, instance, action, **kw):
    if action == "post_add":
        iss_id_list = [issue.pk for issue in instance.issues.all()]
        for iss in instance.issues.all():
            if iss.parent and iss.parent.pk not in iss_id_list:
                instance.issues.add(iss.parent)
            if not iss.parent:
                child_ids = [c.pk for c in iss.children.all()]
                if not set(child_ids)&set(iss_id_list):
                    instance.issues.remove(iss)

m2m_changed.connect(parent_add_to_m2m, sender=ExpertBlank.issues.through)

from django.core import urlresolvers
from django.contrib.contenttypes.models import ContentType

import json
class ModerateInstitution(models.Model):
    approved = models.BooleanField(default=False, verbose_name=u'Принять')
    user = models.ForeignKey(User)
    institution_updated_json = models.TextField(blank=True, null=True)
    institution_added_json = models.TextField(blank=True, null=True)

    studycenter_updated_json = models.TextField(blank=True, null=True)
    studycenter_added_json = models.TextField(blank=True, null=True)

    association_updated_json = models.TextField(blank=True, null=True)
    association_added_json = models.TextField(blank=True, null=True)

    info = models.TextField(blank=True, null=True)

    def get_admin_url(self):
        content_type = ContentType.objects.get_for_model(self.__class__)
        return urlresolvers.reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(self.id,))

    def get_human_info(self):
        info = u'<b>Блок Высшее образование:</b><br />'
        if self.institution_updated_json:
            info += u'<br /><span style="text-decoration: underline;">Обновление</span>:<br />'
            data = json.loads(self.institution_updated_json)
            for dat in data:
                info += u'<table>'
                inst = Institution.objects.get(pk=dat['pk'])
                info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(u'Название поля', u'Старая версия', u'Новая версия')
                for k,v in dat.items():
                    if k not in ('pk', 'id', 'expertblank'):
                        info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(Institution._meta.get_field_by_name(k)[0].verbose_name.capitalize(), getattr(inst, k),v)
                info += u'</table>'
        if self.institution_added_json:
            info += u'<br /><span style="text-decoration: underline;">Добавление:</span><br />'
            data = json.loads(self.institution_added_json)
            for dat in data:
                info += u'<table>'
#                info += u'>{}<br />'.format(inst.instatution_name)
                for k,v in dat.items():
                    if k not in ('pk', 'id', 'expertblank'):
                        info += u'<tr><td>{}</td><td>{}</td></tr>'.format(Institution._meta.get_field_by_name(k)[0].verbose_name.capitalize() ,v)
                info += u'</table>'

        info += u'<br /><b>Блок Учебные центры:</b><br />'
        if self.studycenter_updated_json:
            info += u'<br /><span style="text-decoration: underline;">Обновление</span>:<br />'
            data = json.loads(self.studycenter_updated_json)
            for dat in data:
                info += u'<table>'
                inst = StudyCenter.objects.get(pk=dat['pk'])
                info += u'<tr><td>{}</td><td>{}</td></tr>'.format(u'Старая версия', u'Новая версия')
                for k,v in dat.items():
                    if k not in ('pk', 'id', 'expertblank'):
                        info += u'<tr><td>{}</td><td>{}</td></tr>'.format(k,v)
                info += u'</table>'
        if self.studycenter_added_json:
            info += u'<br /><span style="text-decoration: underline;">Добавление:</span><br />'
            data = json.loads(self.studycenter_added_json)
            for dat in data:
                info += u'<table>'
                #                info += u'>{}<br />'.format(inst.instatution_name)
                for k,v in dat.items():
                    if k not in ('pk', 'id', 'expertblank'):
                        info += u'<tr><td>{}</td><td>{}</td></tr>'.format(StudyCenter._meta.get_field_by_name(k)[0].verbose_name.capitalize() ,v)
                info += u'</table>'

        info += u'<br /><b>Блок Ассоциации:</b><br />'
        if self.association_updated_json:
            info += u'<br /><span style="text-decoration: underline;">Обновление</span>:<br />'
            data = json.loads(self.association_updated_json)
            for dat in data:
                info += u'<table>'
                inst = Association.objects.get(pk=dat['pk'])
                info += u'<tr><td>{}</td><td>{}</td></tr>'.format(u'Старая версия', u'Новая версия')
                for k,v in dat.items():
                    if k not in ('pk', 'id', 'expertblank'):
                        info += u'<tr><td>{}</td><td>{}</td></tr>'.format(k,v)
                info += u'</table>'
        if self.association_added_json:
            info += u'<br /><span style="text-decoration: underline;">Добавление:</span><br />'
            data = json.loads(self.association_added_json)
            for dat in data:
                info += u'<table>'
                #                info += u'>{}<br />'.format(inst.instatution_name)
                for k,v in dat.items():
                    if k not in ('pk', 'id', 'expertblank'):
                        info += u'<tr><td>{}</td><td>{}</td></tr>'.format(Association._meta.get_field_by_name(k)[0].verbose_name.capitalize() ,v)
                info += u'</table>'

        return info

    def save(self, *a, **kw):
        super(ModerateInstitution, self).save(*a, **kw)
        if self.approved:
            if self.institution_updated_json:
                data = json.loads(self.institution_updated_json)
                for dat in data:
                    dpk = dat['pk']
                    del dat['pk']
                    inst = Institution.objects.filter(id=dpk).update(**dat)
            if self.institution_added_json:
                data = json.loads(self.institution_added_json)
                for dat in data:
                    dat['expertblank'] = ExpertBlank.objects.get(pk=dat['expertblank'])
                    Institution.objects.create(**dat)
            if self.studycenter_updated_json:
                data = json.loads(self.studycenter_updated_json)
                for dat in data:
                    dpk = dat['pk']
                    del dat['pk']
                    inst = StudyCenter.objects.filter(id=dpk).update(**dat)
            if self.studycenter_added_json:
                data = json.loads(self.studycenter_added_json)
                for dat in data:
                    dat['expertblank'] = ExpertBlank.objects.get(pk=dat['expertblank'])
                    StudyCenter.objects.create(**dat)

            if self.association_updated_json:
                data = json.loads(self.association_updated_json)
                for dat in data:
                    dpk = dat['pk']
                    del dat['pk']
                    inst = Association.objects.filter(id=dpk).update(**dat)

            if self.association_added_json:
                data = json.loads(self.association_added_json)
                for dat in data:
                    dat['expertblank'] = ExpertBlank.objects.get(pk=dat['expertblank'])
                    Association.objects.create(**dat)
            self.delete()
#        else:
#            if not self.info:
#                self.info = info
#            self.save()

    class Meta:
        verbose_name = u'Запрос на модерацию [Блок "Образование"]'
        verbose_name_plural = u'Запросы на модерацию [Блок "Образование"]'

    def __unicode__(self):
        return u'Запрос на изменение данных пользователя {} [Блок "Образование"]'.format(self.user.username)

class ModeratePrivateData(models.Model):
    approved = models.BooleanField(default=False, verbose_name=u'Принять')
    new_photo = models.ImageField(upload_to='avatars', blank=True, null=True)
    new_lastname = models.CharField(max_length=100, verbose_name=u'Новая фамилия', blank=True, null=True)
    new_firstname = models.CharField(max_length=100, verbose_name=u'Новое имя', blank=True, null=True)
    new_middlename = models.CharField(max_length=100, verbose_name=u'Новое отчество', blank=True, null=True)
    new_birthday = models.DateField(verbose_name=u'Новая дата рождения', blank=True, null=True)

    new_send_me_q = models.BooleanField(default=False, verbose_name=u'Получать вопросы от клиентов')
    new_free_consult = models.BooleanField(default=False, verbose_name=u'Предоставлять 10 минут бесплатно')

    new_about = models.TextField(verbose_name=u'Новая информация о себе', blank=True, null=True)

    new_country = models.CharField(
        max_length=255, verbose_name=u"Новая страна проживания",
        blank=True, null=True,
        choices=COUNTRIES)
    new_city = models.CharField(
        max_length=255, verbose_name=u"город проживания", blank=True, null=True)

    new_time_zone = models.FloatField(verbose_name=u'Новый пояс времени', choices=TZ_CHOICES, default=float(3), blank=True, null=True)

    user = models.ForeignKey(User, verbose_name=u'Пользователь')

    def get_admin_url(self):
        content_type = ContentType.objects.get_for_model(self.__class__)
        return urlresolvers.reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(self.id,))

    def get_human_info(self):
        info = u'<span style="text-decoration: underline;">Обновление</span>: <br />'
        info += u'<table>'
        info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(u'Название поля', u'Старая версия', u'Новая версия')
        if self.new_free_consult and self.new_free_consult != self.user.expertblank.free_consult:
            info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'\
            .format(u'Предоставлять 10 минут бесплатно', self.user.expertblank.free_consult, self.new_free_consult)
        if self.new_send_me_q and self.new_send_me_q != self.user.expertblank.send_me_q:
            info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'\
            .format(u'Получать вопросы от клиентов', self.user.expertblank.send_me_q, self.new_send_me_q)
        if self.new_firstname and self.new_firstname != self.user.expertblank.first_name:
            info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'\
            .format(u'Имя', self.user.expertblank.first_name, self.new_firstname)
        if self.new_lastname and self.new_lastname != self.user.expertblank.last_name:
            info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(u'Фамилия', self.user.expertblank.last_name, self.new_lastname)
        if self.new_middlename and self.new_middlename != self.user.expertblank.middle_name:
            info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(u'Отчество', self.user.expertblank.middle_name, self.new_middlename)
        if self.new_city and self.new_city != self.user.expertblank.city:
            info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(u'Город', self.user.expertblank.city, self.new_city)
        if self.new_about and self.new_about != self.user.expertblank.personal:
            info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(u'О себе', self.user.expertblank.personal, self.new_about)
        if self.new_country and self.new_country != self.user.expertblank.country:
            info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(u'Страна', self.user.expertblank.country, self.new_country)
        if self.new_photo and self.new_photo != self.user.expertblank.avatar:
            info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(u'Аватар', self.user.expertblank.avatar, self.new_photo)
        if self.new_birthday and self.new_birthday != self.user.expertblank.birthday:
            info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(u'День рождения', self.user.expertblank.birthday, self.new_birthday)
        if self.new_time_zone and self.new_time_zone != self.user.expertblank.expert.profile.time_zone:
            info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(u'Часовой пояс', self.user.expertblank.expert.profile.time_zone,
                self.new_time_zone)
        info += u'</table>'
        return info

    def is_expert(self):
        return True if self.user.groups.filter(name='experts').count() > 0 else False

    def save(self, *a, **kw):
        super(ModeratePrivateData, self).save(*a, **kw)
        if self.approved:
            if self.new_photo:
                self.user.expertblank.avatar.save(self.new_photo.url, ContentFile(self.new_photo.read()), save=False)
                #= self.new_photo
            if self.new_free_consult: self.user.expertblank.free_consult = self.new_free_consult
            if self.new_send_me_q: self.user.expertblank.send_me_q = self.new_send_me_q
            if self.new_firstname: self.user.expertblank.first_name = self.new_firstname
            if self.new_lastname: self.user.expertblank.last_name = self.new_lastname
            if self.new_middlename: self.user.expertblank.middle_name = self.new_middlename
            if self.new_birthday: self.user.expertblank.birthday = self.new_birthday
            if self.new_about: self.user.expertblank.personal = self.new_about
            if self.new_country: self.user.expertblank.country = self.new_country
            if self.new_city: self.user.expertblank.city = self.new_city
            if self.new_time_zone: self.user.profile.time_zone = self.new_time_zone
            if self.is_expert(): self.user.expertblank.save()
            self.user.profile.save()
            self.user.save()
            self.delete()

    class Meta:
        verbose_name = u'Запрос на модерацию [Блок "Личные данные"]'
        verbose_name_plural = u'Запросы на модерацию [Блок "Личные данные"]'

    def __unicode__(self):
        return u'Запрос на изменение данных пользователя {} [Блок "Личные данные"]'.format(self.user.username)

class ModerateLoginData(models.Model):
    approved = models.BooleanField(default=False, verbose_name=u'Принять')
    new_email = models.EmailField(verbose_name=u'Новая почта', blank=True, null=True)
    new_phone = models.CharField(max_length=100, verbose_name=u'Новый телефон', blank=True, null=True)
    new_password = models.CharField(max_length=100, verbose_name=u'Новый пароль', blank=True, null=True)
    new_subscribe = models.CharField(max_length=3, verbose_name=u'Подписаться на рассылку', blank=True, null=True)

    user = models.ForeignKey(User, verbose_name=u'Пользователь')

    def get_admin_url(self):
        content_type = ContentType.objects.get_for_model(self.__class__)
        return urlresolvers.reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(self.id,))

    def get_human_info(self):
        info = u'<span style="text-decoration: underline;">Обновление</span>: <br />'
        info += u'<table>'
        info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(u'Название поля', u'Старая версия', u'Новая версия')
        if self.new_email and self.new_email != self.user.email:
            info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(u'Почта', self.user.email, self.new_email)
        if self.new_phone and self.new_phone != self.user.profile._phone_number:
            info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(u'Телефон', self.user.profile._phone_number, self.new_phone)
        if self.new_password:
            info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(u'Пароль', u'***', self.new_password)
        if self.new_subscribe:
            info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(u'Подписка', u'Да' if self.user.profile.subscribe else u'Нет', self.new_subscribe)
        info += u'</table>'
        return info

    def save(self, *a, **kw):
        super(ModerateLoginData, self).save(*a, **kw)
        if self.approved:
            if self.new_email:
                self.user.email = self.new_email
            if self.new_phone:
#                self.user.profile.phone_number = self.new_phone
#                self.user.profile.phone_number
                self.user.profile._phone_number = self.new_phone
                if self.user.groups.filter(name='experts').count() > 0:
                    self.user.expertblank.phone_number = self.new_phone
                    self.user.expertblank.save()
            if self.new_password:
                self.user.set_password(self.new_password)
            if self.new_subscribe:
                self.user.profile.subscribe = True if self.new_subscribe == u'Да' else False
            self.user.profile.save()
            self.user.save()
            self.delete()


    class Meta:
        verbose_name = u'Запрос на модерацию [Блок "Учетные данные"]'
        verbose_name_plural = u'Запросы на модерацию [Блок "Учетные данные"]'

    def __unicode__(self):
        return u'Запрос на изменение данных пользователя {} [Блок "Учетные данные"]'.format(self.user.username)

class ModerateIssues(models.Model):
    approved = models.BooleanField(verbose_name=u'Принять', default=False)
    user = models.ForeignKey(User, verbose_name=u'Пользователь')
    data = models.TextField(verbose_name=u'Данные', blank=True, null=True)

    class Meta:
        verbose_name = u'Запрос на модерацию [Блок "Проблемы"]'
        verbose_name_plural = u'Запросы на модерацию [Блок "Проблемы"]'

    def get_admin_url(self):
        content_type = ContentType.objects.get_for_model(self.__class__)
        return urlresolvers.reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(self.id,))

    def __unicode__(self):
        return u'Запрос на изменение данных пользователя {} [Блок "Проблемы"]'.format(self.user.username)

    def save(self, *a, **kw):
        super(ModerateIssues, self).save(*a, **kw)
        if self.approved:
            self.user.expertblank.issues.through.objects.filter(expertblank=self.user.expertblank).delete()
            for iss in Issue.objects.filter(id__in=json.loads(self.data)):
                self.user.expertblank.issues.add(iss)
                self.user.expertblank.save()
            self.delete()

    def get_human_info(self):
            info = u'<span style="text-decoration: underline;">Обновление</span>: <br />'
            d = json.loads(self.data)
            old_iss_ids = self.user.expertblank.issues.values_list('id', flat=True)
            diff = set(d) ^ set(old_iss_ids)
            diff_list = list(diff)
            info += u'<table>'
            info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(u'Название поля', u'Старая версия', u'Новая версия')
            for iss in Issue.objects.filter(id__in=diff_list):
                if iss.pk in d:
                    info+= u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(iss.name, u'Нет', u'Да')
                if iss.pk in old_iss_ids:
                    info += u'<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(iss.name, u'Да', u'Нет')
            info += u'</table>'
            return info

class PersonalCard(models.Model):
    expert = models.ForeignKey(User, verbose_name=u'Специалист', related_name='personal_card_expert')
    client = models.ForeignKey(User, verbose_name=u'Клиент', related_name='personal_card_client')
    note = models.TextField(verbose_name=u'Заметки', blank=True, null=True)

    class Meta:
        verbose_name = u'Заметка специалиста'
        verbose_name_plural = u'Заметки специалиста'

    def __unicode__(self):
        ret = [u'-', u'-']
        if hasattr(self.expert, 'username'): ret[0] = self.expert.username
        if hasattr(self.client, 'username'): ret[1] = self.client.username
        return u'[{}] {}'.format(*ret)

class ExpertQuestion(models.Model):
    title = models.CharField(max_length=256, verbose_name=u'Заголовок вопроса', default=u'')
    text = models.TextField(verbose_name=u'Текст вопроса')
    user = models.ForeignKey(User, verbose_name=u'Автор вопроса', blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name=u'Время создания')
    issue = models.ForeignKey(Issue, verbose_name=u'Проблема')

    def get_answers(self):
        return self.answers.order_by('created')

    def __unicode__(self):
        return u'[{}]: {}'.format(self.created, self.title)

    class Meta:
        verbose_name = u'Вопрос к экспертам'
        verbose_name_plural = u'Вопросы к экспертам'


class ExpertAnswer(models.Model):
    text = models.TextField(verbose_name=u'Текст ответа')
    expert = models.ForeignKey(User, verbose_name=u'Эксперт', null=True, blank=True)
    question = models.ForeignKey(ExpertQuestion, verbose_name=u'Вопрос', related_name='answers', null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата создания')

    def __unicode__(self):
        return u'{} | {}'.format(self.question, self.text[:20])

    class Meta:
        verbose_name = u'Ответ эксперта'
        verbose_name_plural = u'Ответы экспертов'