#coding: UTF-8

import pytz
import string
import random

from dateutil import tz
from datetime import datetime

from django.contrib.auth.models import User
from django.utils import timezone

from django.db import models


class TimeTable(models.Model):

    expert = models.OneToOneField(User)

    @property
    def gaps(self):
        return self.timegap_set.all()


class TimeGap(models.Model):

    table = models.ForeignKey(TimeTable)
    dtstart = models.DateTimeField(db_index=True)
    dtstop = models.DateTimeField(blank=True, null=True)
    timezone=tz.tzutc()
