# coding: UTF-8

from django.contrib.auth.models import User

from core.utils import user_add_group, send_email
from core.models import Profile

from models import ExpertBlank, TimeTable
import logging

def create_expert(data):
    if User.objects.filter(
        username=data["username"],
        first_name=data["first_name"],
        last_name=data["last_name"],
        email=data["email"]
    ).count() > 0:
        expert = User.objects.filter(
            username=data["username"],
            first_name=data["first_name"],
            last_name=data["last_name"],
            email=data["email"]
        )[0]
    else:
        expert = User(
            username=data["username"],
            first_name=data["first_name"],
            last_name=data["last_name"],
            email=data["email"]
            )

    expert.set_password(data["password"])
    expert.save()

    user_add_group(expert, "experts", True)

    if Profile.objects.filter(user=expert).count() > 0: profile = Profile.objects.filter(user=expert)[0]
    else: profile = Profile(user=expert)
    if TimeTable.objects.filter(expert=expert).count() > 0: timetable = TimeTable.objects.filter(expert=expert)[0]
    else: timetable = TimeTable(expert=expert)

    profile.save()
    timetable.save()

    #шлем уведомление эксперту, что он зарегистрирован
    send_email(
        user=expert,
        subject=u"Заявка одобрена!",
        template_name="expert_registration_approved.txt",
        _context={"password": data["password"], "username": expert.username}
        )
    logging.debug(u'Email sended after saved profile')

    return expert
