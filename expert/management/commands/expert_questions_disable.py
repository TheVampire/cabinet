from django.core.management.base import BaseCommand, CommandError

from expert.models import ExpertBlank

class Command(BaseCommand):

    def handle(self, *args, **options):
        for eb in ExpertBlank.objects.filter(is_tested=False):
            eb.send_me_q = False
            eb.save()