#coding: UTF-8

from json import dumps
import base64, hashlib
import datetime
import pytils
import os
from django.contrib.auth.models import User

from django.views.generic import View, ListView, TemplateView, CreateView, DetailView
from django.shortcuts import render, redirect, get_object_or_404, Http404
from django.forms.models import modelformset_factory
from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings
from django.forms import HiddenInput
from django.core.mail import send_mail
from django.core.urlresolvers import reverse

from django.utils.decorators import method_decorator
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login
from django.db.models import Count, Q

from core.utils import json_defaults, get_or_none, QuerySetDiggPaginator, DiggPaginator
from core.models import Banner

from core.utils import remove_holddown, send_email

from forms import (
    ExpertBlankForm, AssociationForm, InstitutionForm, ExpertQuestionForm, ExpertAnswerForm,
    ConsultatonAddForm, StudyCenterForm, RecalAddForm
    )
from core.forms import PayRegForm
from models import (
    ExpertBlank, Association, Institution, ExpertQuestion, ExpertAnswer,
    Recal, StudyCenter, Specialization, Consultation, Issue, WorkDay, WorkHour, PersonalCard, DiplomScan
    )
from django.contrib.sites.models import Site, RequestSite
from registration.models import RegistrationProfile
from registration import signals

from registration.backends.default.views import RegistrationView


class WorkDayItemView(TemplateView):
    template_name = 'private/workday/item.html'
    def get(self, request, *a, **kw):
        self.year = int(request.GET.get('year', 0))
        self.month = int(request.GET.get('month', 0))
        self.day = int(request.GET.get('day', 0))
        self.week = int(request.GET.get('week', 0))
        self.dow = int(request.GET.get('dayofweek', -1))
        return super(WorkDayItemView, self).get(request, *a, **kw)

    def get_context_data(self, **kwargs):
        ctx = super(WorkDayItemView, self).get_context_data()
        if self.year:
            date = date=datetime.date(
                self.year,
                self.month,
                self.day
            )
            ctx['wps'] = WorkDay.get_periods(user=self.request.user, date=date).distinct()
            ctx['wd'] = WorkDay.get_wd(user=self.request.user,date=date)
            ctx['title'] = pytils.dt.ru_strftime(format=u'%d %B', date=date, inflected=True)
        if self.week:
            ctx['wps'] = WorkDay.get_periods(user=self.request.user, week=self.week).distinct()
            ctx['wd'] = WorkDay.get_wd(user=self.request.user, week=self.week)
            ctx['title'] = u'{} неделя'.format(self.week+1)
        if self.dow != -1:
            ctx['wps'] = WorkDay.get_periods(user=self.request.user, dow=self.dow).distinct()
            ctx['wd'] = WorkDay.get_wd(user=self.request.user,dow=self.dow)
            d = [
                u'Поднедельник',
                u'Вторник',
                u'Среда',
                u'Четверг',
                u'Пятница',
                u'Суббота',
                u'Воскресенье'
            ]
            ctx['title'] = d[self.dow]
        if 'wps' in ctx and len(ctx['wps']) == 1:
            ctx['is_allday'] = ctx['wps'][0].is_allday()
        return ctx

class WorkDayView(View):
    def post(self, request):
        oneday = None
        dayofweek = None
        week = None
        if request.POST.get('oneday', False):
            oneday = datetime.datetime.strptime(request.POST.get('oneday'), '%Y %m %d').date()
        if request.POST.get('dayofweek', False):
            dayofweek = request.POST.get('dayofweek')
        if request.POST.get('week', False):
            week = request.POST.get('week')
        kw = {'user': request.user}
        if oneday:
            kw['oneday'] = oneday
        if week:
            kw['week'] = week
        if dayofweek:
            kw['dayofweek'] = dayofweek
        wd,cr = WorkDay.objects.get_or_create(**kw)
        if request.POST.get('holliday', False) == '1':
            wd.holliday = True
            wd.workperoids.clear()
            wd.save()
        if request.POST.get('allday', False):
            wh,cr = WorkHour.objects.get_or_create(
                start_time='00:00',
                end_time='23:59'
            )
            wd.workperoids.add(wh)
            wd.holliday = False
            wd.save()
        if request.POST.get('time-left', False) and request.POST.get('time-right', False):
            wd.holliday = False
            wd.save()
            left_times = request.POST.getlist('time-left')
            right_times = request.POST.getlist('time-right')
            times = []
            for i in xrange(0, len(left_times)):
                times.append((left_times[i], right_times[i]))
#            print times
            if request.POST.get('changed_data', False):
#                print request.POST.get('changed_data')
                for ch in request.POST.get('changed_data').split(' '):
                    ch_splitted = ch.split('-')
                    ch_s_time = ch_splitted[1]
                    ch_e_time = ch_splitted[2]
                    ch_pk = ch_splitted[0]
                    wd.workperoids.remove(WorkHour.objects.get(pk=ch_pk))
                    wh,cr = WorkHour.objects.get_or_create(
                        start_time=ch_s_time,
                        end_time = ch_e_time
                    )
                    wd.workperoids.add(wh)
            if request.POST.get('deleted', False):
                for it in request.POST.get('deleted').split(';'):
                    if it and it.isdigit():
                        wd.workperoids.remove(
                            WorkHour.objects.get(pk=it)
                        )
            for time in times:
                s_time = datetime.time(int(time[0].split(':')[0]), int(time[0].split(':')[1]))
                e_time = datetime.time(int(time[1].split(':')[0]), int(time[1].split(':')[1]))
                wh,cr = WorkHour.objects.get_or_create(
                    start_time = s_time,
                    end_time = e_time
                )
                wd.workperoids.add(wh)
            if week:
                for workday in WorkDay.objects.filter(oneday__isnull=False):
                    if workday.oneday.strftime('%W') == week:
                        workday.delete()
            if dayofweek:
                for workday in WorkDay.objects.filter(oneday__isnull=False):
                    if unicode(workday.oneday.weekday()) == dayofweek:
                        workday.delete()
#            print wd.workperoids.all()
            wd.sort_workperoids()
#            print wd.workperoids.all()
#            wd.workperoids.save()
        return render(request, "private/workday/saved.html",{
            'msg': u'Изменения сохранены'
        })

class ExpertRegistration(View):

    #@todo: need use datetime mask in registration form in order to locale,
    #now locale is Russian, so 21.10.2010 works but 2010-10-21 dont 

    Associations = modelformset_factory(
            Association, AssociationForm)
    Institutions = modelformset_factory(
            Institution, InstitutionForm, max_num=3)
    StudyCenters = modelformset_factory(
        model=StudyCenter, form=StudyCenterForm, max_num=10)

    def get(self, request):

        associations = ExpertRegistration.Associations(
            queryset=Association.objects.none(), prefix="associations")
        institutions = ExpertRegistration.Institutions(
            queryset=Institution.objects.none(), prefix="institutions")
        study_centers = ExpertRegistration.StudyCenters(
            queryset=StudyCenter.objects.none(), prefix="study_centers")

        return render(
            request,
            "registration/expert_registration_form.html",
            {
                "form": remove_holddown(
                    ExpertBlankForm, ("specialization", "issues")
                    ),
                "associations": associations,
                "institutions": institutions,
                "study_centers": study_centers
            }
            )

    def post(self, request):

        associations = ExpertRegistration.Associations(
            request.POST, prefix="associations")
        institutions = ExpertRegistration.Institutions(
            request.POST, prefix="institutions")
        study_centers = ExpertRegistration.StudyCenters(
            request.POST, prefix="study_centers")

        form = ExpertBlankForm(request.POST, request.FILES)

        if (form.is_valid() and associations.is_valid() and
            institutions.is_valid() and study_centers.is_valid()):

            blank = form.save()
            i = 1
            while True:
                if 'diplom-scan-file-{}'.format(i) in request.FILES:
                    diplom = request.FILES['diplom-scan-file-{}'.format(i)]
                    path = os.path.join(settings.PROJECT_ROOT, 'media', 'images', diplom.name)
                    with open(path, 'wb+') as destination:
                        for chunk in diplom.chunks():
                            destination.write(chunk)
                    new_diplom = DiplomScan(
                        scan = os.path.join('media', 'images', diplom.name),
                        text = request.POST.get('diplom-scan-text-{}'.format(i))
                    )
                    new_diplom.save()
                    blank.scans.add(new_diplom)
                    blank.save()
                    i += 1
                else:
                    break
            for association in map(
                lambda form: form.save(commit=False), associations):
                association.expertblank = blank
                association.save()
            for institution in map(
                lambda form: form.save(commit=False), institutions):
                institution.expertblank = blank
                institution.save()

            for study_center in map(
                lambda form: form.save(commit=False), study_centers):
                study_center.expertblank = blank
                study_center.save()

            map(lambda admin:
            send_email(
                subject=u"Добавлена новая анкета специалиста", 
                template_name="expert_registration_message_for_admin.txt",
                _context={"blank": blank, "admins_name": admin[0]},
                email=admin[1]
                ),
            settings.ADMINS
            )

            send_email(
                subject=u"Кабинет №1 - ваша заявка принята и обрабатывается",
                template_name="expert_registration_blank_accepted.txt",
                _context={"blank": blank},
                email=blank.email
                )

            return render(
                request,
                "registration/expert_registration_request_accepted.html",
                {
                    "email":  request.POST.get('email', '')
                }
                )

        else:
            return render(
                request,
                "registration/expert_registration_form.html",
                {
                    "form": form,
                    "associations": associations,
                    "institutions": institutions,
                    "study_centers": study_centers,
                }
                )


class ExpertDetails(View):

    def get(self, request, username):
        expert = get_object_or_404(
            User, username=username, groups__name='experts')
        form = RecalAddForm()
        form.fields['expert'].initial = expert
        form.fields['expert'].widget = HiddenInput()
        cur_issue = None
        if self.request.GET.get("issue", False):
            cur_issue = Issue.objects.get(pk=self.request.GET.get("issue"))
        iss_parent = Issue.objects.get(pk=1)
        groups = {}
        for iss in iss_parent.children.all():
            if iss.children.count() > 0:
                groups[iss.name] = []
                for isss in iss.children.all():
                    groups[iss.name].append(isss)
#            if ' - ' in iss.name:
#                if iss.name.split(' - ')[0] not in groups:
#                    groups[iss.name.split(' - ')[0]] = []
#                groups[iss.name.split(' - ')[0]].append(iss)
        if groups:
            for k,v in groups.items():
                groups[k] = sorted(v, key=lambda x: x.name)

        is_visible_add_recal_form = False
        if request.user.is_authenticated() and Consultation.objects.filter(
            expert=expert,
            client=request.user,
            status=u'done'
        ).count() > 0:
            is_visible_add_recal_form = True
        return render(
            request, "expert_details.html",
            {
                'is_visible_add_recal_form': is_visible_add_recal_form,
                'expert': expert,
                'wizzard_groups': groups,
                'recals': Recal.objects.filter(expert=expert).order_by('-datetime'),
                'cur_issue': cur_issue,
                'recal_form': form,
                'groups': Issue.objects.filter(parent=None).filter(~Q(name=u'Другое')),
                'comment_possible': request.user.is_authenticated() and Consultation.objects.filter(client=request.user, expert=expert, status=u'done').count()
            }
            )

    def post(self, request, username):
        if self.request.POST.get('recal_type') and self.request.POST.get('recal_text'):
            expert = get_object_or_404(
                User, username=username, groups__name='experts')
            if Consultation.objects.filter(
                expert=expert,
                client=request.user,
                status=u'done'
            ).count() > 0:
                Recal.objects.create(
                    client=request.user,
                    expert=expert,
                    text=request.POST.get('recal_text'),
                    type=request.POST.get("recal_type")
                )
                msg = u'Клиент (username): {}\n'.format(request.user.username)
                msg += u'Клиент (ФИО): {} {} {}\n'.format(request.user.first_name, request.user.last_name, request.user.profile.middlename)
                msg += u'Специалист (username): {}\n'.format(expert.username)
                msg += u'Дата/Время: {}\n'.format(timezone.localtime(timezone.now()).strftime(u'%d.%m.%Y/%H:%M'))
                msg += u'Текст отзыва: {}\n'.format(request.POST.get("recal_text"))
                send_mail(u'Кабинет №1 - Добавлен отзыв', msg, 'info@kabinet1.ru', ['support@kabinet1.ru'])
        return self.get(request, username)
def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)+1):
        yield start_date + datetime.timedelta(n)
def check_wd(wd, time):
    ex_pass = False
    if not wd.holliday:
        if wd.workperoids.count() == 0:
            ex_pass = True
        else:
            if time != 'all':
                if time == 'morning':
                    if wd.workperoids.filter(
                        start_time__lt=datetime.time(14, 00)
                    ).count() > 0:
                        ex_pass = True
                if time == 'day':
                    if wd.workperoids.filter(
                        start_time__lt=datetime.time(19, 00),
                        start_time__gte=datetime.time(14, 00)
                    ).count() > 0:
                        ex_pass = True
                if time == 'evening':
                    if wd.workperoids.filter(
                        start_time__gte=datetime.time(19, 00)
                    ).count() > 0:
                        ex_pass = True
            else:
                ex_pass = True
    return ex_pass

class ExpertList(ListView):

    queryset = User.objects.filter(groups__name='experts', expertblank__is_tested=False)
    context_object_name = "experts"
    template_name = "expert_list.html"
    paginate_by = None
    order_by = 'pk'
    paginator_class = QuerySetDiggPaginator
    group_list = []
    pf = None
    sexf = None
    freef = None
    issue_values = None

    def get_paginator(self, queryset, per_page, orphans=0, allow_empty_first_page=True):
        return QuerySetDiggPaginator(queryset, per_page, padding=3, body=7, tail=1)

    def get_queryset(self):
        if self.request.GET.get('filter', False):
            expertblanks = Issue.objects.get(pk=self.request.GET.get('filter', None)).expertblank_set.all()
            self.queryset = self.queryset.filter(id__in=[ex.expert.pk for ex in expertblanks if ex.expert])
        if self.request.GET.get('group'):
            self.group_list = self.request.GET.getlist('group')
            ex_pk = []
            for g in self.group_list:
                iss = Issue.objects.get(pk=g)
                ex_pk.extend(iss.expertblank_set.values_list('expert__id', flat=True))
            ex_pk = list(set(ex_pk))
            self.queryset = self.queryset.filter(id__in=ex_pk)
        if self.request.GET.get('issue'):
            self.issue_values = self.request.GET.getlist('issue')
            e_ids = []
            for iv in self.issue_values:
                iss = Issue.objects.get(pk=iv)
                e_ids.extend(iss.expertblank_set.values_list('expert__id', flat=True))
            self.queryset = self.queryset.filter(id__in=e_ids)

        if self.request.GET.get('price_filter'):
            self.pf = self.request.GET.getlist('price_filter')
            qqq = None
            for i, pff in enumerate(self.pf):
                if pff[0] == '-' or pff[0] == '+':
                    limit = int(pff[1:])
                    if pff[0] == '-':
                        if i == 0:
                            qqq = Q(expertblank__fix__lt=limit)
                        else:
                            qqq |= Q(expertblank__fix__lt=limit)
                    if pff[0] == '+':
                        if i == 0:
                            qqq = Q(expertblank__fix__gt=limit)
                        else:
                            qqq |= Q(expertblank__fix__gt=limit)
                else:
                    min_limit,max_limit = pff.split('-')
                    if i == 0:
                        qqq = Q(expertblank__fix__gte=min_limit, expertblank__fix__lte=max_limit)
                    else:
                        qqq |= Q(expertblank__fix__gte=min_limit, expertblank__fix__lte=max_limit)
            if qqq:
                self.queryset = self.queryset.filter(qqq)

        if self.request.GET.get('sex'):
            self.sexf = self.request.GET.getlist('sex')
            if len(self.sexf) == 1:
                sex = True if self.sexf[0] == '1' else False
                self.queryset = self.queryset.filter(expertblank__sex=sex)

        if self.request.GET.get('free'):
            self.freef = self.request.GET.get('free')
            free = True if self.freef == '1' else False
            self.queryset = self.queryset.filter(expertblank__free_consult=free)
        min_date = None
        max_date = None
        is_holiday = False
        is_workday = False
        if self.request.GET.get("range_start"):
            min_date = datetime.datetime.strptime(self.request.GET.get("range_start"), '%d.%m.%Y')
        if self.request.GET.get("range_end"):
            max_date = datetime.datetime.strptime(self.request.GET.get("range_end"), '%d.%m.%Y')
        if self.request.GET.get("is_holiday"):
            is_holiday = True
        if self.request.GET.get("is_workday"):
            is_workday = True
        time = self.request.GET.get("time", 'all')
        for ex in self.queryset:
            ex_pass = False
            if min_date and max_date:
                for d in daterange(min_date, max_date):
                    if time != 'all':
                        if time == 'morning':
                            if WorkDay.is_worked_in_morning(ex, d, is_holiday, is_workday):
                                ex_pass = True
                        if time == 'day':
                            if WorkDay.is_worked_in_day(ex, d, is_holiday, is_workday):
                                ex_pass = True
                        if time == 'evening':
                            if WorkDay.is_worked_in_evening(ex, d, is_holiday, is_workday):
                                ex_pass = True
                    else:
                        if WorkDay.is_worked(ex, d):
                            if is_holiday ^ is_workday:
                                if is_holiday:
                                    if d.weekday() in [5,6]:
                                        ex_pass = True
                                if is_workday:
                                    if d.weekday() in xrange(0, 5):
                                        ex_pass = True
                            else:
                                ex_pass = True
                    if ex_pass:
                        continue
            else:
                # if no date range
                if is_holiday ^ is_workday:
                    if is_workday:
                        if ex.workday_set.filter(dayofweek__in=xrange(0, 5)).distinct().count() == 5:
                            for wd in ex.workday_set.filter(dayofweek__in=xrange(0, 5)).distinct():
                                if check_wd(wd, time):
                                    ex_pass = True
                        else:
                            ex_pass = True
                    if is_holiday:
                        if ex.workday_set.filter(dayofweek__in=[5,6]).distinct().count() == 2:
                            for wd in ex.workday_set.filter(dayofweek__in=[5,6]).distinct():
                                if check_wd(wd, time):
                                    ex_pass = True
                        else:
                            ex_pass = True
                else:
                    if ex.workday_set.filter(dayofweek__isnull=False).distinct().count() == 7:
                    # check this
                        for wd in ex.workday_set.filter(dayofweek__isnull=False).distinct():
                            if check_wd(wd, time):
                                ex_pass = True
                    else:
                        ex_pass = True

            if not ex_pass:
                self.queryset = self.queryset.exclude(id=ex.pk)
        return self.queryset

    def get_context_data(self, **kwargs):
        ctx = super(ExpertList, self).get_context_data(**kwargs)
        ctx['price_filter'] = self.pf
        ctx['sex'] = self.sexf
        ctx['free'] = self.freef
        ctx['group_values'] = self.group_list
        ctx['right_banner'] = get_or_none(Banner, path=self.request.path, place='R')
        ctx['issues'] = Issue.objects.filter(parent=None)
        ctx['active_issue'] = int(self.request.GET.get('filter', -1))
        ctx['expert_question_form'] = ExpertQuestionForm()
        if self.issue_values:
            ctx['iss_value'] = [int(p) for p in self.issue_values]
        return ctx

class ExpertListAjaxView(ExpertList):
    template_name = 'expert_list_ajax.html'
    paginate_by = None

class RecalList(ListView):

    queryset = Recal.objects.order_by('-datetime')
    context_object_name = "recals"
    template_name = "expert_recal__list.html"
    paginate_by = 10


#отзыв об эксперте
class RecalAdd(View):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(RecalAdd, self).dispatch(*args, **kwargs)

    def post(self, request):
        form = RecalAddForm(request.POST)
        if form.is_valid():

            recal = form.save(commit=False)
            recal.client = request.user
            recal.save()

            return render(request, "expert_details__add_review_success.html", {})
        else:
            return HttpResponse(
                dumps(form.errors),
                content_type="application/javascript"
                )


class ConsultationDetails(View):
    def get(self, request,  secret_code):
        consultation = get_object_or_404(Consultation, secret_code=secret_code)
        return render(
            request, "consultation.html", {"consultation": consultation}
            )


class ConsultationAdd(View):
    def post(self, request):

        session = request.session

        data = request.POST
        session.update(
        {
            "container":
                {
                    "issue": get_object_or_404(Issue, id=data["issue"]),
                    "expert": get_object_or_404(User, id=data["expert"]),
                    "category": get_object_or_404(Issue, id=data["category"])
                }
        })

        print "session cache after update", request.session._session_cache

        return HttpResponseRedirect("/wizzard/time/")


class Pulse(View):
    def get(self, request):
        pass

import calendar, json
class WorkDayAjaxView(View):
    def get(self, request):
        days = []
        if request.GET.get('key', False):
            self.month = int(request.GET.get('key').split('.')[0])-1
            self.year = int(request.GET.get('key').split('.')[1])
            if self.year and self.month:
                for i in xrange(calendar.monthrange(self.year, self.month+1)[1]):
#                    print i
                    date = datetime.date(self.year, self.month+1, i+1)
#                    print date
                    session = request.session
                    container = session.get("container", None)
                    if container:
                        u = container["expert"]
                        days.append(date.strftime('%d.%m.%Y') if WorkDay.is_worked(user=u, date=date) else '')
        j_data = json.dumps(days)
        return HttpResponse(j_data, mimetype='application/json')

class CheckIntervalView(View):
    def get(self, request):
        rt = '0'
        self.date_str = request.GET.get('date', None)
        self.hour_str = int(request.GET.get('time-hour', None))
        self.min_str = int(request.GET.get('time-min', None))
#        print self.hour_str, self.min_str
        self.date = datetime.datetime.strptime(self.date_str, '%d.%m.%Y')
        self.time = datetime.time(self.hour_str, self.min_str)
        self.datetime = datetime.datetime.strptime(self.date_str + ' {}:{}'.format(self.hour_str, self.min_str), '%d.%m.%Y %H:%M')
        if self.datetime > datetime.datetime.now():
            session = request.session
            container = session.get("container", None)
            if container:
                u = container["expert"]
                if WorkDay.is_worked(user=u, date=self.date):
                    wpds = WorkDay.get_periods(user=u, date=self.date)
                    if wpds.count() > 0:
                        for wp in wpds:
                            if wp.start_time < self.time < wp.end_time:
                                rt='1'
                    else:
                        rt = '1'
            else:
                rt = "-1"
        else:
            rt = "-1"
        return HttpResponse(rt)

class PersonalCardView(View):
    def get(self, request):
        data = {}
        client_id = request.GET.get('client', None)
        if client_id:
            client = User.objects.get(pk=client_id)
            pc, cr = PersonalCard.objects.get_or_create(
                expert=request.user,
                client=client
            )
            data = {
                'client_name': u'{} {}'.format(client.first_name, client.last_name),
                'info': pc.note
            }
        j_data = json.dumps(data)
        return HttpResponse(j_data, mimetype='application/json')

    def post(self, request):
        r = 0
        client_id = request.GET.get('client', None)
        client = User.objects.get(pk=client_id)
        if client:
            client = User.objects.get(pk=client_id)
            pc, cr = PersonalCard.objects.get_or_create(
                expert=request.user,
                client=client
            )
            pc.note = request.POST.get('info', u'')
            pc.save()
            r=1
        return HttpResponse(r)

class ExpertQuestionListView(ListView):
    model = ExpertQuestion
    template_name = 'questions/list.html'
    context_object_name = 'expert_questions'
    queryset = ExpertQuestion.objects.order_by('-created')

    def post(self, request, *args, **kwargs):
        if request.POST:
            form = ExpertAnswerForm(request.POST)
            if form.is_valid():
                qa = form.save()
                qa.expert = request.user
                qa.question = ExpertQuestion.objects.get(pk=request.POST.get('question'))
                qa.save()
                msg = u'{}'.format(request.build_absolute_uri(
                    reverse('expert-questions-view') + '?question={}#{}'.format(qa.question.pk, qa.question.pk)
                ))
                send_mail(u'Вам ответили на вопрос', msg, 'info@kabinet1.ru', [qa.question.user.email])
        return self.get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super(ExpertQuestionListView, self).get_context_data(**kwargs)
        ctx['expert_question_form'] = ExpertQuestionForm()
        ctx['answer_form'] = ExpertAnswerForm()
        if self.request.GET.get('question'):
            ctx['active_question'] = int(self.request.GET.get('question'))
        return ctx

class AskExpertAjaxView(CreateView):
    model = ExpertQuestion
    form_class = ExpertQuestionForm
    template_name = 'base.html'

    def form_invalid(self, form):
        data = {
            'errors': form.errors
        }
        return HttpResponse(json.dumps(data), content_type='application/json')

    def form_valid(self, form):
        eq = form.save()
        email = form.cleaned_data['email']
        user = None
        if User.objects.filter(email = email).exists():
            user = User.objects.get(email=email)
        else:
            if self.request.POST.get('username'):
                p = User.objects.make_random_password()
                form = PayRegForm(self.request.POST)
                if form.is_valid():
                    username=form.cleaned_data['username']
                    email = form.cleaned_data['email']

                    if Site._meta.installed:
                        site = Site.objects.get_current()
                    else:
                        site = RequestSite(self.request)
                    new_user = RegistrationProfile.objects.create_inactive_user(username, email,
                                                                                p, site)
                    signals.user_registered.send(sender=RegistrationView,
                                     user=new_user,
                                     request=self.request)

                    msg = u'Логин: {}\n Пароль: {}\n'.format(username, p)
                    send_mail(u'Кабинет №1 - данные для входа в Ваш личный кабинет', msg, 'info@kabinet1.ru', [new_user.email])

                    user = new_user
        if user:
            eq.user = user
            eq.save()

            next = '{}'.format(reverse('expert-questions-view') + '?question={}#{}'.format(eq.pk, eq.pk))

            for n, eb in enumerate(ExpertBlank.objects.filter(send_me_q=True).all()):
                us = eb.expert
                m = hashlib.md5()
                m.update(settings.LOGIN_SECRET + '{}{}'.format(us.pk, us.email))
                checksum = m.hexdigest()
                backurl = self.request.build_absolute_uri(
                        reverse('autologin-view') + '?email={}&checksum={}&next={}'.format(base64.b64encode(us.email), checksum, next)
                    )
                msg = u"""
Здравствуйте, {}\n
Посетитель сайта, которому сейчас сложно определиться с выбором психолога, описал свою проблему. Если Вам близка данная тематика и Вы заинтересованы в проведении консультации, пожалуйста, перейдите по ссылке и напишите ответ. Он будет направлен потенциальному клиенту с ссылкой на Ваш профиль,а также опубликован в разделе "Советы", доступной всем посетителям сайта.

{}:
{}
{}
{}""".format(eb.first_name, eq.user.username, eq.text, backurl, u'P.S. Вы получили это письмо,'\
                                                                                  u' так как зарегистрировали профиль специалиста-психолога на сайте www.kabinet1.ru. '\
                                                                                  u'Вы можете изменить настройки получения оповещений о вопросах в Вашем личном кабинете в разделе "личные данные"'\
                                                                                  u' (ссылка на страницу входа в ЛК http://kabinet1.ru/accounts/profile/).')
                send_mail(u'Вопрос от потенциального клиента', msg, 'info@kabinet1.ru', [us.email])
                if n == 0:
                    send_mail(u'Вопрос от потенциального клиента', msg, 'info@kabinet1.ru', ['support@kabinet1.ru'])
        return HttpResponse("OK")

class AutoLoginView(View):
    def get(self, request, *args, **kwargs):
        email = request.GET.get('email')
        email = base64.b64decode(email)
        try:
            user = User.objects.filter(email=email).all()[0]
            csum = request.GET.get('checksum')
            ss = settings.LOGIN_SECRET + '{}{}'.format(user.pk, user.email)
            m = hashlib.md5()
            m.update(ss)
            if csum == m.hexdigest():
                user.backend = "django.contrib.auth.backends.ModelBackend"
                login(request, user)
                next = request.GET.get('next', '/')
                return redirect(next)
            else:
                raise Http404
        except:
            raise Http404

autologin_view = AutoLoginView.as_view()
expert_registration = ExpertRegistration.as_view()
expert_list = ExpertList.as_view()
expert_list_ajax = ExpertListAjaxView.as_view()
expert_details = ExpertDetails.as_view()
recal_list = RecalList.as_view()
recal_add = RecalAdd.as_view()
consultation_add = ConsultationAdd.as_view()
consultation_details = ConsultationDetails.as_view()
workday_view = WorkDayView.as_view()
workday_item_view = WorkDayItemView.as_view()
workdays_ajax = WorkDayAjaxView.as_view()
check_interval = CheckIntervalView.as_view()
personal_card_ajax = PersonalCardView.as_view()
ask_experts_view = AskExpertAjaxView.as_view()
expert_questions_view = ExpertQuestionListView.as_view()