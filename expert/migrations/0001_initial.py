# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Issue'
        db.create_table('expert_issue', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('ckeditor.fields.RichTextField')(blank=True)),
            ('description_short', self.gf('django.db.models.fields.TextField')()),
            ('parent', self.gf('mptt.fields.TreeForeignKey')(blank=True, related_name='children', null=True, to=orm['expert.Issue'])),
            ('icon', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100, null=True, blank=True)),
            ('lft', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('rght', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('tree_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('level', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
        ))
        db.send_create_signal('expert', ['Issue'])

        # Adding model 'TimeTable'
        db.create_table('expert_timetable', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('expert', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
        ))
        db.send_create_signal('expert', ['TimeTable'])

        # Adding model 'TimeGap'
        db.create_table('expert_timegap', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('table', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['expert.TimeTable'])),
            ('dtstart', self.gf('django.db.models.fields.DateTimeField')(db_index=True)),
            ('dtstop', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal('expert', ['TimeGap'])

        # Adding model 'Consultation'
        db.create_table('expert_consultation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('client', self.gf('django.db.models.fields.related.ForeignKey')(related_name='consultation_client', to=orm['auth.User'])),
            ('expert', self.gf('django.db.models.fields.related.ForeignKey')(related_name='consultation_expert', to=orm['auth.User'])),
            ('dtstart', self.gf('django.db.models.fields.DateTimeField')()),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('status', self.gf('django.db.models.fields.CharField')(default=u'\u043d\u0430\u0437\u043d\u0430\u0447\u0435\u043d\u0430', max_length=255)),
        ))
        db.send_create_signal('expert', ['Consultation'])

        # Adding model 'Specialization'
        db.create_table('expert_specialization', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('ckeditor.fields.RichTextField')()),
        ))
        db.send_create_signal('expert', ['Specialization'])

        # Adding model 'Course'
        db.create_table('expert_course', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('institution', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['expert.Institution'])),
            ('course_date_start', self.gf('django.db.models.fields.DateField')()),
            ('course_date_stop', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal('expert', ['Course'])

        # Adding model 'Institution'
        db.create_table('expert_institution', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('expertblank', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['expert.ExpertBlank'])),
            ('instatution_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('institution_specialization', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('faculty', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('instatution_start', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('instatution_stop', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('degree', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
        ))
        db.send_create_signal('expert', ['Institution'])

        # Adding model 'StudyCenter'
        db.create_table('expert_studycenter', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('expertblank', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['expert.ExpertBlank'])),
            ('center_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('diploma_details', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('expert', ['StudyCenter'])

        # Adding model 'Association'
        db.create_table('expert_association', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('expertblank', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['expert.ExpertBlank'])),
            ('association_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
        ))
        db.send_create_signal('expert', ['Association'])

        # Adding model 'ExpertBlank'
        db.create_table('expert_expertblank', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('middle_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('birthday', self.gf('django.db.models.fields.DateField')(null=True)),
            ('personal', self.gf('django.db.models.fields.TextField')()),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('phone_number', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('country', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('avatar', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
            ('expert', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True, null=True, blank=True)),
            ('approved', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('datetime', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('username', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('expert', ['ExpertBlank'])

        # Adding M2M table for field issues on 'ExpertBlank'
        db.create_table('expert_expertblank_issues', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('expertblank', models.ForeignKey(orm['expert.expertblank'], null=False)),
            ('issue', models.ForeignKey(orm['expert.issue'], null=False))
        ))
        db.create_unique('expert_expertblank_issues', ['expertblank_id', 'issue_id'])

        # Adding model 'Recal'
        db.create_table('expert_recal', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('client', self.gf('django.db.models.fields.related.ForeignKey')(related_name='recal_client', to=orm['auth.User'])),
            ('expert', self.gf('django.db.models.fields.related.ForeignKey')(related_name='recal_expert', to=orm['auth.User'])),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('datetime', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('approved', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('expert', ['Recal'])


    def backwards(self, orm):
        # Deleting model 'Issue'
        db.delete_table('expert_issue')

        # Deleting model 'TimeTable'
        db.delete_table('expert_timetable')

        # Deleting model 'TimeGap'
        db.delete_table('expert_timegap')

        # Deleting model 'Consultation'
        db.delete_table('expert_consultation')

        # Deleting model 'Specialization'
        db.delete_table('expert_specialization')

        # Deleting model 'Course'
        db.delete_table('expert_course')

        # Deleting model 'Institution'
        db.delete_table('expert_institution')

        # Deleting model 'StudyCenter'
        db.delete_table('expert_studycenter')

        # Deleting model 'Association'
        db.delete_table('expert_association')

        # Deleting model 'ExpertBlank'
        db.delete_table('expert_expertblank')

        # Removing M2M table for field issues on 'ExpertBlank'
        db.delete_table('expert_expertblank_issues')

        # Deleting model 'Recal'
        db.delete_table('expert_recal')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'expert.association': {
            'Meta': {'object_name': 'Association'},
            'association_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'expertblank': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['expert.ExpertBlank']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        },
        'expert.consultation': {
            'Meta': {'object_name': 'Consultation'},
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'consultation_client'", 'to': "orm['auth.User']"}),
            'dtstart': ('django.db.models.fields.DateTimeField', [], {}),
            'expert': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'consultation_expert'", 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "u'\\u043d\\u0430\\u0437\\u043d\\u0430\\u0447\\u0435\\u043d\\u0430'", 'max_length': '255'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'expert.course': {
            'Meta': {'object_name': 'Course'},
            'course_date_start': ('django.db.models.fields.DateField', [], {}),
            'course_date_stop': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['expert.Institution']"})
        },
        'expert.expertblank': {
            'Meta': {'object_name': 'ExpertBlank'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'avatar': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'birthday': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'datetime': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'expert': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'issues': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['expert.Issue']", 'symmetrical': 'False'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'middle_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'personal': ('django.db.models.fields.TextField', [], {}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'expert.institution': {
            'Meta': {'object_name': 'Institution'},
            'degree': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'expertblank': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['expert.ExpertBlank']"}),
            'faculty': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instatution_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'instatution_start': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'instatution_stop': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'institution_specialization': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'expert.issue': {
            'Meta': {'object_name': 'Issue'},
            'description': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'description_short': ('django.db.models.fields.TextField', [], {}),
            'icon': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['expert.Issue']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'expert.recal': {
            'Meta': {'object_name': 'Recal'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'recal_client'", 'to': "orm['auth.User']"}),
            'datetime': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'expert': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'recal_expert'", 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        'expert.specialization': {
            'Meta': {'object_name': 'Specialization'},
            'description': ('ckeditor.fields.RichTextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'expert.studycenter': {
            'Meta': {'object_name': 'StudyCenter'},
            'center_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'diploma_details': ('django.db.models.fields.TextField', [], {}),
            'expertblank': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['expert.ExpertBlank']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'expert.timegap': {
            'Meta': {'object_name': 'TimeGap'},
            'dtstart': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'dtstop': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'table': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['expert.TimeTable']"})
        },
        'expert.timetable': {
            'Meta': {'object_name': 'TimeTable'},
            'expert': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['expert']