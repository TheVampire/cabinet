#coding: utf-8

from django.db import models

from ckeditor.fields import RichTextField
from sorl.thumbnail.fields import ImageField
from mptt.models import MPTTModel, TreeForeignKey

class Issue(MPTTModel):
    """
    Узел дерева проблем
    """
    class Meta:
        verbose_name = u'Типовая преблема'
        verbose_name_plural = u'Типовые проблемы'

    class MPTTMeta:
        order_insertion_by = ['name']

    name = models.CharField(max_length=255, verbose_name=u"название")
    description = RichTextField(
        verbose_name=u"описание",
        blank=True)

    description_short = models.TextField(
        verbose_name=u"короткое описание",blank=True)

    parent = TreeForeignKey(
        'self', null=True, blank=True, related_name='children',
        verbose_name=u"Является подтипом этой проблемы"
        )

    icon = ImageField(
        upload_to='problem_category',
        verbose_name=u"иконка",
        null=True,
        blank=True
        )

    def __unicode__(self):
        return self.name

    def for_name(self):
        if self.name == u'Взрослым': return u'Взрослых'
        if self.name == u'Родителям': return u'Родителей'
        if self.name == u'Семейным парам': return u'Семейных пар'
        if self.name == u'Подросткам': return u'Подростков'
        if self.name == u'Организациям': return u'Организаций'
        return self.name