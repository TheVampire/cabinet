from django.conf.urls import patterns, include, url


urlpatterns = patterns('expert.views',
    url("^autologin/$", "autologin_view", name='autologin-view'),
    url("^questions/$", "expert_questions_view", name='expert-questions-view'),
    url("^ask/$", "ask_experts_view", name='ask-experts'),
    url("^workdays-ajax/$", "workdays_ajax", name='workdays-ajax'),
    url("^personal-card-ajax/$", "personal_card_ajax", name='personal-card-ajax'),
    url("^check-interval-ajax/$", "check_interval", name='check-interval'),
    url("^recals/$", "recal_list", name='recal-list'),
    url("^recals/add/$", "recal_add", name='recal_add'),
    url("^consultations/add/$", "consultation_add", name='consultation_add'),
    url("^consultations/(\w+)/$", "consultation_details", name='consultation_details'),
    url("^registration/$", "expert_registration", name='expert-registration'),
    url("^ajax/workday/$", "workday_view", name='workday-view'),
    url("^ajax/workday/item/$", "workday_item_view", name='workday-item-view'),
    url("^ajax/$", "expert_list_ajax", name='expert-list-ajax'),
    url("^$", "expert_list", name='expert-list'),
    url("^(\w+)/$", "expert_details", name='expert-details'),
)
