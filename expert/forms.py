#coding: UTF-8
import string
import random
from string import ascii_letters, digits
from django.utils.html import escape, strip_tags

from django.contrib.auth.models import User
from django.conf import settings
from django.forms import (
    ModelForm,TextInput,Textarea,PasswordInput,
    CheckboxSelectMultiple,ValidationError, DateField, DateInput,
    )
from form_utils.forms import BetterForm, BetterModelForm
from stuff import widgets, fieldsets, FileInput
from captcha.fields import CaptchaField

from models import (
    ExpertBlank, Institution, Association, Consultation, StudyCenter, Recal, ExpertQuestion, ExpertAnswer
    )

from utils import create_expert
from expert.issues import Issue
import user
from django import forms

class ExpertAnswerForm(forms.ModelForm):
    class Meta:
        model = ExpertAnswer
        exclude = ['expert', 'question']

class ExpertQuestionForm(forms.ModelForm):
    username = forms.CharField(max_length=256, required=False)
    email = forms.CharField(max_length=256, required=True)
    captcha = CaptchaField()
    class Meta:
        model = ExpertQuestion

    def __init__(self, *args, **kwargs):
        super(ExpertQuestionForm, self).__init__(*args, **kwargs)
        self.fields['issue'].queryset = Issue.objects.filter(parent=None)
        self.fields['issue'].empty_label = u'Выберите категорию'

class ExpertBlankForm(BetterModelForm):
    birthday = DateField(
        label=u"День рождения",
        input_formats=["%d.%m.%Y",],
        help_text=u'11.11.1970',
        widget=DateInput(attrs={'class': 'validate[required]'}, format='%d.%m.%Y')
    )

    class Meta:
        model = ExpertBlank
        exclude = ('approved', 'expert', 'datetime')
        fieldsets = fieldsets
        widgets = widgets

    def __init__(self, *args, **kwargs):

        super(ExpertBlankForm, self).__init__(*args, **kwargs)
        #self.fields['issues'].queryset = Issue.objects.filter(parent=None)

    def clean(self, *args, **kwargs):
        data = super(ModelForm, self).clean(*args, **kwargs)
        if data.get("password") and data.get("password")!=data.get(
            "password_again"):
            raise ValidationError(u"проверьте правильность ввода пароля")

        if data.get("personal") and len(data.get("personal")) >= 1000:
            raise ValidationError(u"больше 1000 знаков личной информации")

        if not set(data["username"]).issubset(set(ascii_letters+digits+'_-')):
            raise ValidationError(
            u"""никнейм должен состоять только из английских букв, цифр, знаков 
            нижнего подчеркивания и тире""")

        if User.objects.filter(username=data["username"]).exists():
            raise ValidationError(u"""пользователь с таким псевдонимом 
            уже существует""")

        data['username'] = strip_tags(data['username'])
        data['first_name'] = strip_tags(data['first_name'])
        data['middle_name'] = strip_tags(data['middle_name'])
        data['last_name'] = strip_tags(data['last_name'])
        data['personal'] = strip_tags(data['personal'])
        data['city'] = strip_tags(data['city'])

        return data


class InstitutionForm(ModelForm):

    class Meta:
        model = Institution
        exclude = ('expertblank',)
        widgets = {
            'instatution_name': TextInput(attrs={'description': u'Вы можете также добавить сведения о \
                                                 дополнительном профессиональном\
                                                 образовании в своем Личном\
                                                 кабинете',
                                                 })
        }

    def clean(self, *args, **kwargs):
        data = super(InstitutionForm, self).clean(*args, **kwargs)
        data['instatution_name'] = strip_tags(data['instatution_name'])
        data['institution_specialization'] = strip_tags(data['institution_specialization'])
        return data


class AssociationForm(ModelForm):

    class Meta:
        model = Association
        exclude = ('expertblank',)


class StudyCenterForm(ModelForm):

    class Meta:
        model = StudyCenter
        exclude = ('expertblank',)


#This form overrides default admin form for ExpertBlank model
#need it to get approve/deny functionality
class ExpertBlankOverrideForm(ModelForm):

    def __init__(self, *a, **kw):
        super(ExpertBlankOverrideForm, self).__init__(*a, **kw)
        iss = Issue.objects.filter(parent__isnull=False)
        w = self.fields['issues'].widget
        choices = []
        for choice in iss:
            choices.append((choice.id, choice.parent.name + ' -> ' + choice.name))
        w.choices = choices

    def clean(self, *args, **kwargs):

        data = super(ExpertBlankOverrideForm, self).clean(*args, **kwargs)
        data.update(
            {"password":\
            ''.join(
                random.choice(
                    string.ascii_uppercase + string.digits) for x in range(10)
                    )
            }
            )

        #user = None

        if 'approve' in self.data:
            self.instance.expert = create_expert(data)
            self.instance.approved = True

        elif 'deny' in self.data:
            "@todo: not implemented"
            pass

        #data.update({"the_user": user})
        return data

    def save(self, *args, **kwargs):
        obj = super(ExpertBlankOverrideForm, self).save(commit=False)
        #obj.approved = True
        return obj


class RecalAddForm(ModelForm):
    class Meta:
        model = Recal
        exclude = ("client", "datetime", "approved",)


class ConsultatonAddForm(ModelForm):
    class Meta:
        models = Consultation
        exclude = ('status')
