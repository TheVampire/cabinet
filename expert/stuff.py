# coding: utf-8
#bunch of useful but ugly stuff

from django.forms import (
    ModelForm,TextInput,Textarea,PasswordInput,
    CheckboxSelectMultiple,ValidationError, DateField, DateInput, FileInput
    )


#ExpertBlankForm widgets
widgets = {
    'first_name': TextInput(attrs={'class': 'validate[required]'}),
    'last_name': TextInput(attrs={'class': 'validate[required]'}),
    'fix': TextInput(attrs={'class': 'validate[required, min[800]]',
                            'mh': 'min_height_100',
                            'description': u'Вы можете назначить любую стоимость, \
                            однако минимальная стоимость\
                            консультации составляет 800 рублей.\
                            Впоследствии вы сможете изменить это\
                                значение в своем Личном кабинете.'}),
    'consultation_time': TextInput(attrs={'class': 'validate[required]',
                                          'mh': 'min_height_100',
                                          'description': u'Вы можете на свое усмотрение установить\
                                          длительность онлайн консультации. В последствии Вы сможете\
                                           изменить это значение в своем Личном кабинете.'}),
#    'birthday': DateInput(attrs={'class': 'validate[required]'}, format='%d.%m.%Y'),
    'personal': Textarea(attrs={'cols': 200, 'rows': 10, 'class': 'validate[required,maxSize[1000]]',
                                'description': u'Напишите несколько слов о себе.Эту информацию увидят все наши\
    посетители. Объем информации ограничен 1000 знаками.*'}),
    'email': TextInput(attrs={'class': 'validate[required,custom[email,ajax[ajaxUserEmailCall]]'}),
    'phone_number': TextInput(attrs={'class': 'validate[required]'}),
    'username': TextInput(attrs={'description': u'Будет использоваться для входа в личный кабинет', 'class': 'validate[required,custom[onlyAZdigchar,ajax[ajaxUsernameCall]]'}),
    'specialization': CheckboxSelectMultiple(),
    'issues': CheckboxSelectMultiple(),
    'avatar': FileInput(attrs={
        'class': 'validate[required]',
        'description': u"Выберите фотографию, по которой вас будут узнавать наши посетители."
    })
}


#ExpertBlankForm fieldsets
fieldsets = [('first', {'fields': ['username', 'first_name', 'middle_name', 'last_name', 'birthday', 'avatar','country','city',
                                   'email', 'phone_number',
                                   'personal',
                                    'consultation_time','fix'
                                   , ]}),
             ('issues', {'fields': ['issues']}),
             ('last', {'fields': ['diplom', 'specialization']})
]