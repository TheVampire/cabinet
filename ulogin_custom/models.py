import datetime
import os

from django.db import models
from django.http import HttpResponse
from django.core.files.base import ContentFile
from django_ulogin.models import ULoginUser
from django_ulogin.signals import assign
from django.contrib.auth.models import User
import requests

from pytils import translit

ULOGIN_FIELDS = ['first_name', 'last_name', 'sex', 'email']


def catch_ulogin_signal(*args, **kwargs):
    user = kwargs['user']
    ulogin = kwargs['ulogin_user']
    json = kwargs['ulogin_data']

    if kwargs['registered']:
        user.first_name = json['first_name']
        user.last_name = json['last_name']
        user.email = json['email']

#        username = u"%s%s" % (
#            translit.translify(unicode(user.first_name)),
#            translit.translify(unicode(user.last_name)))
        username = user.username
        counter = User.objects.filter(username=username).filter(~models.Q(email=user.email)).count()

        if not counter:
            user.username = username
        else:
            user.username = u"%s_%d" % (username, counter)

        user.save()


assign.connect(
    receiver=catch_ulogin_signal,
    sender=ULoginUser,
    dispatch_uid='customize.models'
    )
