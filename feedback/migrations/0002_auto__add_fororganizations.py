# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ForOrganizations'
        db.create_table('feedback_fororganizations', (
            ('feedback_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['feedback.FeedBack'], unique=True, primary_key=True)),
            ('country', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('company_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('contact', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('feedback', ['ForOrganizations'])


    def backwards(self, orm):
        # Deleting model 'ForOrganizations'
        db.delete_table('feedback_fororganizations')


    models = {
        'feedback.feedback': {
            'Meta': {'object_name': 'FeedBack'},
            'admin_response': ('django.db.models.fields.TextField', [], {}),
            'datetime': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'feedback.fororganizations': {
            'Meta': {'object_name': 'ForOrganizations', '_ormbases': ['feedback.FeedBack']},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'company_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'contact': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'feedback_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['feedback.FeedBack']", 'unique': 'True', 'primary_key': 'True'})
        }
    }

    complete_apps = ['feedback']