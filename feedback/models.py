#coding: UTF-8

from django.db import models

from ckeditor.fields import RichTextField

from choices import TYPES
from expert.choices import COUNTRIES
from django.core.mail import send_mail

class FeedBack(models.Model):

    class Meta:
        verbose_name = u'Фидбэк'
        verbose_name_plural = u'Фидбэки'

    type = models.CharField(
        max_length=255, choices=TYPES, verbose_name=u"категория проблемы")
    text = models.TextField(verbose_name=u"описание проблемы")
    email = models.EmailField(verbose_name=u"адрес электронной почты")
    datetime = models.DateTimeField(auto_now=True)
    admin_response = models.TextField(verbose_name=u"ответ")

    def save(self, *args, **kwargs):
        super(FeedBack, self).save(*args, **kwargs)
        send_mail(u'Обратная связь - {}'.format(self.type), self.text, self.email, [u'support@kabinet1.ru'])

class ForOrganizations(FeedBack):

    class Meta:
        verbose_name = u'Информация из формы "оргинизациям"'
        verbose_name_plural = u'Информация из форм "оргинизациям"'

    country = models.CharField(
        max_length=255, verbose_name=u"страна",
        choices=COUNTRIES)
    company_name = models.CharField(
        verbose_name=u'название компании', max_length=255)
    contact = models.CharField(verbose_name=u'контактное лицо', max_length=255)
    city = models.CharField(verbose_name=u'город', max_length=255)
