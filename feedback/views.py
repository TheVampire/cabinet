import json

from django.views.generic import TemplateView, View
from django.shortcuts import render

from django.http import HttpResponse

from forms import FeedBackForm


class FeedBackAll(View):

    def get(self, request, form):

        feedback_form = form()
        if request.user.is_authenticated():
            feedback_form.fields["email"].initial = request.user.email

        return render(
            request, "feedback_form.html", {"feedback_form": feedback_form})

    def post(self, request, form):
        feedback_form = form(request.POST)
        if feedback_form.is_valid():
            feedback = feedback_form.save(commit=False)
            feedback.save()

            return render(
                request, "feedback_form_ok.html", {})

        else:
            return render(
                request, "feedback_form.html", {"feedback_form": feedback_form})

feedback_all = FeedBackAll.as_view()
