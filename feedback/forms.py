#coding: UTF-8

from django.forms import (
    ModelForm, TextInput, Textarea, Select,
    )

from django.forms import CharField
from ckeditor.widgets import CKEditorWidget

from core.utils import send_email

from models import FeedBack, ForOrganizations


class FeedBackForm(ModelForm):

    class Meta:
        model = FeedBack
        exclude = ('admin_response', 'datetime')
        widgets = {
            'email': TextInput(attrs={'class': 'validate[required]'}),
            'text': Textarea(attrs={'class': 'validate[required]'}),
            'type': Select(attrs={'class': 'validate[required]'}),
        }


class FeedBackOrganizations(FeedBackForm):

    class Meta:
        model = ForOrganizations


class FeedBackOverrideForm(ModelForm):

    def clean(self, *args, **kwargs):

        data = super(FeedBackOverrideForm, self).clean(*args, **kwargs)

        if 'response' in self.data:
            send_email(
                subject=u"Заявка о рассмотрении ошибки",
                email=self.data['email'],
                template_name="feedback_response.txt",
                _context={
                    'email_adress': self.data['email'],
                    'text_request': self.data['text'],
                    'text_response': self.data['admin_response'],
                    }
                )

        return data
