from django.conf.urls import patterns, include, url
from forms import FeedBackForm

urlpatterns = patterns('feedback.views',
    url("^$", 'feedback_all', name='feedback-all', kwargs={"form": FeedBackForm}),
)
