from django.contrib.admin import site, ModelAdmin

from models import FeedBack
from forms import FeedBackOverrideForm


class FeedBackAdmin(ModelAdmin):

    form = FeedBackOverrideForm
    list_display = ("email", )


site.register(FeedBack, FeedBackAdmin)
