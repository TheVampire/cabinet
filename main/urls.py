from django.conf.urls import patterns, include, url
from main.views import MainPage,ChatPage,ServicePage

urlpatterns = patterns('',
    url("^$", MainPage.as_view(), name='main'),
    url("^chat/$", ChatPage.as_view(), name='chat'),
#    url("^service/$", ServicePage.as_view(), name='service'),
)
