from django.views.generic import TemplateView, View
from django.shortcuts import render
from django.contrib.auth.models import User
from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator

from library.models import LibItem
from news.models import News
from expert.models import Recal, Issue, ExpertQuestion
from expert.forms import ExpertQuestionForm
from core.models import Banner
from core.utils import get_or_none
from pybb.models import Post

class MainPage(View):

#    @method_decorator(cache_page(60*15))
    def dispatch(self, *args, **kwargs):
        return super(MainPage, self).dispatch(*args, **kwargs)

    def get(self, request):
        return render(
            request,
            "main.html",
            {
                'right_banner': get_or_none(Banner, path=request.path, place='R'),
                "news": News.objects.all(),
                "experts": User.objects.filter(groups__name='experts')[:18],
                "recals": Recal.objects.all(),
                'categories': Issue.objects.filter(parent=None),
                "experts_online": User.objects.filter(
                    profile__online=True, groups__name="experts"
                    ),
                "lib_items": LibItem.objects.all().order_by("-datetime")[:3],
                "expert_question_form": ExpertQuestionForm(),
                'posts': Post.objects.order_by('-created')[:10],
                'expert_questions': ExpertQuestion.objects.order_by('-created')[:4]
            }
            )


class ChatPage(TemplateView):
    template_name = "chat.html"


class ServicePage(TemplateView):
    template_name = "service.html"