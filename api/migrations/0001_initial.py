# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Config'
        db.create_table('api_config', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('rtmp_url', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('balance_timeout', self.gf('django.db.models.fields.IntegerField')()),
            ('consultation_active_check_timeout', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('api', ['Config'])


    def backwards(self, orm):
        # Deleting model 'Config'
        db.delete_table('api_config')


    models = {
        'api.config': {
            'Meta': {'object_name': 'Config'},
            'balance_timeout': ('django.db.models.fields.IntegerField', [], {}),
            'consultation_active_check_timeout': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rtmp_url': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        }
    }

    complete_apps = ['api']