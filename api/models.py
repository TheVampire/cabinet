# coding: utf-8
__author__ = 'vampire'
from django.db import models
from django.contrib.auth.models import User

class Config(models.Model):
    rtmp_url = models.CharField(max_length=256, verbose_name=u'Адрес медиа-сервера')
    balance_timeout = models.IntegerField(verbose_name=u'Интервал запроса о балансе')
    consultation_active_check_timeout = models.IntegerField(verbose_name=u'Интервал запроса информации о консультации во время звонка')

    class Meta:
        verbose_name = u'Первичная настройка консультации'
        verbose_name_plural = u'Первичные настройки консультации'

    def __unicode__(self):
        return u'Настройка консультации'