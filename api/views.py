# coding: utf-8
__author__ = 'vampire'

from django.core.urlresolvers import reverse
from django.views.generic import View
from django.conf import settings
from django.http import HttpResponse
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from api.models import Config
from sprypay.models import Balance
from expert.models import Consultation
from django_settings.models import Setting
import datetime, json, pytz
from django.db.models import Q
from django.core.cache import cache
import decimal
from core.templatetags.util import intspace
from core.utils import send_notify, send_email_with_done_consultation
from sprypay.models import refill, Operation

class ConsultationView(View):

    def is_expert(self, u):
        return True if u.groups.filter(name='experts').count() > 0 else False

    def is_client(self, u):
        return True if u.groups.filter(name='clients').count() > 0 else False

#    def get(self, request):
#        data = {}
#        if request.user.is_authenticated():
#        return HttpResponse(
#            json.dumps(data),
#            mimetype='application/json'
#        )
    def post(self, request, *a, **kw):
        data = {}
        consl = None
        local_now = timezone.localtime(timezone.now())
        if request.user.is_authenticated():
            if self.is_client(request.user):
                consl = Consultation.objects.filter(
                    client=request.user,
                    dtstart__gt=datetime.datetime.now() - datetime.timedelta(hours=1)
                ).filter(Q(status='approved')|Q(status='stopped')).order_by('dtstart')
                if consl.count() > 0:
                    consl = consl[0]
            if self.is_expert(request.user):
                consl = Consultation.objects.filter(
                    expert=request.user,
                    dtstart__gt=datetime.datetime.now() - datetime.timedelta(hours=1)
                ).filter(Q(status='approved')|Q(status='stopped')).order_by('dtstart')
                if consl.count() > 0:
                    consl = consl[0]
            if consl:
                if local_now > timezone.localtime(consl.dtstart) - datetime.timedelta(hours=1):
                    n_dt = consl.dtstart.astimezone(pytz.timezone(settings.TIME_ZONE))
                    photo = ''
                    name = ''
                    if self.is_client(request.user):
                        if consl.client.profile.photo:
                            photo = consl.client.profile.photo.url
                        if consl.expert.first_name and consl.expert.last_name:
                            name = u'{} {}'.format(consl.expert.first_name, consl.expert.last_name)
                        else:
                            name = consl.expert.username
                    if self.is_expert(request.user):
                        if consl.expert.profile.photo:
                            photo = consl.expert.profile.photo.url
                        if consl.client.first_name and consl.client.last_name:
                            name = u'{} {}'.format(consl.client.first_name, consl.client.last_name)
                        else:
                            name = consl.client.username
                    data = {
                        'rcpt_name': name,
#                        'rcpt_firstname': consl.client.first_name if self.is_expert(request.user) else consl.expert.first_name,
#                        'rcpt_lastname': consl.client.last_name if self.is_expert(request.user) else consl.expert.last_name,
                        'rcpt_photo_url': photo,
                        'consultation_start': n_dt.strftime('%Y-%m-%d-%H-%M'),
                        'consultation_stop': (n_dt + datetime.timedelta(minutes=consl.length)).strftime('%Y-%m-%d-%H-%M'),
                        'consultation_id': consl.pk,
                        'consultation_status': consl.status,
                        'success': 'true'
                    }
                else:
                    data = {
                        'success': 'true',
                        'error': u'Нет ни одной консультации в данный временной период'
                    }
            else:
                data = {
                    'success': 'true',
                    'error': u'Консультация не найдена'
                }
            if request.POST.get("go_on"):
                c = Consultation.objects.get(pk = request.POST.get("go_on"))
                c.status = u'approved'
                c.save()
                data['status'] = u'Go on'
            if request.POST.get("consultation_id") and request.POST.get("consultation_start"):
                c = Consultation.objects.get(pk=request.POST.get("consultation_id"))
                if c:
                    c.dtstart = datetime.datetime.strptime(
                        request.POST.get("consultation_start"),
                        "%Y-%m-%d-%H-%M"
                    )
                    c.save()
        else:
            data = {
                'success': 'false',
                'error': u'Пользователь на авторизован'
            }
        return HttpResponse(
            json.dumps(data),
            mimetype='application/json'
        )

class BalanceView(View):
    def post(self, request):
        data = {}
        if request.user.is_authenticated():
            if Balance.objects.filter(user=request.user).count() == 0:
                Balance.objects.create(
                    user=request.user,
                    sum=0.0
                )
            balance = Balance.objects.get(user=request.user)
            warning = 0 if balance.sum > Setting.objects.get_value('balance_warning_limit') else 1
            data = {
                'balance': intspace(int(balance.sum)),
                'warning': warning,
                'success': 'true'
            }
            if request.user.groups.filter(name="clients").count() > 0:
                consl = Consultation.objects.get(pk=request.POST.get('consultation_id'))
                spend_time = int(request.POST.get('spend_time', 0))
                if spend_time != consl.spend_time:
                    diff = spend_time - consl.spend_time
                    consl.spend_time = spend_time
                    consl.save()
                    decimal.getcontext().prec = 2

                    if Balance.objects.filter(user=consl.expert).exists():
                        balance_expert = Balance.objects.get(
                            user=consl.expert
                        )
                    else:
                        balance_expert = Balance.objects.create(
                            user=consl.expert,
                            sum=0.0
                        )
                    if Balance.objects.filter(user=consl.client).exists():
                        balance_client = Balance.objects.get(
                            user=consl.client
                        )
                    else:
                        balance_client = Balance.objects.create(
                            user=consl.client,
                            sum=0.0
                        )
#                    print diff
                    diff = (decimal.Decimal(diff)/decimal.Decimal(3600))*decimal.Decimal(consl.expert.expertblank.fix)
#                    print diff
#                    refill(consl.client, -diff)
#                    refill(consl.expert, diff)
#                    balance_client.sum -= diff
#                    balance_expert.sum += diff
                    f_diff = float(diff)
                    f_b_c = float(balance_client.sum)
                    f_b_e = float(balance_expert.sum)
                    f_b_c -= f_diff
                    f_b_e += f_diff
#                    print f_diff
#                    print f_b_c
#                    print f_b_e
                    balance_client.sum = f_b_c
                    balance_expert.sum = f_b_e
#                    print balance_client.sum
#                    print balance_expert.sum
                    if balance_client.sum < 0:
                        balance_client.sum = 0
                    balance_client.save()
                    balance_expert.save()
                    if request.user.groups.filter(name='clients').count() > 0:
                        warning = 0 if balance_client.sum > Setting.objects.get_value('balance_warning_limit') else 1
                        data = {
                            'balance': intspace(int(balance_client.sum)),
                            'warning': warning,
                            'success': 'true'
                        }
                else:
                    data = {
                        'success': 'false',
                        'error': u'Время консультации прошло'
                    }
        else:
            data = {
                'success': 'false',
                'error': u'Пользователь не авторизован'
            }
        return HttpResponse(
            json.dumps(data), mimetype='application/json'
        )

class ConfigView(View):

    def post(self, request, *a, **kw):
        data = {}
        if request.user.is_authenticated():
            conf = Config.objects.all()[0]
            role = None
            if request.user.groups.filter(name='clients').count() > 0:
                role = 0
            if request.user.groups.filter(name='experts').count() > 0:
                role = 1
            data = {
                'rtmp_url': conf.rtmp_url,
                'role': role,
                'time': datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S'),
                'balance_timeout': conf.balance_timeout,
                'consultation_active_check_timeout': conf.consultation_active_check_timeout,
                'user_id': request.user.pk,
                'success': 'true'
            }
        else:
            data = {
                'success': 'false',
                'error': u'Пользователь не авторизован'
            }
        return HttpResponse(
            json.dumps(data), mimetype='application/json'
        )

class StopConsultationView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(StopConsultationView, self).dispatch(*args, **kwargs)

    def post(self, request, *a, **kw):
        data = {}
        if request.POST.get("consultation_id"):
            c = Consultation.objects.get(pk=request.POST.get("consultation_id"))
            c.status = u'stopped'
            c.stop_time = timezone.localtime(timezone.now())
            c.save()
            data['status'] = u'Stopped'
            data.update({
                'success': 'true'
            })
        else:
            data = {
                'success': 'false',
                'error': u'Параметр consultation_id не задан'
            }
        return HttpResponse(
            json.dumps(data), mimetype='application/json'
        )

class CheckStopConsultationView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CheckStopConsultationView, self).dispatch(*args, **kwargs)

    def get(self, request, *a, **kw):
        data = {}
        for c in Consultation.objects.filter(
            status=u'stopped'
        ):
            if c.stop_time + datetime.timedelta(seconds=60*5) < timezone.localtime(timezone.now()):
                c.status = u'done'
                c.length = float(c.spend_time)/60.0 # convert to minutes
                notify_msg =\
        u"""
        Консультация окончена. Вы можете оставить отзыв специалисту. <a href="{}">Нажмите здесь</a>
        """.format(reverse('expert-details', args=[c.expert.username]))
                send_notify(c.client, notify_msg)
                send_email_with_done_consultation(c)
                ex_name = u'{} {}'.format(c.expert.first_name, c.expert.last_name)\
                if c.expert.first_name and c.expert.last_name else c.expert.username
                cl_name = u'{} {}'.format(c.client.first_name, c.client.last_name)\
                if c.client.first_name and c.client.last_name else c.client.username
                Operation.objects.create(
                    sum=-1*(float(c.spend_time)/3600.0)*float(c.expert.expertblank.fix),
                    user=c.client,
                    type=u'покупка консультации',
                    comment=u'Оплата консультации от специалиста {}'.format(ex_name),
                    status=u'оплачено'
                )
                Operation.objects.create(
                    sum=(float(c.spend_time)/3600.0)*float(c.expert.expertblank.fix),
                    user=c.expert,
                    type=u'оплата клиентом консультации',
                    comment=u'Оплата консультации от клиента {}'.format(cl_name),
                    status=u'оплачено'
                )
                c.save()
        return HttpResponse(
            json.dumps(data), mimetype='application/json'
        )

class DoneConsultationView(View):

    def post(self, request, *a, **kw):
        data = {}
        if request.POST.get("consultation_id"):
            c = Consultation.objects.get(pk=request.POST.get("consultation_id"))
            c.status = u'done'
            c.length = float(c.spend_time)/60.0
            notify_msg = \
u"""
Консультация окончена. Вы можете оставить отзыв специалисту. <a href="{}">Нажмите здесь</a>
""".format(reverse('expert-details', args=[c.expert.username]))
            send_notify(c.client, notify_msg)
            send_email_with_done_consultation(c)
#            balance_expert = Balance.objects.get(user=c.expert)
#            balance_client = Balance.objects.get(user=c.client)
#            summ = (c.expert.fix*c.spend_time)/60.0
#            balance_client.sum -= summ
#            balance_expert.sum += summ
#            balance_client.save()
#            balance_expert.save()
            ex_name = u'{} {}'.format(c.expert.first_name, c.expert.last_name) \
            if c.expert.first_name and c.expert.last_name else c.expert.username
            cl_name = u'{} {}'.format(c.client.first_name, c.client.last_name) \
            if c.client.first_name and c.client.last_name else c.client.username
            Operation.objects.create(
                sum=-1*(float(c.spend_time)/3600.0)*float(c.expert.expertblank.fix),
                user=c.client,
                type=u'покупка консультации',
                comment=u'Оплата консультации от специалиста {}'.format(ex_name),
                status=u'оплачено'
            )
            Operation.objects.create(
                sum=(float(c.spend_time)/3600.0)*float(c.expert.expertblank.fix),
                user=c.expert,
                type=u'оплата клиентом консультации',
                comment=u'Оплата консультации от клиента {}'.format(cl_name),
                status=u'оплачено'
            )
            c.save()
            data['status'] = u'Done'
            data.update({
                'success': 'true'
            })
        else:
            data = {
                'success': 'false',
                'error': u'Параметр consultation_id не задан'
            }
        return HttpResponse(
            json.dumps(data), mimetype='application/json'
        )

class NotificationDelView(View):
    def get(self, request, *a, **kw):
        rt = ''
        key = request.GET.get("key")
        if key:
            rt = 'Key found'
            cache_key = "messages-for-{}".format(self.request.user.pk)
            if cache.get(cache_key):
                rt = 'Cache key found'
                messages = json.loads(cache.get(cache_key))
                for i, m in enumerate(messages):
                    if key in m:
                        messages.remove(m)
                        rt = 'Removed'
                cache.set(cache_key, json.dumps(messages), 3600)
        return HttpResponse(rt)

class NotificationView(View):
    def get(self, request, *a, **kw):
        r = ''
        if request.user.is_authenticated():
            cache_key = "messages-for-{}".format(self.request.user.pk)
            if cache.get(cache_key):
                r = 'Cache key found'
                messages = json.loads(cache.get(cache_key))
                msg_dict = {}
                for m in messages:
                    msg_dict.update(m)
                r = json.dumps(msg_dict)
        return HttpResponse(
            r,
            mimetype='application/json'
        )

config_view = ConfigView.as_view()
balance_view = BalanceView.as_view()
consultation_view = ConsultationView.as_view()
stop_consultation_view = StopConsultationView.as_view()
done_consultation_view = DoneConsultationView.as_view()
check_stopped_consultation_view = CheckStopConsultationView.as_view()
notification_del_view = NotificationDelView.as_view()
notification_view = NotificationView.as_view()