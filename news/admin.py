from django.contrib.admin import site, ModelAdmin

from models import News, SEOStuff


class NewsAdmin(ModelAdmin):

    list_display = ("title", "datetime",)
    prepopulated_fields = {'slug': ('title',)}


site.register(News, NewsAdmin)
site.register(SEOStuff)
