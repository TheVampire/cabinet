# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'News.some_f'
        db.delete_column('news_news', 'some_f')


    def backwards(self, orm):
        # Adding field 'News.some_f'
        db.add_column('news_news', 'some_f',
                      self.gf('django.db.models.fields.TextField')(default='123'),
                      keep_default=False)


    models = {
        'news.news': {
            'Meta': {'ordering': "('-datetime',)", 'object_name': 'News'},
            'body': ('ckeditor.fields.RichTextField', [], {}),
            'datetime': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['news']