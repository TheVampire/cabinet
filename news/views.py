
from django.views.generic import View, ListView, DetailView
from django.shortcuts import render, get_object_or_404


from models import News


class NewsList(ListView):

    queryset = News.objects.filter()
    context_object_name = "news"
    template_name = "news_list.html"
    paginate_by = 10


class NewsDetails(DetailView):
    model = News
    context_object_name = 'novelty'
    template_name = 'news_details.html'

    # def get(self, request, news_id):
    #     novelty = get_object_or_404(News, id=news_id)
    #     return render(request, "news_details.html", {"novelty": novelty})


news_list = NewsList.as_view()
news_details = NewsDetails.as_view()
