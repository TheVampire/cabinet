#coding: UTF-8

from django.db import models
from ckeditor.fields import RichTextField
from sorl.thumbnail.fields import ImageField
from django.contrib.auth.models import User
# Create your models here.
import time

def make_upload_path_avatar(instance, filename):
    return 'images/{}.{}'.format(time.time(), filename.split('.')[-1])

class SEOStuff(models.Model):
    url = models.TextField(verbose_name=u'URL')
    title = models.TextField(verbose_name=u'Title', blank=True)
    description = models.TextField(verbose_name=u'Description', blank=True)
    keywords = models.TextField(verbose_name=u'Keywords', blank=True)

    def __unicode__(self):
        return self.url

    class Meta:
        verbose_name = u'Замена SEO тегов для ссылок'
        verbose_name_plural = u'Замены SEO тегов для ссылок'


class News(models.Model):

    """
    Модель новости
    """
    class Meta:
        verbose_name = u"Новость"
        verbose_name_plural = u"Новости"
        ordering = ('-datetime',)

    def __unicode__(self):
        return self.title

    title = models.CharField(max_length=255, verbose_name=u'заголовок')
    slug = models.CharField(max_length=256, verbose_name=u'Slug', blank=True)
    body = RichTextField(verbose_name=u'тело')
    image = ImageField(upload_to=make_upload_path_avatar, verbose_name=u'картинка')
    datetime = models.DateTimeField(
        db_index = True,
       auto_now_add=True,
        verbose_name=u'дата'
        )
    author = models.ForeignKey(User, verbose_name=u'Автор')
