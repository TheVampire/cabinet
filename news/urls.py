from django.conf.urls import patterns, include, url


urlpatterns = patterns('news.views',
    url("^$", "news_list", name='news-list'),
    url("^(?P<pk>\d+)/$", "news_details", name='news-details'),
    url("^(?P<slug>[-\w]+)/$", "news_details", name='news-details-slug'),
)
