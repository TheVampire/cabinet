$(function(){
    $.widget("ui.popupform", {
        options: {
            wrapper: '<div class="%class-wrapper%"><div class="ui-content"></div><span class="close"></span></div>',
            klass: 'auth-popup',
            duration: 300,
            setPosition: function(){
                var left = $(this).offset().left,
                    top = $(this).offset().top;

                return {
                    'left': left,
                    'top': top + 50
                }
            }
        },
        _is_first_run: true,
        _create: function(){
            var self = this,
                options = self.options;

            this.popup = $( options.wrapper.replace( /\%class-wrapper\%/g, options.klass ) );

            this.element.click(function(e){
                e.preventDefault();
                setTimeout(function(){
                    self.getContent();
                    self.showPopup();
                }, 10);
            });

            $(document).on("keyup", function(e){
                if (e.keyCode == 27){
                    self.hidePopup();
                }
            })

            $("body").click(function(){
                self.hidePopup();
            });

            this.popup.on("click", ".close", function(e){
                e.preventDefault();
                self.hidePopup();
            }).on("submit", "form", function(e){
                    e.preventDefault();
                    self.submitForm(this);
                }).on("click", function(e){
                    e.stopPropagation();
                });
        },
        _getPosition: function (){
            var el = this.element;

            return this.options.setPosition.call(el);
        },
        submitForm: function(el){
            var self = this,
                form = $(el),
                data = form.serialize();

            $.ajax({
                url: form.attr("action"),
                data: data,
                type: form.attr("method").toUpperCase(),
                success: function(data) {
                    if (data.status) {
                        window.location.reload();
                    } else {
                        self.setContent(data.template);
                    }
                }
            })
        },
        setContent: function(template) {
            var self = this;
            $(".ui-content", self.popup).html(template);
        },
        getContent: function(){
            var self = this,
                opts = self.options,
                url = $(self.element).attr("href") || self.options.url;


            $.ajax({
                url: url,
                cache: false,
                success: function(data){
                    self.setContent(data.template);
                }
            });
        },
        showPopup: function() {
            if (this._is_first_run) {
                this.popup.appendTo("body");
                this._is_first_run = false;
            }
            this.popup.css(this._getPosition()).fadeIn(this.options.duration);
            if (this.options.afterPopup) this.options.afterPopup();
        },
        hidePopup: function(){
            this.popup.fadeOut(this.options.duration);
        }
    });
});