#coding: UTF-8
from django.conf.urls import patterns, include, url
from django.conf import settings

from django.contrib.auth.views import login
from django.contrib import admin
from django.views.generic import TemplateView
from expert.models import ModerateInstitution, ModerateIssues, ModeratePrivateData
from sms.views import sms_response_view
from registration.backends.default.views import RegistrationView, ActivationView
#from registration.views import register, activate
from registration.forms import RegistrationFormUniqueEmail
#from core import MyDefaultBackend
from core.views import RegistrationCompleteView, CustomRegistrationView, RegistrationLoginView

from core.forms import patch_form, HELPS, MyRegForm


admin.autodiscover()

handler403 = 'core.views.permission_denied'
handler404 = 'core.views.page_not_found'
handler500 = 'core.views.server_error'

urlpatterns = patterns('',
    url(r"^", include("validator.urls")),
    url(r"^", include("main.urls")),
    url(r"^experts/", include("expert.urls")),
    url(r"^wizzard/", include("wizzard.urls")),
    url(r"^news/", include("news.urls")),
    url(r"^feedback/", include("feedback.urls")),
    url(r"^library/", include("library.urls")),
    url(r"^groups/", include("target.urls")), #target groups
    url(r'^comments/', include('django.contrib.comments.urls')),

    # Simple Wizzard
#    url(r'^s_wizzard/(?P<step>.+)/$', 'wizzard.views.simple_wizzard_view', name='simple_wizzard'),

    # FAQ
    url(r'^faq4expert/$', 'core.views.faq_view', name='faq-view'),
    url(r'^faq4client/$', 'core.views.faq_view', name='faq-view'),

    # sms
    url(r'^sms/response/$', sms_response_view, name='sms-response-view'),

    # for cron
    url(r'^check/consultation/approved/$', "core.views.check_consultation_approved", name='check-consultation-approved-view'),

    # for recal-spec
    url(r'^consultation/recal/ajax/$', "core.views.recal_after_consultation_view", name='recal-after-consultation-view'),

    # api
    url(r'^api/notification/$', 'api.views.notification_view', name='notification-view'),
    url(r'^api/notification/del/$', 'api.views.notification_del_view', name='notification-del-view'),
    url(r'^api/config/$', 'api.views.config_view', name='api-config-view'),
    url(r'^api/balance/$', 'api.views.balance_view', name='api-balance-view'),
    url(r'^api/consultation/$', 'api.views.consultation_view', name='api-consultation-view'),
    url(r'^api/stop_consultation/$', 'api.views.stop_consultation_view', name='api-stop-consultation-view'),
    url(r'^api/done_consultation/$', 'api.views.done_consultation_view', name='api-done-consultation-view'),
    url(r'^api/check_stopped_consultation/$', 'api.views.check_stopped_consultation_view', name='api-check-stopped-consultation-view'),

    # captcha
    url(r'^captcha/', include('captcha.urls')),

    #common instruments
    url(r'^accounts/profile/$', 'core.views.profile_view', name="profile-view"),
    url(r'^accounts/messages/$', 'core.views.messages_view', name="messages-view"),
    url(r'^accounts/mode/$', 'core.views.mode_view', name="mode-view"),
    url(r'^accounts/consultation/$', 'core.views.consultation_view', name="consultation-view"),
    url(r'^accounts/balance/$', 'core.views.balance_view', name="balance-view"),
    url(r'^accounts/myclients/$', 'core.views.my_clients_view', name="my-clients-view"),
    url(r'^accounts/schedule/$', 'core.views.schedule_view', name="schedule-view"),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    # url(
    #     r'^accounts/basic/register/$',
    #     CustomRegistrationView.as_view(
    #         form_class=patch_form(MyRegForm, HELPS)
    #     ),
    #     {'backend': 'registration.backends.default.DefaultBackend'},
    #     name='registration_register'
    #     ),
    url(r'^accounts/basic/register/complete/$',
        RegistrationCompleteView.as_view(),
        name='registration_complete'),
#    url(r'^accounts/basic/activate/complete/$',
#        TemplateView.as_view(template_name='registration/activation_complete.html'),
#        name='registration_activation_complete'),
#    url(r'^accounts/basic/activate/(?P<activation_key>\w+)/$',
#        ActivationView.as_view(),
#        {'backend': 'core.MyDefaultBackend'},
#        name='registration_activate'),

    url(r'^accounts/basic/', include('registration.urls')),
    url(r'^accounts/ulogin/', include('django_ulogin.urls')),

    url(r'^login/$', RegistrationLoginView.as_view(), name="login"),

    url(r'^superuser/jsi18n/$', admin.site.i18n_javascript),
    url(r'^superuser/', include(admin.site.urls), {'extra_context': {'moderate_institutuin': lambda: ModerateInstitution.objects.all(),
                                                                     'moderate_issues': lambda: ModerateIssues.objects.all(),
                                                                     'moderate_privatedata': lambda: ModeratePrivateData.objects.all()}}),
    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),

    url(
        r'^media/(?P<path>.*)$',
         'django.views.static.serve',
         {
            'document_root': settings.MEDIA_ROOT,
            'show_indexes': True
         }
        ),

    url(
        r'^static/(?P<path>.*)$',
         'django.views.static.serve',
         {
            'document_root': settings.STATIC_ROOT,
            'show_indexes': True
         }
        ),

    url(r'^forums/', include('pybb.urls', namespace='pybb')),
    url('^onpay/', include('onpay.urls')),
    url(r'^sprypay/', include('sprypay.urls')),
    url(r'^', include('django.contrib.flatpages.urls')),
)
