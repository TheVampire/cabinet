# coding: UTF-8
# Django settings for CABINET project.
import os
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS
from django.forms import ValidationError

AKISMET_API_KEY = '22b6724edf4d'

def my_check_text(user, body):
    if u'url=http://' in body or u'href=http://' in body:
        raise ValidationError(u'На этом форуме запрещена вставка ссылок на внешние ресурсы')

def check_text(user, body):
    try:
        from akismet import Akismet
    except:
        return

    ak = Akismet(
        key = AKISMET_API_KEY,
        blog_url = 'http://kabinet1.ru/'
    )
    if ak.verify_key():
        data = {
            'user_ip': '127.0.0.1',
            'user_agent': ''
        }
        if ak.comment_check(body.encode('utf-8'), data=data, build_data=True):
            raise ValidationError(u'Это сообщение похоже на СПАМ')
    else:
        raise ValidationError('NO API KEY GOOD')

ADMINS = (
    ('Alexander Lifanov', 'lifanov.a.v@gmail.com'),
    ('Alexey 4kusnik', '4.kusnik@gmail.com'),
    ('Support', 'support@kabinet1.ru'),
#    ('magniff', 'magniff@etsoft.ru'),
#    ('Alexx', 'a.voronkin@etsoft.ru'),
#    ('a.nikitin', 'a@nikitin.name'),
)

PROJECT_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(__file__), os.pardir, os.pardir))
#print PROJECT_ROOT
MANAGERS = ADMINS
DEFAULT_FROM_EMAIL = u'Кабинет №1 <support@kabinet1.ru>'
SERVER_EMAIL = DEFAULT_FROM_EMAIL
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Moscow'
BIRTHDAY_INPUT_FORMAT = ('%Y.%m.%d',)
# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'ru-ru'

USER_LANGUAGE_ACTIONS = 'djanguage.profile'

LANGUAGES = (
                ('ru', u'Russian'),
                ('en', u'English'),
            )

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'core.loaders.myloader.Myloader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    'pybb.middleware.PybbMiddleware',
    'core.middleware.TimezoneMiddleware',
    'core.middleware.ActiveUserMiddleware',
#    'gatekeeper.middleware.GatekeeperMiddleware',
)
USER_ONLINE_TIMEOUT = 300
USER_LASTSEEN_TIMEOUT = 60 * 60 * 24 * 7

LOCALE_PATHS = (
    '/home/vampire/PycharmProjects/cabinet/conf/locale/',
)

#need this for forum
CONTEXT_PROCESSORS = (
                    "django.core.context_processors.i18n",
                    #'pybb.context_processors.processor',
                     )

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
    'pybb.context_processors.processor',
    'core.context_processors.insert_current_date',
)

NOTIFICATIONS_STORAGE = 'session.SessionStorage'

ROOT_URLCONF = 'cabinet.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'cabinet.wsgi.application'

TEMPLATE_DIRS = (
)

#import gatekeeper
#GATEKEEPER_DEFAULT_STATUS = gatekeeper.APPROVED_STATUS

INSTALLED_APPS = (
#    'admintools_bootstrap',

    # admin_tools
#    'admin_tools',
#    'admin_tools.theming',
#    'admin_tools.menu',
#    'admin_tools.dashboard',

    # dist
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.comments',
    'django.contrib.flatpages',
    "django.contrib.formtools",

    # third party
    'registration',
    'django_ulogin',
    'ckeditor',
    'sorl.thumbnail',
    'mptt',
    'feincms',
    'widget_tweaks',
    'django_settings',
    'googlecharts',

#    'gatekeeper',
    # cabinet
    'core',
    'pybb',
    'onpay',
    'main',
    'ulogin_custom',
    'expert',
    'news',
    'dashboard',
    'feedback',
    'wizzard',
    'library',
    'validator',
    'target',
    'sms',
    'api',
    'sprypay',
    'djrill',
    'captcha',

    'django.contrib.admin',
    'south',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

#------------------------------------------------------------------------------
#ULOGIN STUFF
ULOGIN_FIELDS = ['first_name', 'last_name', 'sex', 'email']
ULOGIN_OPTIONAL = ['photo', 'photo_big', 'city', 'country', 'bdate']
ULOGIN_SCHEMES = {
    'default': {
        'DISPLAY': 'panel',
        'PROVIDERS': [
            "vkontakte",
            "facebook",
            "twitter",
            "google",
            "mailru",
            "livejournal"],
        #'HIDDEN': [ "yandex", "mailru", 'livejournal'],
    },
    'small_google': {
        'DISPLAY': 'small',
        'PROVIDERS': ["google"],
        'HIDDEN': [],
    }
}
#ULOGIN_CALLBACK = "function(){console.log('login')}"
#------------------------------------------------------------------------------
#CKEDITOR
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': [
            [      'Undo', 'Redo',
              '-', 'Bold', 'Italic', 'Underline',
              '-', 'Link', 'Unlink', 'Anchor',
              '-', 'Format',
              '-', 'SpellChecker', 'Scayt',
              '-', 'Maximize',
              '-', 'Image',

            ],
            [      'HorizontalRule',
              '-', 'Image',
              '-', 'Table',
              '-', 'BulletedList', 'NumberedList',
              '-', 'Cut','Copy','Paste','PasteText','PasteFromWord',
              '-', 'SpecialChar',
              '-', 'Source',
              '-', 'About',
            ]
        ],
        'width': 840,
        'height': 300,
        'toolbarCanCollapse': False,
    }
}

CKEDITOR_UPLOAD_PATH = os.path.join(MEDIA_ROOT, 'ckeditor')
#------------------------------------------------------------------------------
#ADMIN TOOLS    
ADMIN_TOOLS_INDEX_DASHBOARD = 'dashboard.dashboard.CustomIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'dashboard.dashboard.CustomAppIndexDashboard'
ADMIN_TOOLS_THEMING_CSS = 'style.css'
#------------------------------------------------------------------------------
AUTH_PROFILE_MODULE = 'core.Profile'

PYBB_DEFAULT_AVATAR_URL = os.path.join(STATIC_URL, 'pybb/img/default_avatar.jpg')
#------------------------------------------------------------------------------
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211'
    }
}
#------------------------------------------------------------------------------
ONPAY = {
    "onpay_login": "Kabinet1_ru",
    "private_code": "E0vY3EfZgRIze8FuIiKr",
    "use_balance_table": True,
    'new_operation_status': 0,
    "f":1
    }

PYBB_DEFAULT_TITLE = u'Форум психологов'

#3hVPCwtGY1V
#E0vY3EfZgRIze8FuIiKr

PYBB_BODY_VALIDATOR = check_text

SPRYPAY = {
    'shopID': '218267',
    'secret': 'e91695993816c4d0d1c9c5a7a7f739bf'
}

AUTHENTICATION_BACKENDS = (
    'core.backends.EmailOrUsernameModelBackend',
    'django.contrib.auth.backends.ModelBackend'
    )

ULOGIN_CREATE_USER_CALLBACK = 'core.models.create_user_by_ulogin'

MANDRILL_API_KEY = "bvpbQZB2JISY_h-JRTg6VQ"
EMAIL_BACKEND = "djrill.mail.backends.djrill.DjrillBackend"

LOGIN_SECRET = 'asdkj2349as@3234'
CAPTCHA_OUTPUT_FORMAT = u' %(hidden_field)s %(text_field)s %(image)s'
CAPTCHA_LETTER_ROTATION = None
CAPTCHA_NOISE_FUNCTIONS = None
CAPTCHA_FILTER_FUNCTIONS = None
