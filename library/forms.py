
from django.forms import ModelForm
from models import LibItem, ItemFile


class LibItemAddForm(ModelForm):

    class Meta:
        model = LibItem
        exclude = ('owner', 'viewed_times', 'approved',)

#    def __init__(self, *args, **kwargs):
#        super(LibItemAddForm, self).init(*args, **kwargs)


class LibItemFileAddForm(ModelForm):

    class Meta:
        model = ItemFile
        exclude = ('item',)
