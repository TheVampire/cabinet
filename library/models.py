#coding: UTF-8

from django.contrib.auth.models import User
from django.db import models

from sorl.thumbnail import ImageField
from djangosphinx.models import SphinxSearch
from ckeditor.fields import RichTextField

from expert.models import Issue
from core.utils import JsonMixin


class LibSection(models.Model):
    class Meta:
        verbose_name = u"секция библиотеки"
        verbose_name_plural = u"секции библиотеки"

    type = models.ForeignKey(Issue)
    description = RichTextField(verbose_name=u"описание секции")
    icon = ImageField(
        upload_to="icons",
        verbose_name=u"иконка секци")

    @property
    def name(self):
        return self.type.name

    @models.permalink
    def get_absolute_url(self):
        return (
                'section_details',
                None,
                (self.id,)
                )

    def __unicode__(self):
        return self.name


class LibItem(models.Model, JsonMixin):

    class Meta:
        verbose_name = u"библиотечная запись"
        verbose_name_plural = u"библиотечные записи"

    owner = models.ForeignKey(User)
    name = models.CharField(max_length=200, verbose_name=u"заголовок записи")
    text = RichTextField(verbose_name=u"текст записи")
    format = models.ManyToManyField("ItemFormat")

    section = models.ForeignKey(LibSection, verbose_name="секция записи")
    icon = ImageField(
                      upload_to="icons",
                      verbose_name=u"иконка записи"
                      )

    datetime = models.DateTimeField(
        auto_now=True, verbose_name=u"дата добавления")
    approved = models.BooleanField(default=True)
    viewed_times = models.IntegerField(
        default=0, verbose_name=u"число просмотров")

    search = SphinxSearch(
                          index="library",
                          weights={
                                   'name': 100,
                                   'text': 100
                                  }
                          )

    def __unicode__(self):
        return u"Запись, под заголовком %s" % self.name


class ItemFile(models.Model, JsonMixin):

    class Meta:
        verbose_name = u"библиотечный файл"
        verbose_name_plural = u"библиотечные файлы"

    title = models.CharField(max_length=255, verbose_name=u"название файла")
    item = models.ForeignKey(LibItem, verbose_name=u"запись")
    link = models.FileField(upload_to='library', verbose_name=u"путь")

    @property
    def name(self):
        return self.title


class ItemFormat(models.Model):

    class Meta:
        verbose_name = u"формат библиотечной записи"
        verbose_name_plural = u"форматы библиотечных записей"

    name = models.CharField(
        max_length=255,
        verbose_name=u'название формата, например "аудиозапись"'
        )
    description = models.TextField(verbose_name=u'описание формата')

    def __unicode__(self):
        return self.name
