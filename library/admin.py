#coding: UTF-8
from django.contrib.admin import site, ModelAdmin, StackedInline
from models import LibSection, LibItem, ItemFile, ItemFormat


class FileAdmin(StackedInline):

    model = ItemFile


class SectionAdmin(ModelAdmin):
    list_display = ("name",)


class ItemAdmin(ModelAdmin):
    inlines = [FileAdmin]
    list_display = ("name",)


class ItemFormatAdmin(ModelAdmin):
    list_display = ("name",)


site.register(LibItem, ItemAdmin)
site.register(ItemFormat, ItemFormatAdmin)
site.register(LibSection, SectionAdmin)
