from django.conf.urls import patterns, include, url

urlpatterns = patterns('library.views',
    url("^$", 'lib_main', name='lib-main'),

    url("^items/(\d+)/$", 'item_details', name='libitem-details'),

    #url("^sections/$", 'section_list', name='libsection-list'),
    url("^sections/(\d+)/$", 'section_details', name='libsection-details'),

    #url("^authors/$", 'authors_list', name='authors-list'),
    #url("^authors/(\w+)/$", 'author_details', name='author-details'),

    #url("^formats/$", 'format_list', name='format_list'),
    #url("^formats/(\d+)/$", 'format_details', name='format_details'),

    url("^add/$", 'lib_item_add', name='lib-item-add'),
)
