import json

from django.views.generic import View
from django.shortcuts import get_object_or_404, render
from django.forms.models import modelformset_factory
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from core.decorators import require_groups
from core.utils import sphinx_search
from core.forms import SearchForm

from models import LibSection, LibItem, ItemFile
from forms import LibItemAddForm, LibItemFileAddForm


class LibMain(View):

    def get(self, request):
        form = SearchForm()
        return render(
            request, 'lib_main.html', {
                "sections": LibSection.objects.all(),
                "search_form": form
                }
            )


class LibSectionDetails(View):

    def get(self, request, section_id):
        section = get_object_or_404(LibSection, id=section_id)
        return render(
            request, "lib_section_details.html", {"section": section})


class LibItemDetails(View):

    def get(self, request, item_id):
        item = get_object_or_404(LibItem, id=item_id)
        return render(request, "lib_item_details.html", {"item": item})


class LibItemAdd(View):

    @method_decorator(require_groups(("experts",)))
    def dispatch(self, request, *args, **kwargs):
        return super(LibItemAdd, self).dispatch(request, *args, **kwargs)

    def get(self, request):
        form = LibItemAddForm()
        Files = modelformset_factory(
            model=ItemFile,
            form=LibItemFileAddForm,
            )
        files = Files(queryset=ItemFile.objects.none())
        return render(
            request, "lib_item_add.html", {"form": form, "files": files})

    def post(self, request):
        Files = modelformset_factory(ItemFile, LibItemFileAddForm)
        form = LibItemAddForm(request.POST, request.FILES)
        files = Files(request.POST, request.FILES)

        if form.is_valid() and files.is_valid():

            item = form.save(commit=False)
            item.owner = request.user
            item.save()
            
            form.save_m2m()

            for f in map(lambda f: f.save(commit=False), files):
                f.item = item
                f.save()

            if request.is_ajax():
                return HttpResponse(json.dumps({"status": "ok"}),
                    content_type="application/javascript")
            else:
                return render(request, "lib_item_add_success.html", {})

        else:
            if request.is_ajax():
                return HttpResponse(json.dumps(form.errors, files.errors),
                content_type="application/javascript")
            else:
                return render(request,
                     "lib_item_add.html", {"form": form, "files": files})


class Search(View):

    def get(self, request):
        form = SearchForm(request.GET)
        if form.is_valid():
            results = sphinx_search(
                query=form.cleaned_data['query'],
                index="library"
                )
            if not request.is_ajax():
                return render(
                          request,
                          'library_search_results.html',
                          {'form': form, 'results': results}
                          )
            else:
                return HttpResponse(json.dumps(results),
                    content_type="application/javascript")


section_details = LibSectionDetails.as_view()
item_details = LibItemDetails.as_view()
lib_main = LibMain.as_view()
lib_item_add = LibItemAdd.as_view()
