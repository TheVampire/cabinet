# coding: utf-8
__author__ = 'vampire'
from hashlib import md5
from django import forms
from django.conf import settings

class PaymentForm(forms.Form):
    spShopId = forms.CharField(widget=forms.HiddenInput(), initial=settings.SPRYPAY.get('shopID'))#номер вашего магазина
    spShopPaymentId = forms.CharField(widget=forms.HiddenInput()) # номер платежа на сайте
    spCurrency = forms.CharField(widget=forms.HiddenInput(),initial='rur') # валюта (rur, usd, eur, uah)
    spPurpose = forms.CharField(widget=forms.HiddenInput(),initial=u'Пополнение баланса') # назначение платеже
    spAmount = forms.DecimalField() # сумма
    # spUserDataUserId = forms.CharField(widget=forms.HiddenInput(),) # ID пользователя на сайте
    spIpnUrl = forms.CharField(widget=forms.HiddenInput(),initial=u'http://kabinet1.ru/sprypay/back/') # URL скрипта обработка платежей
    spIpnMethod = forms.CharField(widget=forms.HiddenInput(),initial=1) # 0 - GET, 1 - POST
    spSuccessUrl = forms.CharField(widget=forms.HiddenInput(),) # URL после успешного платежа
    spFailUrl = forms.CharField(widget=forms.HiddenInput(),max_length=256) # URL после неудачного платежа
    spFailMethod = forms.CharField(widget=forms.HiddenInput(),initial=1) # 0 - GET, 1 - POST
    lang = forms.CharField(widget=forms.HiddenInput(),initial=u'ru') # язык