__author__ = 'vampire'
from sprypay.models import Operation, Balance
from django.contrib.admin import site, ModelAdmin

class BalanceAdmin(ModelAdmin):
    list_display = ['sum', 'user']
    list_filter = ['user',]
    search_fields = ['user__username',]

site.register(Operation)
site.register(Balance, BalanceAdmin)