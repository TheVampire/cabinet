from django.conf.urls.defaults import *

urlpatterns = patterns('sprypay.views',
    url(r'^back/$', 'process_answer_from_payment', name='sprypay-snawer'),
    url(r'^nop/$', 'update_sum_in_operation', name='sprypay-operation'),
    url(r'^success/$', 'success_view', name='sprypay-success-url'),
    url(r'^fail/$', 'fail_view', name='sprypay-fail-url'),
#    url(r'^fill/$', 'process_first_step', name='onpay_fill'),
#    url(r'^api/$', 'process_api_request', name='onpay_api' ),
)
