__author__ = 'vampire'
from hashlib import md5
from django.http import HttpResponse
from sprypay.models import Operation, data_update_user_balance
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.contrib.auth.models import User
from decimal import *

from django.views.generic import TemplateView

class SuccessView(TemplateView):
    template_name = 'success.html'

    def get_context_data(self, **kwargs):
        ctx = super(SuccessView, self).get_context_data(**kwargs)
        ctx['d'] = self.request.session['redirect_to_pay']
        return ctx

class FailView(TemplateView):
    template_name = 'fail.html'

success_view = SuccessView.as_view()
fail_view = FailView.as_view()

@csrf_exempt
def update_sum_in_operation(request):
    if request.POST.get('sum') and request.POST.get('opid') and request.user.is_authenticated and not request.user.profile.is_expert():
        o = Operation.obejcts.get(pk=request.POST.get('opid'))
        o.sum = request.POST.get('sum')
        o.save()
        return HttpResponse(o.pk)
    return HttpResponse('-1')

def get_raw_str(r, d):
    s = u''
    for i in d:
        s += r.POST.get(i)
    s += settings.SPRYPAY.get('secret')
    return s

def get_hash(r, d):
    m = md5()
    m.update(get_raw_str(r, d).encode('utf-8'))
    return m.hexdigest()

def check_summ_payment(r, d):
    if get_hash(r, d) == r.POST.get('spHashString'):
        return True
    return False

@csrf_exempt
def process_answer_from_payment(request):
    spShopId = request.POST.get('spShopId')
    spShopPaymentId = request.POST.get('spShopPaymentId')
    spBalanceAmount = request.POST.get('spBalanceAmount')
    spUserDataUserId = request.POST.get('spUserDataUserId')

    check_vars = [
        'spPaymentId', 'spShopId', 'spShopPaymentId', 'spBalanceAmount', 'spAmount', 'spCurrency', 'spCustomerEmail',
        'spPurpose', 'spPaymentSystemId', 'spPaymentSystemAmount', 'spPaymentSystemPaymentId', 'spEnrollDateTime'
    ]

    if check_summ_payment(request, check_vars):
        if spShopId == settings.SPRYPAY.get('shopID'):
            # try:
            #     user = User.objects.get(pk=spUserDataUserId)
            # except User.DoesNotExist:
            #     user = None
            # if user:
            try:
                o = Operation.objects.get(
                    # user=user,
                    pk=spShopPaymentId,
                    status = False
                )
            except Operation.DoesNotExist:
                o = None
            if o:
                spBalanceAmount = Decimal(spBalanceAmount)
                o.sum = spBalanceAmount
                o.save()
                data_update_user_balance(o.pk, spBalanceAmount)
                return HttpResponse('ok')
            else:
                return HttpResponse('error')
            # else:
            #     return HttpResponse('error')
        else:
            return HttpResponse('error')
    else:
        return HttpResponse('error')