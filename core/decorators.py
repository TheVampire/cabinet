from django.http import HttpResponseBadRequest, HttpResponseForbidden
from django.core.exceptions import PermissionDenied

def unpack_params(method, param_names):

    def decorator(func):

        def wrapper(request, *args, **kwargs):
            request_params = getattr(request, method)

            try:
                params = {key: request_params[key] for key in param_names}
            except KeyError:
                return HttpResponseBadRequest()

            kwargs.update(params)
            return func(request, *args, **kwargs)

        return wrapper

    return decorator


def require_groups(required_groups):
    """
    This decorator checks whether required group set is
    subset of users group set. If so, allow user to run
    this view, otherwise forbidden;

    Parameters:

     - required_group_name_list: something like ['artists', 'users']
     - fail_template: template to show in case of fail
    """
    def decorator(func):

        def wrapper(request, *args, **kwargs):
            request.user.groups.filter(name__in=required_groups)

            if request.user.groups.filter(name__in=required_groups).count():
                return func(request, *args, **kwargs)
            else:
                raise PermissionDenied

        return wrapper

    return decorator
