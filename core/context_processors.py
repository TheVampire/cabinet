# coding: utf-8
__author__ = 'vampire'
import pytils
import datetime
from sprypay.models import Balance
from django.core.cache import cache
import json
from models import UserMessage, Aphorism
from news.models import SEOStuff

def insert_current_date(request):
    balance = ''
    messages = ""
    private_messages = None
    if not request.user.is_anonymous():
        balance = Balance.objects.filter(user=request.user)
        if balance.count() > 0:
            balance = balance[0]
        cache_key = "messages-for-{}".format(request.user.pk)
        messages = cache.get(cache_key)
        if messages:
            messages = json.loads(messages)
            messages = [m.items()[0] for m in messages]
#            messages = messages.split("][")
#        cache.delete(cache_key)
        private_messages = UserMessage.objects.filter(
            to_user=request.user
        ).order_by('-created')[:5]
    data = {
        'aphorism': Aphorism.objects.order_by('?')[0],
        'current_date': pytils.dt.ru_strftime(u"%d %B %Y", inflected=True, date=datetime.datetime.now()) + u' года',
        'balance': balance,
        "c_messages": messages,
        "private_messages": private_messages
    }
    if SEOStuff.objects.filter(url=request.get_full_path()).exists():
        data['seostuff'] = SEOStuff.objects.get(url=request.get_full_path())
    return data