var flash_initialized = false;
var should_connect = false;

var flash;
var chat_input = document.getElementById("ChatInput");
var chat_scroll = document.getElementById("ChatScroll");
var chat_div = document.getElementById("ChatDiv");
var chat_app = document.getElementById("ChatApp");
var header = document.getElementById("Header");
var welcome_screen = document.getElementById("WelcomeScreen");
var send_button = document.getElementById("SendButton");
var chat_input_wrapper = document.getElementById("ChatInputWrapper");
var code_input = document.getElementById("CodeInput");

var chat_code = document.getElementById("chat_code");

var got_prv_msg = false;
var prv_phrase_is_own = false;
var prv_msg_is_status = false;

var chat_blocked = true;

function codeKeyDown(evt) {
    if (evt.keyCode == 13) {
        connect();
    }
}
if(chat_code.value){
    //should_connect = true;
    connect()
}
function flashInitialized() {
    flash = document.getElementById("MyChat");

    flash_initialized = true;
    console.log(chat_code.value);


    if (should_connect) {
        doConnect();
    }
}

function connect() {
    welcome_screen.style.display = "none";

    chat_app.style.display = "block";
    {
        header.style.left = "0";
        header.style.visibility = "visible";
    }

    doConnect();

    chat_input.focus();

    {

        chat_scroll.scrollTop = chat_scroll.scrollHeight;
    }
}


function doConnect() {
    should_connect = true;
    if (!flash_initialized) {
        return;
    }

    var code = chat_code.value ? chat_code.value : code_input.value;

    document ["MyChat"].connect(code);
}

function newCall() {
    chat_app.style.display = "none";
    {
        header.style.left = "-99999px";
        header.style.visibility = "hidden";
    }

    welcome_screen.style.display = "block";

    code_input.focus();
}


function doAddChatMessage(msg, is_own, is_status, color) {
    var scroll_to_bottom = (chat_scroll.scrollTop + chat_scroll.clientHeight >= chat_scroll.scrollHeight);

    var msg_div = document.createElement('div');

    if (is_status) {
        if (prv_msg_is_status || !got_prv_msg) {
            msg_div.className = "status_msg";
        } else {
            msg_div.className = "lead_status_msg";
        }
        prv_msg_is_status = true;
    } else {
        if (is_own) {
            if (got_prv_msg) {
                if (prv_phrase_is_own && !prv_msg_is_status) {
                    msg_div.className = "own_chat_phrase";
                } else {
                    msg_div.className = "lead_own_chat_phrase";
                }
            } else {
                msg_div.className = "own_chat_phrase";
            }

            prv_phrase_is_own = true;
        } else {
            if (got_prv_msg) {
                if (!prv_phrase_is_own && !prv_msg_is_status) {
                    msg_div.className = "chat_phrase";
                } else {
                    msg_div.className = "lead_chat_phrase";
                }
            } else {
                msg_div.className = "chat_phrase";
            }

            prv_phrase_is_own = false;
        }
        prv_msg_is_status = false;
    }
    got_prv_msg = true;

    if (color == "red") {
        msg_div.className += " status_msg_red";
    } else if (color == "green") {
        msg_div.className += " status_msg_green";
    }

    var p_tag = document.createElement('span');
    var msg_text = document.createTextNode(msg);
    p_tag.appendChild(msg_text);
    msg_div.appendChild(p_tag);
    chat_div.appendChild(msg_div);

    if (scroll_to_bottom) {
        chat_scroll.scrollTop = chat_scroll.scrollHeight;
    }

}

function addChatMessage(msg) {
    doAddChatMessage(msg, false /* is_own */, false /* is_status */);
}

function addStatusMessage(msg) {
    doAddChatMessage(msg, false /* is_own */, true /* is_status */, "" /* color */);
}

function addRedStatusMessage(msg) {
    doAddChatMessage(msg, false /* is_own */, true /* is_status */, "red" /* color */);
}

function addGreenStatusMessage(msg) {
    doAddChatMessage(msg, false /* is_own */, true /* is_status */, "green" /* color */);
}

function sendChatMessage() {
    if (chat_blocked) {
        return;
    }


    msg = chat_input.value;
    chat_input.value = "";

    doAddChatMessage(msg, true /* is_own */, false /* is_status */);
    flash.sendChatMessage(msg);
}

function blockChat() {
    chat_blocked = true;
    chat_input_wrapper.className = "chat_input_wrapper_blocked";
    chat_input.className = "chat_input chat_input_blocked";
    send_button.className = "send_button send_button_blocked";
}

function unblockChat() {
    chat_blocked = false;
    chat_input_wrapper.className = "chat_input_wrapper_unblocked";
    chat_input.className = "chat_input chat_input_unblocked";
    send_button.className = "send_button send_button_unblocked";
}

function sendButtonClick() {
    sendChatMessage();
}

function chatKeyDown(evt) {
    if (evt.keyCode == 13) {
        sendChatMessage();
    }

}

function sendKeyDown(evt) {
    if (evt.keyCode == 13 /* Enter */ || evt.keyCode == 32 /* Spacebar */) {
        sendChatMessage();
    }

}
