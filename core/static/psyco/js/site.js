$(document).ready(function(){
    $('#help_spec_head li a').click(function(e){
        e.preventDefault();
        var a = $(this);
        var li_list = $('#help_spec_head li');

        li_list.removeClass('active');
        a.parent().addClass('active');

        $('#category').val(a.data('categoryId'));

        var n = li_list.index(a.parent());
        $('.help_spec_check').children().hide();
        $('.help_spec_check').children().eq(n).show();
    });



    $('#pay_head li a').click(function(e){
        e.preventDefault();
        $('#pay_head li').removeClass('active');
        $(this).parents('li').eq(0).addClass('active');
        var n = $('#pay_head li').index($(this).parents('li').eq(0));
        $('.pay_cont .frame').children().hide();
        $('.pay_cont .frame').children().eq(n).show();

    });

//    if ($('select').length != 0) {
//		var params = {
//			changedEl: "select",
//			visRows: 5,
//			scrollArrows: true
//		}
//		cuSel(params);
//	}

//    if ($('input[type="radio"]').length != 0){
//		var label = $('label');
//		label.each(function(i){
//			if ( $(this).children().attr("checked") ) $(this).addClass('active');
//		});
//		label.live('click', function(){
//			$(this).children('input').click(function(){return false;});
//			var name = $(this).children().attr("name");
//				label.children('input[name=' + name + ']').parent().removeClass('active');
//				label.children('input[name=' + name + ']').removeAttr("checked");
//				$(this).children('input[name=' + name + ']').attr("checked", "checked");
//				$(this).addClass('active');
//			return false;
//		});
//	} // if

//    $('.change_service table tr:not(.serv_detail)').find('label').live('click', clickLabel);
//
//    function clickLabel(e){
//      //if($(this).find('input').eq(0).attr('checked')) return;
//      $('.serv_detail').hide();
//      $('.serv_detail').find('input:radio').eq(0).removeAttr('checked');
//      $('.serv_detail').find('label').eq(0).removeClass('active');
//      $('.serv_detail').prev().show();
//      $(this).parents('tr').hide();
//      $(this).parents('tr').eq(0).next().show();
//      $(this).parents('tr').eq(0).next().find('label').eq(0).addClass('active')
//      $(this).parents('tr').eq(0).next().find('input:radio').eq(0).attr('checked', "");
//    }

    //if ($('.bubble_answer .frame').length != 0) $('.bubble_answer .frame').jScrollPane({verticalDragMaxHeight: 9});

    $('.parents .item .answer a').bind('click', clickAnswer);
    $('.parents .close').bind('click', clickClose1);

    function clickClose1(e){
      $(this).parents('.bubble_answer').hide();
    }

    function clickAnswer(e){
        e.preventDefault();
        //alert('test');
		$('.bubble_answer').hide();
		var l = $(this).parents('.line').eq(0).offset().top-$(this).parents('.parents').eq(0).offset().top-100;
		//alert(l);
		$(this).parents('.item').eq(0).find('.bubble_answer').css('top', l+'px')
        $(this).parents('.item').eq(0).find('.bubble_answer').show();
        //$(this).parents('.item').eq(0).find('.bubble_answer  .frame_cont').jScrollPane({verticalDragMaxHeight: 9});
    }
	
	// client popup calendar
	$('.b-schedule table td.clients a').click(function(){
		$('.client-popup').remove();
		var link = $(this).attr('href'),
			left = $(this).offset().left - 22,
			top = $(this).offset().top - 22;
			if (link){
				$.ajax({
					type: "POST",
					url: link,
					success: function(data){
						$('body').append('<div class="client-popup">' + data + '<span class="close"></span></div>');
						if (top + $('.client-popup').height() > $('#wrap-all').height() - 150) top = $('#wrap-all').height() - $('.client-popup').height() - 150;
						$('.client-popup').css({
							'left': left,
							'top': top
						}).fadeIn(150);
						$('.client-popup .close').live('click', function(){
							$('.client-popup').fadeOut(150, function(){
								$('.client-popup').remove();
							});
						});
						$('.client-popup .scroll').jScrollPane({
							verticalDragMaxHeight: 10
						});
					}
				});
			}
		return false;
	});
	////////////////////
	// time popup calendar
	$('.b-schedule table.calendar a.settings').click(function(){
		$('.time-popup').remove();
		var link = $(this).attr('href'),
			left = $(this).parent().offset().left - 1,
			top = $(this).parent().offset().top - 1;
			if (link){
				$.ajax({
					type: "GET",
					url: link,
					success: function(data){
						$('body').append('<div class="time-popup">' + data + '<span class="close"></span></div>');
						$('.time-popup').css({
							'left': left,
							'top': top
						}).fadeIn(150);
						$('.time-popup .close').click(function(){
							$('.time-popup').fadeOut(150, function(){
								$('.time-popup').remove();
							});
							$('.b-schedule table.calendar td').removeClass('select');
						});
					}
				});
			}
			$('.b-schedule table.calendar td').removeClass('select');
			if ($(this).parents('th').length > 0){
				if (link.indexOf('week-day') >= 0){
					var weekDay = link.substr(link.indexOf('week-day') + 'week-day'.length + 1, 1);
						$('.b-schedule table.calendar tr').each(function(i){
							$(this).find('td').eq(weekDay - 1).addClass('select')
						});
				}
				if (link.indexOf('week-count') >= 0){
					var weekCount = link.substr(link.indexOf('week-count') + 'week-count'.length + 1, 1);
						$('.b-schedule table.calendar tr').eq(weekCount).find('td').addClass('select')
				}
			}
		return false;
	});
	$('.time-popup .add').live('click', function(){
		var current_line = $(this).parents('.line'),
			new_line = current_line.clone();
			new_line.insertAfter(current_line);
			$(this).removeClass('add').addClass('del');
		return false;
	});
	$('.time-popup .del').live('click', function(){
		var current_line = $(this).parents('.line'),
			prev_line = current_line.prev('.line');
			if (prev_line){
				current_line.remove();
			}
		return false;
	});
	////////////////////
	
	if ($('ol.numbers').length > 0){
		var ol = $('ol.numbers'),
			start = ol.attr('start') || 1;
			
			ol.find('li').each(function(i){
				$(this).wrapInner('<div class="text"></div>');
				$('<span class="n">' + (start++) + '</span>').insertBefore($(this).find('.text'));
			});
			
	}
	
});

var apiElement = [], apiScroll = [];

window.onload = function(){
	
//	if ($('#specialists_search input[type="text"]').length > 0){
//		var form_action = $('#specialists_search').attr('action');
//		$.get(form_action, function(data) {
//            var result = [];
//            $.each(data, function(i,v) {
//                result.push(v.last_name + " " + v.first_name + " " + v.middle_name );
//            });
//            if ($("#specialists_search").length) var appendTo = "#specialists_search";
//            $('#specialists_search input[type="text"]').autocomplete({
//				source: result,
//				appendTo: appendTo
//            });
//        });
//	}
	
	$('.show_details').click(function(){
		
		var i = $('.show_details').index(this);
			
		$(this).parents('tr').next('tr.detail').toggle();
		$(this).parents('tbody').toggleClass('active');
		
		if (apiScroll[i]){
			apiScroll[i].destroy();
		}
		
		apiElement[i] = $('.b-clients table .detail .scroll').eq(i);
		apiElement[i].width($('.b-clients').width() - 96).jScrollPane({ verticalDragMaxHeight: 10 });
		apiScroll[i] = apiElement[i].data('jsp')
			
		return false;
	});	
		
	$('.person').mouseenter(function(){
        setTimeout(function()
        {
            var content = $(this).find('.bubble_person').html();
            $('.person-popup').remove();
            if (content){
                $('body').append('<div class="person-popup"><div class="b-frame bubble_person">'+content+'</div></div>');
                $('.person-popup')
                    .css({
                        'position': 'absolute',
                        'left': $(this).offset().left,
                        'top': $(this).offset().top,
                        'z-index': 9999
                    });
                $('.person-popup .bubble_person').show();
            }
        }, 5000);

	});
	$('.person-popup').live('mouseleave', function(){
		$('.person-popup').remove();
	});
	
}
