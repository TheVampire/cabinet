from django.utils import timezone
from django.conf import settings
import datetime
from django.core.cache import cache

class TimezoneMiddleware(object):
    def process_request(self, request):
#        try:
#            timezone.activate(request.user.profile.timezone)
#        except AttributeError:
#            timezone.activate(settings.TIME_ZONE)
        pass

#class BannerMiddleware(object):
#    def process_response(self, request, response):
#

class ActiveUserMiddleware:

    def process_request(self, request):
        current_user = request.user
        if request.user.is_authenticated():
            if request.path != '/api/notification/':
                now = datetime.datetime.now()
                cache.set('seen_{}'.format(current_user.username), now,
                    settings.USER_LASTSEEN_TIMEOUT)