#coding: UTF-8

import json, datetime, pytils
from django.contrib.messages import get_messages
import calendar, time
import django
import os
from django.views.generic import TemplateView, View, ListView
from django.views.generic.edit import ProcessFormView
from django.shortcuts import render, redirect, get_object_or_404, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User, check_password
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.db.models import Q, Count, Max
from django.core.mail import mail_admins, send_mail
from django.core.cache import cache
from django.core.urlresolvers import reverse
from pybb.models import Topic, Post
from django.utils.translation import ugettext_lazy as _

from expert.models import Consultation
from django.conf import settings
from sprypay.models import Operation
from sprypay.forms import PaymentForm
from core.models import UserMessage, OrderOutMoney, FAQuestion
from core.utils import DiggPaginator, QuerySetDiggPaginator
from core.forms import MONTHS, PayRegForm
from expert.models import Issue, Institution, StudyCenter, Association, ModeratePrivateData, ModerateInstitution, \
    ModerateLoginData, ModerateIssues, WorkDay, DiplomScan, Recal
from forms import ProfileEditForm, PrivateClientForm, PrivateExpertForm
from django.forms.models import modelformset_factory, BaseModelFormSet, HiddenInput
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.sites.models import Site, RequestSite
from registration.models import RegistrationProfile
from registration import signals

from registration.backends.default.views import RegistrationView

class RegistrationLoginView(TemplateView):
    template_name = 'registration/reg_login.html'

    def post(self, request, *args, **kwargs):
        if request.POST.get('username'):
            p = User.objects.make_random_password()
            form = PayRegForm(request.POST)
            if form.is_valid():
                username=form.cleaned_data['username']
                email = form.cleaned_data['email']

                if Site._meta.installed:
                    site = Site.objects.get_current()
                else:
                    site = RequestSite(request)
                new_user = RegistrationProfile.objects.create_inactive_user(username, email,
                                                                            p, site)
                signals.user_registered.send(sender=RegistrationView,
                                 user=new_user,
                                 request=request)

                msg = u'Логин: {}\n Пароль: {}\n'.format(username, p)
                send_mail(u'Кабинет №1 - данные для входа в Ваш личный кабинет', msg, 'info@kabinet1.ru', [new_user.email])

                request.session['redirect_to_pay'] = True

                return redirect(reverse('registration_complete') + '?email=' + new_user.email)
            else:
                return self.render_to_response(self.get_context_data(*args, **kwargs))
            return redirect(request.path)


    def get_context_data(self, **kwargs):
        ctx = super(RegistrationLoginView, self).get_context_data(**kwargs)
        ctx['reg_form'] = PayRegForm()
        ctx['form'] = AuthenticationForm()
        return ctx

class CustomRegistrationView(RegistrationView):
    def get_success_url(self, request, user):
        return (u'{}{}'.format(reverse('registration_complete'), u'?email={}'.format(user.email)), [], {})

class RegistrationCompleteView(TemplateView):
    template_name = 'registration/registration_complete.html'

    def get_context_data(self, **kwargs):
        ctx = super(RegistrationCompleteView, self).get_context_data(**kwargs)
        ctx['email'] = self.request.GET.get('email')
        return ctx

class LeftLinkMixin(object):
    def is_expert(self):
        return True if self.request.user.groups.filter(name='experts').count() > 0 else False

    def get_context_data(self, *a, **kw):
        ctx = super(LeftLinkMixin, self).get_context_data(*a, **kw)
        ctx['is_expert'] = self.is_expert()
        return ctx

class PrivateBaseFormSet(BaseModelFormSet):
    moderate_item = None
    class_prefix = None

    def save_existing_objects(self, commit=True):
        self.changed_objects = []
        self.deleted_objects = []
        if not self.initial_forms:
            return []

        new_data_list = []
        saved_instances = []
        user = None
        for form in self.initial_forms:
            pk_name = self._pk_field.name
            raw_pk_value = form._raw_value(pk_name)

            # clean() for different types of PK fields can sometimes return
            # the model instance, and sometimes the PK. Handle either.
            pk_value = form.fields[pk_name].clean(raw_pk_value)
            pk_value = getattr(pk_value, 'pk', pk_value)

            obj = self._existing_object(pk_value)
            if self.can_delete and self._should_delete_form(form):
                self.deleted_objects.append(obj)
                obj.delete()
                continue
            if form.has_changed():
                if not user:
                    user = obj.expertblank.expert
                newdata = {}
                for field in form.changed_data:
                    newdata[field] = form.cleaned_data[field]
                newdata['pk'] = obj.pk
                newdata['expertblank'] = obj.expertblank.pk
                new_data_list.append(newdata)

                self.changed_objects.append((obj, form.changed_data))
#                saved_instances.append(self.save_existing(form, obj, commit=commit))
                if not commit:
                    self.saved_forms.append(form)
        if new_data_list:
            self.moderate_item, created = ModerateInstitution.objects.get_or_create(
                user=user
            )
            setattr(self.moderate_item, '{}_updated_json'.format(self.class_prefix), json.dumps(new_data_list))
            self.moderate_item.save()
            return []
        return saved_instances

    def save_new_objects(self, commit=True):
        self.new_objects = []
        new_data_list = []
        user = None
        for form in self.extra_forms:
            if not form.has_changed():
                continue
                # If someone has marked an add form for deletion, don't save the
            # object.
            if self.can_delete and self._should_delete_form(form):
                continue
            newdata = {}
            if not user:
                user = form.cleaned_data['expertblank'].expert
            for field in form.cleaned_data:
                if field == 'expertblank':
                    newdata[field] = form.cleaned_data[field].pk
                else:
                    newdata[field] = form.cleaned_data[field]
            new_data_list.append(newdata)
#            self.new_objects.append(self.save_new(form, commit=commit))
            if not commit:
                self.saved_forms.append(form)
        if new_data_list:
            self.moderate_item, created = ModerateInstitution.objects.get_or_create(
                user=user
            )
            setattr(self.moderate_item, '{}_added_json'.format(self.class_prefix), json.dumps(new_data_list))
            self.moderate_item.save()
            return []
        return self.new_objects

class InstitutionBaseFormSet(PrivateBaseFormSet):
    class_prefix = 'institution'
class StudyCenterBaseFormSet(PrivateBaseFormSet):
    class_prefix = 'studycenter'
class AssociationBaseFormSet(PrivateBaseFormSet):
    class_prefix = 'association'

#    def save_existing(self, form, instance, commit=True):

#        print instance.__class__.__name__

InstitutionFormSet = modelformset_factory(Institution, extra=0, formset=InstitutionBaseFormSet)
StudyCenterFormSet = modelformset_factory(StudyCenter, extra=0, formset=StudyCenterBaseFormSet)
AssociationFormSet = modelformset_factory(Association, extra=0, formset=AssociationBaseFormSet)

class ProfileView(LeftLinkMixin, View):
    template_name = "private/client__data.html"


    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        django.utils.translation.activate('ru')
        return super(ProfileView, self).dispatch(*args, **kwargs)


    def get(self, request):
        form = None
        if request.user.groups.filter(name='clients').count() > 0:
            self.template_name = 'private/client__data.html'
            form = PrivateClientForm(
                {
                    'email': request.user.email,
                    'subscribe': request.user.profile.subscribe,
                    'phone': request.user.profile.phone_number,
                    'middlename': request.user.profile.middlename,
                    'name':request.user.first_name,
                    'country': request.user.profile.country,
                    'surname': request.user.last_name,
                    'timezone': request.user.profile.time_zone,
                    'birth_day': request.user.profile.birthday.day,
                    'birth_month': request.user.profile.birthday.month,
                    'birth_year': request.user.profile.birthday.year
                }
            )
        if self.is_expert():
            self.institution_formset = InstitutionFormSet(prefix="institution",
                queryset=Institution.objects.filter(expertblank=request.user.expertblank))
            for form in self.institution_formset:
                form.fields["expertblank"].widget= HiddenInput()
            self.studycenter_formset = StudyCenterFormSet(prefix="studycenter",
                queryset=StudyCenter.objects.filter(expertblank=request.user.expertblank))
            for form in self.studycenter_formset:
                form.fields["expertblank"].widget= HiddenInput()
            self.association_formset = AssociationFormSet(prefix="association",
                queryset=Association.objects.filter(expertblank=request.user.expertblank))
            for form in self.association_formset:
                form.fields["expertblank"].widget= HiddenInput()
            self.template_name = 'private/expert__data.html'
            form = PrivateExpertForm(
                {
                    'about': request.user.expertblank.personal,
                    'email': request.user.email,
                    'subscribe': request.user.profile.subscribe,
                    'phone': request.user.profile.phone_number,
                    'middlename': request.user.expertblank.middle_name,
                    'name':request.user.expertblank.first_name,
                    'country': request.user.expertblank.country,
                    'surname': request.user.expertblank.last_name,
                    'city': request.user.expertblank.city,
                    'timezone': request.user.profile.time_zone,
                    'birth_day': request.user.expertblank.birthday.day,
                    'birth_month': request.user.expertblank.birthday.month,
                    'birth_year': request.user.expertblank.birthday.year,
                    'photo': request.user.expertblank.avatar,
                    'fix': request.user.expertblank.fix,
                    'consultation_time': request.user.expertblank.consultation_time
                }
            )
        return render(
            request,
            self.template_name,
            {
                "is_expert": self.is_expert(),
                "user": request.user,
                "issues": Issue.objects.filter(parent=None),
                "user_issues": [i.pk for i in request.user.expertblank.issues.all()] if request.user.groups.filter(name='experts').count() > 0 else [],
                "profile": request.user.profile,
                "profile_edit_form": form,
                "last_forum_topics": Topic.objects.filter(
                    user=request.user).order_by('-created')[:3],
                "last_forum_posts": Post.objects.filter(
                    user=request.user).order_by("-created")[:10],
                "consultations": Consultation.objects.filter(
                    Q(client=request.user) | Q(expert=request.user)),
                "education_institute_formset": self.institution_formset if hasattr(self, "institution_formset") else "",
                "education_studycenter_formset": self.studycenter_formset if hasattr(self, "studycenter_formset") else "",
                "education_association_formset": self.association_formset if hasattr(self, "association_formset") else "",
            }
            )

    def check_for_success(self, request):
        msgs = get_messages(request)
        success = True
        for m in msgs:
            if m.level == messages.ERROR:
                success = False
        if success:
            messages.add_message(request, messages.INFO, u'Ваш запрос на изменение данных отправлен. ' + \
                                                         u'После подтверждения изменения будут внесены автоматически')

    def post(self, request):
        form = PrivateClientForm(
            request.POST
        )
        if self.is_expert() and request.POST.get('private-data-type', False) == 'education':
            post_data = request.POST
            fs = InstitutionFormSet(post_data, request.FILES, prefix = "institution")
            if fs.is_valid():
                fs.save()
            else:
                msg = u''
                for e in fs.errors:
                    for k,v in e.items():
                        msg += u'<div>{}: {}</div>'.format(Institution._meta.get_field_by_name(k)[0].verbose_name, _(v[0]))
                messages.add_message(request, messages.ERROR, msg)
            fs = StudyCenterFormSet(post_data, request.FILES, prefix = "studycenter")
            if fs.is_valid():
                fs.save()
            else:
                msg = u''
                for e in fs.errors:
                    for k,v in e.items():
                        msg += u'<div>{}: {}</div>'.format(Association._meta.get_field_by_name(k)[0].verbose_name, _(v[0]))
                messages.add_message(request, messages.ERROR, msg)
            fs = AssociationFormSet(post_data, request.FILES, prefix = "association")
            if fs.is_valid():
                fs.save()
            else:
                msg = u''
                for e in fs.errors:
                    for k,v in e.items():
                        msg += u'<div>{}: {}</div>'.format(StudyCenter._meta.get_field_by_name(k)[0].verbose_name, _(v[0]))
                messages.add_message(request, messages.ERROR, msg)
            self.check_for_success(request)
            return self.get(request)

        if request.POST.get('private-data-type', False) == 'problems':
            ids = map(int, request.POST.getlist('issue'))
            mi,cr = ModerateIssues.objects.get_or_create(
                user=request.user
            )
            mi.data = json.dumps(ids)
            mi.save()
            self.check_for_success(request)
            return self.get(request)

        if self.is_expert():
            form = PrivateExpertForm(
                request.POST, request.FILES
            )

        if form.is_valid():
            if self.is_expert():
                # check diploms and scan
                #
                #
                i = 1
                while True:
                    if 'diplom-scan-file-{}'.format(i) in request.FILES:
                        diplom = request.FILES['diplom-scan-file-{}'.format(i)]
                        path = os.path.join(settings.PROJECT_ROOT, 'media', 'images', diplom.name)
                        with open(path, 'wb+') as destination:
                            for chunk in diplom.chunks():
                                destination.write(chunk)
                        new_diplom = DiplomScan(
                            scan = os.path.join('media', 'images', diplom.name),
                            text = request.POST.get('diplom-scan-text-{}'.format(i))
                        )
                        new_diplom.save()
                        request.user.expertblank.scans.add(new_diplom)
                        request.user.expertblank.save()
                        i += 1
                    else:
                        break
                if form.cleaned_data.get('about', False):
                    if self.is_expert():
                        mpd,cr = ModeratePrivateData.objects.get_or_create(
                            user=request.user
                        )
                        mpd.new_about = form.cleaned_data['about']
                        mpd.save()
                    else:
                        request.user.expertblank.personal = form.cleaned_data['about']
                if form.cleaned_data.get('country', False):
                    if self.is_expert():
                        mpd,cr = ModeratePrivateData.objects.get_or_create(
                            user=request.user
                        )
                        mpd.new_country = form.cleaned_data['country']
                        mpd.save()
                    else:
                        request.user.expertblank.country = form.cleaned_data['country']
                if form.cleaned_data.get('city', False):
                    if self.is_expert():
                        mpd,cr = ModeratePrivateData.objects.get_or_create(
                            user=request.user
                        )
                        mpd.new_city = form.cleaned_data['city']
                        mpd.save()
                    else:
                        request.user.expertblank.city = form.cleaned_data['city']
                if form.cleaned_data.get('fix', False):
#                    fix4save = form.cleaned_data.get('fix', False)
#                    if fix4save < 500:
#                        messages.add_message(request, messages.ERROR, u'Минимальная стоимость - 500 рублей')
#                        return self.get(request)
#                    else:
                    request.user.expertblank.fix = form.cleaned_data['fix']
                if form.cleaned_data.get('consultation_time', False):
                    request.user.expertblank.consultation_time = form.cleaned_data['consultation_time']
                if form.cleaned_data.get('photo', False):
#                    print form.cleaned_data['photo']
                    if self.is_expert():
                        mpd,cr = ModeratePrivateData.objects.get_or_create(
                            user=request.user
                        )
                        mpd.new_photo = form.cleaned_data['photo']
                        mpd.save()
                    else:
                        request.user.expertblank.avatar = form.cleaned_data['photo']
                request.user.expertblank.save()
            if form.cleaned_data['birth_day'] and form.cleaned_data['birth_month'] and form.cleaned_data['birth_year']:
                birthday = datetime.date(
                    int(form.cleaned_data['birth_year']),
                    int(form.cleaned_data['birth_month']),
                    int(form.cleaned_data['birth_day']))
                if self.is_expert():
                    mpd,cr = ModeratePrivateData.objects.get_or_create(
                        user=request.user
                    )
                    mpd.new_birthday = birthday
                    mpd.save()
                else:
                    request.user.profile.birthday = birthday
            request.user.profile.subscribe = form.cleaned_data['subscribe']
            if form.cleaned_data['timezone']:
                if self.is_expert():
                    mpd,cr = ModeratePrivateData.objects.get_or_create(
                        user=request.user
                    )
                    mpd.new_time_zone = form.cleaned_data['timezone']
                    mpd.save()
                else:
                    request.user.profile.time_zone = form.cleaned_data['timezone']
            if form.cleaned_data['name']:
                if self.is_expert():
                    mpd,cr = ModeratePrivateData.objects.get_or_create(
                        user=request.user
                    )
                    mpd.new_firstname = form.cleaned_data['name']
                    mpd.save()
                else:
                    request.user.first_name = form.cleaned_data['name']
                    request.user.save()
            if form.cleaned_data['email']:
                request.user.email = form.cleaned_data['email']
                request.user.save()
            if form.cleaned_data['surname']:
                if self.is_expert():
                    mpd,cr = ModeratePrivateData.objects.get_or_create(
                        user=request.user
                    )
                    mpd.new_lastname = form.cleaned_data['surname']
                    mpd.save()
                else:
                    request.user.last_name = form.cleaned_data['surname']
                    request.user.save()
            if form.cleaned_data['middlename']:
                if self.is_expert():
                    mpd,cr = ModeratePrivateData.objects.get_or_create(
                        user=request.user
                    )
                    mpd.new_middlename = form.cleaned_data['middlename']
                    mpd.save()
#                    request.user.expertblank.middle_name = form.cleaned_data['middlename']
                else:
                    request.user.profile.middlename = form.cleaned_data['middlename']
            if form.cleaned_data['country']:
                if not self.is_expert():
                    request.user.profile.country = form.cleaned_data['country']
            if form.cleaned_data['phone']:
                if self.is_expert():
                    request.user.expertblank.phone_number = form.cleaned_data['phone']
                else:
                    request.user.profile._phone_number = form.cleaned_data['phone']
            if form.cleaned_data['old_password']:
                if request.user.check_password(form.cleaned_data['old_password']):
                    if form.cleaned_data['new_password'] and \
                    form.cleaned_data['repeat_new_password']:
                        if form.cleaned_data['repeat_new_password'] == form.cleaned_data['new_password']:
                            request.user.set_password(form.cleaned_data['new_password'])
                        else:
                            messages.add_message(request, messages.ERROR, u'Пароли не совпадают')
                    else:
                        messages.add_message(request, messages.ERROR, u'Не введено поле')
                else:
                    messages.add_message(request, messages.ERROR, u'Старый пароль не верен')

            request.user.profile.save()
            request.user.save()

#            profile = form.save(commit=False)
#            profile.signature_html = "<p>%s</p>" % profile.signature
#            profile.save()e

            if self.is_expert() and request.POST.get('private-data-type', False) != 'login':
                messages.add_message(request, messages.INFO, u"""Ваши новые данные успешно отправены на проверку администрации. В ближайшее время изменения будут внесены либо мы свяжемся с вами для уточнения.""")
            else:
                messages.add_message(request, messages.INFO, u'Профиль обновлен!')

            self.check_for_success(request)

            return self.get(request)

        else:
            messages.add_message(
                request, messages.ERROR, u'Ошибка в форме профиля! {}'.format(form.errors))
            if request.is_ajax():
                return HttpResponse(json.dumps({"status": "error"}),
                    content_type="application/javascript")
            else:
                self.check_for_success(request)
                return self.get(request)

class MessagesProfileView(LeftLinkMixin, ListView):
    paginate_by = 5
    paginator_class = QuerySetDiggPaginator
    context_object_name = 'usermessages'
    template_name = "private/messages.html"

    def get_paginator(self, queryset, per_page, orphans=0, allow_empty_first_page=True):
        return QuerySetDiggPaginator(queryset, per_page, padding=3, body=7, tail=1)

    def get_queryset(self):
        return UserMessage.objects.filter(to_user=self.request.user).order_by('-created')

class ConsultationView(LeftLinkMixin, TemplateView):
    template_name = "private/consultation.html"

    def get_context_data(self, *a, **kw):
        ctx = super(ConsultationView, self).get_context_data(*a, **kw)
        if self.request.user.groups.filter(name='experts').count() > 0:
            ctx['is_expert'] = True
        if self.request.user.groups.filter(name='clients').count() > 0:
            consl = Consultation.objects.filter(
                client=self.request.user,
                dtstart__gt=datetime.datetime.now() - datetime.timedelta(hours=1)
            ).filter(Q(status='approved')|Q(status='stopped')).order_by('dtstart')
            if consl.count() > 0:
                consl = consl[0]
                ctx['consultation'] = consl
            ctx['is_expert'] = False
        return ctx

class PermissionDenied(View):
    def get(self, request):
        return render(request, "403.html", {})


class ServerError(View):
    def get(self, request):
        return render(request, "500.html", {})


class PageNotFound(View):
    def get(self, request):
        return render(request, "404.html", {})

class ModeView(LeftLinkMixin, TemplateView):
    template_name = "private/mode.html"

    def get(self, request, *args, **kwargs):
        self.year = int(request.GET.get('year') if request.GET.get('year', False) else datetime.datetime.now().year)
        self.month = int(request.GET.get('month') if request.GET.get('month', False) else datetime.datetime.now().month)
        return super(ModeView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super(ModeView, self).get_context_data(**kwargs)
        rng = calendar.monthrange (self.year, self.month)[1]#.date(self.year, self.month+1, 1) - datetime.date(self.year, self.month, 1)).days
        st_dow = datetime.date(self.year, self.month, 1).weekday()
        cal = []
        if st_dow != 0:
            for i in xrange(st_dow):
                cal.append((u'', False, datetime.date(self.year, self.month, 1).strftime('%W')))
        for d in xrange(1, rng+1):
            nday = datetime.date(self.year, self.month, d)
            cal.append((d,
                        WorkDay.is_worked(user=self.request.user, date=nday),
                        nday.strftime('%W'),
                        WorkDay.get_periods(self.request.user, date=nday)))
        ctx['calendar'] = cal
#        print ctx
        ctx['now_year'] = self.year
        ctx['now_month'] = self.month
        ctx['now_month_human'] = MONTHS[self.month-1]

        ctx['prev_year'] = self.year
        ctx['prev_month'] = self.month-1
        ctx['prev_month_human'] = MONTHS[self.month-2]
        if ctx['prev_month'] == 0:
            ctx['prev_year'] = self.year-1
            ctx['prev_month'] = 12
            ctx['prev_month_human'] = MONTHS[11]

        ctx['next_year'] = self.year
        ctx['next_month'] = self.month+1
        if ctx['next_month'] == 13:
            ctx['next_year'] = self.year+1
            ctx['next_month'] = 1
            ctx['next_month_human'] = MONTHS[0]
        else:
            ctx['next_month_human'] = MONTHS[self.month]
        return ctx

class BalanceView(ListView):
    template_name = 'private/balance.html'
    queryset = Operation.objects.order_by('-date')
    paginate_by = 10
    context_object_name = 'operations'
    paginator_class = QuerySetDiggPaginator

    def get_paginator(self, queryset, per_page, orphans=0, allow_empty_first_page=True):
        return QuerySetDiggPaginator(queryset, per_page, padding=3, body=7, tail=1)

    def get_context_data(self, **kwargs):
        ctx = super(BalanceView, self).get_context_data(**kwargs)
        p_from = self.request.GET.get('payments-from', None)
        p_before = self.request.GET.get('payments-before', None)
        if p_before and p_before:
            ctx['p_before'] = p_before
            ctx['p_from'] = p_from
            ctx['pagination_amp'] = True
            sp_path = self.request.get_full_path().split('?')[1].split('&')
            for sp in sp_path:
                if 'page=' in sp:
                    sp_path.remove(sp)
            ctx['full_path'] = self.request.path + '?' + '&'.join(sp_path)
        if self.request.user.groups.filter(name='experts').count() > 0:
            ctx['is_expert'] = True
        if self.request.user.groups.filter(name='clients').count() > 0:
            ctx['is_expert'] = False
        o = Operation.objects.create(
            sum = 100.0,
            user = self.request.user
        )
        self.request.session['redirect_to_pay'] = False
        ctx['form_balance_pay'] = PaymentForm(data={
            'spShopId': settings.SPRYPAY.get('shopID'),
            'spCurrency': 'rur',
            'spPurpose': u'Пополнение баланса',
            'spIpnUrl': 'http://kabinet1.ru/sprypay/back/',
            'spIpnMethod': 1,
            'spFailMethod': 1,
            'lang': 'ru',
            'spShopPaymentId': o.pk,
            'spUserDataUserId': self.request.user.pk,
            'spSuccessUrl': u'http://kabinet1.ru{}'.format(reverse(viewname='expert-list')),
            'spSuccessMethod': '1',
            'spFailUrl': u'http://kabinet1.ru{}'.format(reverse(viewname='balance-view')),
            'spAmount': o.sum
        })
        return ctx

    def get_queryset(self):
        qs = self.queryset
        p_from = self.request.GET.get('payments-from', None)
        p_before = self.request.GET.get('payments-before', None)
        if p_before and p_before:
            qs = qs.filter(date__gte=p_from, date__lte=p_before)
        return qs.filter(user=self.request.user, status=True)

    def post(self, request, *args, **kwargs):
        r = 'NOT OK'
        if request.POST.get("payment-data", False):
            data = request.POST.get("payment-data")
            type_payment = u""
            if request.POST.get("payway") == "WM":
                type_payment = u"Web money"
            if request.POST.get("payway") == "card":
                type_payment = u"Вывод на карту Visa/MasterCard"
            if request.POST.get("payway") == "qiwi":
                type_payment = u"Вывод на кошелек Qiwi"
            if request.POST.get("payway") == "transfer":
                type_payment = u"Вывод через банковский перевод"
            msg = u'Пользователь: {}\n'.format(request.user.username)
            msg += u'Способ вывода: {}\n'.format(type_payment)
            msg += u'Реквизиты: {}\n'.format(data)
            order = OrderOutMoney.objects.create(
                user = request.user,
                type = type_payment,
                req = data
            )
            # mail_admins(u'Заявка на вывод денег', msg)
            r = 'OK'
        return HttpResponse(r)

class MyClientsView(ListView):
    template_name = 'private/my-clients.html'
    context_object_name = 'clients'
    paginate_by = 10

    def is_expert(self):
        return True if self.request.user.groups.filter(name='experts').count() > 0 else False

    def get_paginator(self, queryset, per_page, orphans=0, allow_empty_first_page=True):
        return QuerySetDiggPaginator(queryset, per_page, padding=3, body=7, tail=1)

    def get_context_data(self, **kwargs):
        ctx = super(MyClientsView, self).get_context_data(**kwargs)
        if self.is_expert():
            ctx['is_expert'] = True
        ctx['is_permanent_filter_active'] = self.request.GET.get('filter', False) == 'permanent'
        ctx['is_all_filter_active'] = self.request.GET.get('filter', False) == 'all'
        ctx['is_once_filter_active'] = self.request.GET.get('filter', False) == 'once'
        ctx['search_by_name'] = self.request.GET.get('search-by-name', '')
        ctx['sort_active'] = self.request.GET.get('sort', False)
        ctx['sort_last_up_active'] = self.request.GET.get('sort', False) == 'last-up'
        ctx['sort_last_down_active'] = self.request.GET.get('sort', False) == 'last-down'
        ctx['sort_next_down_active'] = self.request.GET.get('sort', False) == 'next-down'
        ctx['sort_next_up_active'] = self.request.GET.get('sort', False) == 'next-up'
        return ctx

    def get_queryset(self):
        ids = self.request.user.consultation_expert.filter(Q(status=u'done')|Q(status=u'approved')).values_list('client__id', flat=True)
        clients = User.objects.filter(id__in=ids)
        if self.request.GET.get('filter', False):
            f_val = self.request.GET.get('filter', False)
            if f_val == 'permanent':
                clients = clients.filter(consultation_client__expert=self.request.user).annotate(consultation_count=Count('consultation_client')).filter(consultation_count__gt=1)
            if f_val == 'once':
                clients = clients.filter(consultation_client__expert=self.request.user).annotate(consultation_count=Count('consultation_client')).filter(consultation_count=1)
        if self.request.GET.get('search-by-name', False):
            s = self.request.GET.get('search-by-name', None)
            clients = clients.filter(Q(first_name__icontains=s)|Q(last_name__icontains=s))
        if self.request.GET.get('sort', False):
            sort = self.request.GET.get('sort')
            if sort == 'last-up':
                clients = clients.annotate(cc_dtstart=Max('consultation_client__dtstart')).order_by('cc_dtstart')
            if sort == 'last-down':
                clients = clients.annotate(cc_dtstart=Max('consultation_client__dtstart')).order_by('-cc_dtstart')
            if sort == 'next-up':
                clients = clients.annotate(cc_dtstart=Max('consultation_client__dtstart')).order_by('cc_dtstart')
            if sort == 'next-down':
                clients = clients.annotate(cc_dtstart=Max('consultation_client__dtstart')).order_by('-cc_dtstart')
        return clients.distinct()

class ScheduleView(ListView):
    template_name = 'private/schedule.html'
    model = Consultation
    context_object_name = 'cons'
    paginate_by = 8
    paginator_class = QuerySetDiggPaginator

    def get_paginator(self, queryset, per_page, orphans=0, allow_empty_first_page=True):
        return QuerySetDiggPaginator(queryset, per_page, padding=3, body=7, tail=1)

    def get_queryset(self):
        if self.request.GET.get('day'):
            date = self.request.GET.get('day').split('.')
            year = int(date[2])
            month = int(date[1])
            day = int(date[0])
            date = datetime.date(
                year, month, day
            )
            min_date = datetime.datetime.combine(date, datetime.time.min)
            max_date = datetime.datetime.combine(date, datetime.time.max)
            user_cons = self.request.user.consultation_expert if self.is_expert() else self.request.user.consultation_client
            return user_cons.filter(Q(status='approved')|Q(status='canceled')|Q(status='assigned')).filter(dtstart__range=(min_date, max_date))
        return Consultation.objects.none()

    def is_expert(self):
        return True if self.request.user.groups.filter(name='experts').count() > 0 else False

    def post(self, request, **kwargs):
        if request.POST.get('consultation', False) and request.POST.get('agree', False):
            c = Consultation.objects.get(pk=request.POST.get('consultation'))
            if request.POST.get('agree') == '1':
                # обработать заявку на консультацию
                user_msg =\
u"""Специалист {} {} подтвердил заказанную консультацию.
Дата и время: {}
Продолжительность: {} мин""".format(
                    c.expert.first_name, c.expert.last_name,
                    pytils.dt.ru_strftime(u"%d %B %Y в %H:%M", date=timezone.localtime(c.dtstart), inflected=True),
                    c.length)
                msg = \
u"""Здравствуйте.

Специалист {} {} подтвердил заказанную консультацию.
Дата и время: {}
Продолжительность: {} мин

Пожалуйста, войдите в свой личный кабинет на сайте и перейдите в раздел “Моя консультация” в назначенное время.

Служба поддержки Кабинета №1
support@kabinet1.ru
www.kabinet1.ru""".format(c.expert.first_name, c.expert.last_name,
                    pytils.dt.ru_strftime(u"%d %B %Y в %H:%M", date=timezone.localtime(c.dtstart), inflected=True),
                    c.length)
                send_mail(u'Кабинет №1 - Консультация подтверждена', msg, u'Кабинет №1 <support@kabinet1.ru>', [c.client.email])
                send_mail(u'Кабинет №1 - Консультация подтверждена', msg, u'Кабинет №1 <support@kabinet1.ru>', ['info@kabinet1.ru'])
                c.status=u'approved'
            else:
                user_msg =\
u"""Специалист {} {} не подтвердил заказанную консультацию.
Дата и время: {}
Продолжительность: {} мин""".format(
                    c.expert.first_name, c.expert.last_name,
                    pytils.dt.ru_strftime(u"%d %B %Y в %H:%M", date=timezone.localtime(c.dtstart), inflected=True),
                    c.length)
                msg = \
u"""Здравствуйте.

Специалист {} {} не подтвердил заказанную консультацию.
Дата и время: {}
Продолжительность: {} мин

Вы можете попробовать выбрать другое время для консультации с этим специалистом либо выбрать другого специалиста на нашем сайте.

Служба поддержки Кабинета №1
support@kabinet1.ru
www.kabinet1.ru""".format(c.expert.first_name, c.expert.last_name,
                    pytils.dt.ru_strftime(u"%d %B %Y в %H:%M", date=timezone.localtime(c.dtstart), inflected=True),
                    c.length)
                msg += u'Специалист отменил консультацию\n' + msg
                send_mail(u'Кабинет №1 - Консультация отменена', msg, u'Кабинет №1 <support@kabinet1.ru>', [c.client.email])
                c.status = u'canceled'
            c.save()
            cache_key = "messages-for-{}".format(c.client.pk)
            if cache.get(cache_key):
                cache_data = json.loads(cache.get(cache_key))
                cache_data.append(
                    {
                        int(100*time.time()): user_msg.replace(u'\n', u'<br/>')
                    }
                )
                cache_data = json.dumps(cache_data)
#                cache_data = unicode(cache.get(cache_key)) + u"][" + user_msg.replace(u"\n", u"<br/>")
            else:
                cache_data = []
                cache_data.append(
                    {
                        int(100*time.time()): user_msg.replace(u'\n', u'<br/>')
                    }
                )
                cache_data = json.dumps(cache_data)
#                cache_data = user_msg.replace("\n", "<br/>")
            cache.set(cache_key, cache_data, 3600)
            UserMessage.objects.create(
                to_user=c.client,
                msg = user_msg.replace("\n", "<br/>")
            )
        self.object_list = self.get_queryset()
        return self.render_to_response(self.get_context_data(object_list=self.object_list))

    def get_context_data(self, **kwargs):
        ctx = super(ScheduleView, self).get_context_data(**kwargs)
        if self.is_expert():
            ctx['is_expert'] = True
            ctx['consultations'] = Consultation.objects.filter(expert=self.request.user, status=u'assigned',
            dtstart__gt=datetime.datetime.now()).order_by('dtstart')
            ctx['new_order_count'] = u'{} {}'.format(ctx['consultations'].count(), pytils.numeral.choose_plural(ctx['consultations'].count(), (u'заявка', u'заявки', u'заявок')))
        self.year = int(self.request.GET.get('year', datetime.datetime.now().year))
        self.month = int(self.request.GET.get('month', datetime.datetime.now().month))
        rng = calendar.monthrange (self.year, self.month)[1]#.date(self.year, self.month+1, 1) - datetime.date(self.year, self.month, 1)).days
        st_dow = datetime.date(self.year, self.month, 1).weekday()
        cal = []
        if st_dow != 0:
            for i in xrange(st_dow):
                cal.append((u'', False))
        for d in xrange(1, rng+1):
            if self.is_expert():
                date = datetime.datetime(self.year, self.month, d)
                min_date = datetime.datetime.combine(date, datetime.time.min)
                max_date = datetime.datetime.combine(date, datetime.time.max)

                consl =self.request.user.consultation_expert\
                .filter(Q(status='approved')|Q(status='assigned')|Q(status='canceled'))\
                .filter(
                    dtstart__range=(min_date, max_date)).order_by('-dtstart')
                if consl.count() > 0:
                    cal.append((d, True,
                                Consultation.is_approved_by_day(expert=self.request.user, date=date),
                                Consultation.is_assigned_by_day(expert=self.request.user, date=date),
                                Consultation.is_canceled_by_day(expert=self.request.user, date=date),
                        ))
                else:
                    cal.append((d, False, ""))
            else:
                date = datetime.datetime(self.year, self.month, d)
                min_date = datetime.datetime.combine(date, datetime.time.min)
                max_date = datetime.datetime.combine(date, datetime.time.max)

                consl =self.request.user.consultation_client\
                .filter(Q(status='approved')|Q(status='assigned')|Q(status='canceled'))\
                .filter(
                    dtstart__range=(min_date, max_date)).order_by('-dtstart')
                if consl.count() > 0:
                    cal.append((d, True,
                                Consultation.is_approved_by_day(client=self.request.user, date=date),
                                Consultation.is_assigned_by_day(client=self.request.user, date=date),
                                Consultation.is_canceled_by_day(client=self.request.user, date=date),
                        ))
                else:
                    cal.append((d, False, ""))
        ctx['calendar'] = cal
        if self.is_expert():
            ctx['all_consultations'] = self.request.user.consultation_expert.order_by('dtstart').distinct()
        else:
            ctx['all_consultations'] = self.request.user.consultation_client.order_by('dtstart').distinct()
        ctx['now_year'] = self.year
        ctx['now_month'] = self.month
        ctx['now_month_human'] = MONTHS[self.month-1]

        ctx['prev_year'] = self.year
        ctx['prev_month'] = self.month-1
        ctx['prev_month_human'] = MONTHS[self.month-2]
        if ctx['prev_month'] == 0:
            ctx['prev_year'] = self.year-1
            ctx['prev_month'] = 12
            ctx['prev_month_human'] = MONTHS[11]

        ctx['next_year'] = self.year
        ctx['next_month'] = self.month+1
        if ctx['next_month'] == 13:
            ctx['next_year'] = self.year+1
            ctx['next_month'] = 1
            ctx['next_month_human'] = MONTHS[0]
        else:
            ctx['next_month_human'] = MONTHS[self.month]
        return ctx

class CheckConsultationApproved(View):
    def get(self, request):
        for c in Consultation.objects.filter(status='assigned').all():
            d = c.created + datetime.timedelta(hours=1)
            d2 = c.created + datetime.timedelta(hours=2)
            if django.utils.timezone.now() > d and django.utils.timezone.now() < d2:
                c.comment = u'Мы не получили ответа от специалиста в течение установленного срока. Возможно специалист ответит позднее. Вы можете ожидать ответа или бесплатно отказаться от консультации.'
                c.save()
                msg = u"""
Здравствуйте.

Специалист {} не подтвердил заказанную консультацию в течение 1 часа после заказа.
Дата и время: {}
Продолжительность: {} мин

Вы можете выбрать другого специалиста на нашем сайте либо попытаться повторить заказ позже.


Служба поддержки Кабинета №1
support@kabinet1.ru
www.kabinet1.ru""".format(
                    u'{} {}'.format(c.expert.first_name, c.expert.last_name)\
                    if c.expert.first_name and c.expert.last_name else c.expert.username,
                    pytils.dt.ru_strftime(u"%d %B %Y в %H:%M", c.dtstart),
                    c.length
                )
                send_mail(u'Кабинет №1 - Консультация не подтверждена', msg, u'Кабинет №1 <support@kabinet1.ru>', [c.client.email])
        return HttpResponse("OK")

class FAQView(LeftLinkMixin, ListView):
    template_name = 'private/faq.html'
    context_object_name = 'faqs'
    model = FAQuestion

    def get_queryset(self):
        if self.request.user.groups.filter(name='clients').count() > 0:
            return self.model.objects.filter(for_user='0')
        if self.request.user.groups.filter(name='experts').count() > 0:
            return self.model.objects.filter(for_user='1')
        return self.model.objects.all()

class RecalAfterConsultationView(View):

    def post(self, request, *args, **kwargs):
        if request.POST.get('feedback-text'):
            text = request.POST.get('feedback-text')
            expert = User.objects.get(pk=request.POST.get('expert_pk'))
            client = request.user
            is_anonymous = True if request.POST.get('anonymous') else False
            type = u'2'
            approved = False
            rc = Recal(
                text=text,
                client=client,
                expert=expert,
                approved=approved,
                type=type,
                hide_author=is_anonymous
            )
            rc.save()
            return HttpResponse(rc.pk)
        return HttpResponse('OK')

check_consultation_approved = CheckConsultationApproved.as_view()
profile_view = ProfileView.as_view()
mode_view = ModeView.as_view()
messages_view = MessagesProfileView.as_view()
permission_denied = PermissionDenied.as_view()
server_error = ServerError.as_view()
page_not_found = PageNotFound.as_view()
balance_view = login_required(BalanceView.as_view())
consultation_view = login_required(ConsultationView.as_view())
my_clients_view = login_required(MyClientsView.as_view())
schedule_view = login_required(ScheduleView.as_view())
faq_view = FAQView.as_view()
recal_after_consultation_view = RecalAfterConsultationView.as_view()