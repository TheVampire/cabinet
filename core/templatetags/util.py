#coding: utf-8
from django import template
from django.template.base import TemplateSyntaxError
from django.template.loaders.app_directories import Loader
from django.utils import timezone
from expert.models import Consultation
import pytils, datetime, re
from django.utils.html import escape
from django.utils.safestring import mark_safe, SafeData, mark_for_escaping
from django.template.defaultfilters import stringfilter
from django.utils.text import normalize_newlines

loader = Loader()
register = template.Library()

@register.filter
def is_expert(user):
    if user.is_authenticated():
        return user.get_profile().is_expert()
    else:
        return False

@register.filter
def if_last_consultation_date(user, expert):
    con = Consultation.objects.filter(expert=expert, client=user, status=u'done').order_by('-dtstart')
    if con.count() > 0:
        return True
    else:
        return False

@register.filter('truncate_expert_msg')
def truncate_expert_msg(value):
    return value.split(u'Продолжительность')[0]

@register.filter(is_safe=True, needs_autoescape=True)
@stringfilter
def unlinebreaksbr(value, autoescape=None):
    autoescape = autoescape and not isinstance(value, SafeData)
    value = normalize_newlines(value)
    value = value.replace('<br />', '\n')
    value = value.replace('<br/>', '\n')
    if autoescape:
        value = escape(value)
    return mark_safe(value.replace('\n', '<br />'))

@register.filter('intspace')
def intspace(value):
    """
    Converts an integer to a string containing spaces every three digits.
    For example, 3000 becomes '3 000' and 45000 becomes '45 000'.
    See django.contrib.humanize app
    """
    orig = unicode(value)
    new = re.sub("^(-?\d+)(\d{3})", '\g<1> \g<2>', orig)
    if orig == new:
        return new
    else:
        return intspace(new)

@register.simple_tag
def wizzard_group_name(val):
    if ' - ' in val:
        return val.split(' - ')[1]
    return val

@register.simple_tag
def constatus(val):
    if val == u'assigned':
        return u'<span class="assigned">Ожидает подтверждения</span>'
    elif val == u"approved":
        return u'<span class="done">Подтверждена</span>'
    elif val == u"canceled":
        return u'<span class="cancel">Отменена</span>'

@register.simple_tag
def ru_strftime(format, date):
    if timezone.is_naive(date):
        return pytils.dt.ru_strftime(format=format, date=timezone.make_aware(date, timezone.get_current_timezone()), inflected=True)
    else:
        return pytils.dt.ru_strftime(format=format, date=timezone.localtime(date), inflected=True)

@register.simple_tag
def next_consultation_date(expert, user):
    con = Consultation.objects.\
    filter(dtstart__gte=datetime.datetime.now()).filter(expert=expert, client=user, status=u'assigned').order_by('dtstart')
    if con.count() > 0:
        con = con[0]
        date_str = pytils.dt.ru_strftime(format=u'%a %d %B в %H:%M', date=timezone.localtime(con.dtstart))
    else:
        con = ''
        date_str = u'Не назначена'
    return date_str

@register.simple_tag
def date_and_length_ru(val):
    start = timezone.localtime(val.dtstart)
    end = start + datetime.timedelta(minutes=val.length)
    str = u'{} - {}'.format(pytils.dt.ru_strftime(format=u'%d %B %H:%M', date=start, inflected=True), pytils.dt.ru_strftime(format=u'%H:%M', date=end))
    return str

@register.simple_tag
def last_consultation_date(expert, user):
    con = Consultation.objects.filter(expert=expert, client=user, status=u'done').order_by('-dtstart')
    if con.count() > 0:
        con = con[0]
        date_str = pytils.dt.ru_strftime(format=u'%a %d %B в %H:%M', date=timezone.localtime(con.dtstart))
    else:
        con = ''
        date_str = ''
    return date_str

@register.simple_tag
def last_consultation_length(expert, user):
    con = Consultation.objects.filter(expert=expert, client=user, status=u'done').order_by('-dtstart')
    if con.count() > 0:
        con = con[0]
        hour = con.length/60
        min = con.length%60
        date_str = u'Продолжительность {} час {} мин'.format(hour, min)
    else:
        con = ''
        date_str = ''
    return date_str

@register.simple_tag
def active(request, pattern):
    import re

    if re.search(pattern, request.path):
        return 'active'
    return '1212'


@register.simple_tag
def active0(request, pattern):
    if pattern == request.path:
        return 'active'
    return ''


def do_include_raw(parser, token):
    """
    Performs a template include without parsing the context,
    just dumps the template in.
    """
    bits = token.split_contents()
    if len(bits) != 2:
        raise TemplateSyntaxError(
            "%r tag takes one argument: "
            "the name of the template to be included" % bits[0])

    template_name = bits[1]
    if template_name[0] in ('"', "'") and template_name[-1] == template_name[0]:
        template_name = template_name[1:-1]

    source, path = loader.load_template_source(template_name)

    return template.TextNode(source)

register.tag("include_raw", do_include_raw)



def tablecols(data, cols):
    rows = []
    row = []
    index = 0
    for user in data:
        row.append(user)
        index = index + 1

        if index % cols == 0:
            rows.append(row)
            row = []

    # Still stuff missing?
    # see how many are missing and add empty elements to complete the row
    if len(row) > 0:
        remaining = len(row)
    else:
        remaining = 0

    to_add = cols - remaining

    for c in range(to_add):
        row.append([])

    rows.append(row)

    return rows

register.filter_function(tablecols)

@register.filter('klass')
def klass(ob):
    return ob.__class__.__name__


