from django import template
from django.conf import settings
from django.template import Context
from django.template.loader import get_template


register = template.Library()


@register.filter
def as_bootstrap(form):
    """
    @todo: rewrite to simple_tag receiving context
    """
    template = get_template("bootstrap/form.html")
    c = Context({"form": form, 'MEDIA_URL': settings.MEDIA_URL})
    return template.render(c)

@register.filter
def if_in_list(val, list):
    if val in list or u'{}'.format(val) in list:
        return True
    return False

@register.filter
def css_class(field):
    return field.field.widget.__class__.__name__.lower()
