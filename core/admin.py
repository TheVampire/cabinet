#coding: UTF-8
from django.contrib.admin import site, ModelAdmin, StackedInline

from django.contrib import admin
from django.contrib.flatpages.models import FlatPage

from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.db.models import Q

from django.contrib.flatpages.admin import FlatPageAdmin as FlatPageAdminOld
from django.contrib.flatpages.admin import FlatpageForm as FlatpageFormOld

from django import forms
from ckeditor.widgets import CKEditorWidget

from models import Profile, Banner, FAQuestion, Aphorism
from core.models import UserMessage, OrderOutMoney

class FlatpageForm(FlatpageFormOld):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = FlatPage # this is not automatically inherited from FlatpageFormOld


class FlatPageAdmin(FlatPageAdminOld):
    form = FlatpageForm


class ProfileInline(StackedInline):
    model = Profile


class UserAdmin(ModelAdmin):
    list_display = ("username", 'email', "first_name", "last_name", 'last_login', 'date_joined')
    filter_horizontal = ['user_permissions',]
    search_fields = ['username', 'email']
    inlines = [ProfileInline, ]


class ProfileAdmin(ModelAdmin):

    list_display = ('user',)

admin.site.unregister(FlatPage)
admin.site.unregister(User)
admin.site.register(UserMessage)
admin.site.register(User, UserAdmin)
admin.site.register(FlatPage, FlatPageAdmin)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(Banner)
admin.site.register(FAQuestion)
admin.site.register(OrderOutMoney)
admin.site.register(Aphorism)
