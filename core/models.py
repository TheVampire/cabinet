#coding: UTF-8
from django.db import models
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from sprypay.models import Balance
from core.utils import user_add_group
from django.conf import settings
from django_ulogin.signals import assign
from django.contrib.auth.signals import user_logged_in, user_logged_out

from django.dispatch import receiver
from django.contrib.auth import login


from registration.signals import user_activated
from pybb.models import PybbProfile

from core.utils import is_user_in_group

from choices import TIMEZONES
import pytils
from utils import is_user_in_group
from datetime import datetime
from django.utils.crypto import get_random_string
from pytils import translit

def create_user_by_ulogin(request, ulogin_response):
    email = ulogin_response.get('email', u'')
    if email:
        if User.objects.filter(email=email).exists():
            return User.objects.get(email=email)
    return User.objects.create_user(
        username=translit.translify(unicode(ulogin_response.get('last_name'))),
        password=get_random_string(10, '0123456789abcdefghijklmnopqrstuvwxyz'),
        email=email
    )

class Aphorism(models.Model):
    text = models.TextField(verbose_name=u'Текст')
    author = models.CharField(max_length=256, verbose_name=u'Автор')

    class Meta:
        verbose_name = u'Афоризм'
        verbose_name_plural = u'Афоризмы'

    def __unicode__(self):
        return u'{} #{}'.format(self.author, self.pk)

class FAQuestion(models.Model):
    q = models.CharField(verbose_name=u'Вопрос', max_length=1024)
    ans = models.TextField(verbose_name=u'Ответ')
    for_user = models.CharField(verbose_name=u'Для кого', choices=(
        ('0', u'Клиент'),
        ('1', u'Специалист')
    ), max_length=2)

    def __unicode__(self):
        return self.q

    class Meta:
        verbose_name = u'FA Question'
        verbose_name_plural = u'FAQs'

class OrderOutMoney(models.Model):
    user = models.ForeignKey(User, verbose_name=u'Специалист')
    type = models.CharField(verbose_name=u'Способ', max_length=64)
    req = models.TextField(verbose_name=u'Реквизиты')
    done = models.BooleanField(verbose_name=u'Выполнено', default=False)
    date = models.DateTimeField(verbose_name=u'Дата создания', auto_now_add=True)

    class Meta:
        verbose_name = u'Заявка на вывод средств'
        verbose_name_plural = u'Заявки на вывод средств'

    def __unicode__(self):
        return u'[{}] Заявка на вывод средств пользователя {}'.format(self.date, self.user.username)


class UserMessage(models.Model):
    to_user = models.ForeignKey(User, verbose_name=u'Кому', related_name=u'usermessages_to')
    from_user = models.ForeignKey(User, verbose_name=u'От кого', blank=True, null=True, related_name=u'usermessages_from')
    msg = models.TextField(u'Сообщение')
    created = models.DateTimeField(u'Дата отправки', auto_now_add=True)

    def ru_created(self):
        return pytils.dt.ru_strftime(u"%d %B %Y %H:%M",date= timezone.localtime(self.created), inflected=True)

    def __unicode__(self):
        return u'Сообщение от {} для {}'.format(self.from_user, self.to_user)

    class Meta:
        verbose_name = u'Пользовательское сообщение'
        verbose_name_plural = u'Пользовательские сообщения'

class Profile(PybbProfile):

    class Meta:
        verbose_name = u"пользовательский профиль"
        verbose_name_plural = u"пользовательские профили"

    user = models.OneToOneField(User, verbose_name = u"пользователь")
    online = models.BooleanField(
        default=True, verbose_name = u"пользователь онлайн")

    middlename = models.CharField(verbose_name=u'Отчество', max_length=64, blank=True, null=True)
    country = models.CharField(verbose_name=u'Страна, Город', max_length=128, blank=True, null=True)
    subscribe = models.BooleanField(verbose_name=u'Получать рассылку на почту', default=True)
    birthday = models.DateField(verbose_name=u'Дата рождения', default=datetime.now())

    _phone_number = models.CharField(
        max_length=255, default="", verbose_name=u"номер телефона")

    @property
    def phone_number(self):
        """
        For experts it is reasonable 2 make this field hidden
        """
        if is_user_in_group(self.user, "experts"):
            return self.user.expertblank.phone_number
        else:
            return self._phone_number

    def is_expert(self):
        if is_user_in_group(self.user, "experts"):
            return True
        return False

    @property
    def photo(self):
        if is_user_in_group(self.user, "experts"):
            return self.user.expertblank.avatar
        else:
            return self.avatar

    def __unicode__(self):
        return u'профиль пользователя %s' % self.user.username

    @staticmethod
    def create_profile(sender, **kwargs):
        user = kwargs["user"]
        if not Balance.objects.filter(user=user).exists():
            Balance.objects.get_or_create(
                user=user,
                sum=0.0
            )
        if user.groups.filter(name='experts').count() == 0:
            user_add_group(user, 'clients')

        profile = Profile.objects.get_or_create(user=user)[0]
        profile.save()

class Banner(models.Model):
    PLACE = (('L', u'Слева'), ('R', u'Справа'))
    path = models.TextField(verbose_name=u'URL страницы, где выводить баннер')
#    content = models.TextField(verbose_name=u'Код баннера')
    image = models.ImageField(upload_to='pictures/', verbose_name=u'Изображение для баннера', blank=True, null=True)
    image_link = models.TextField(verbose_name=u'Ссылка на изображение с внешнего сервера', blank=True, null=True)
    link = models.TextField(verbose_name=u'Целевая ссылка')
    place = models.CharField(max_length=1, choices=PLACE, default='L', verbose_name=u'Положение на странице')

    def __unicode__(self):
        return u'Баннер на страницу ' + self.path

    class Meta:
        verbose_name = u'Баннер'
        verbose_name_plural = u'Баннеры'

def user_log_in(sender, user, *args, **kwargs):

    try:
        profile = user.profile
        profile.online = True
        profile.save()
    except ObjectDoesNotExist:
        pass


def user_log_out(sender, user, *args, **kwargs):

    try:
        profile = user.profile
        profile.online = False
        profile.save()
    except ObjectDoesNotExist:
        pass

@receiver(user_activated)
def login_on_activation(sender, user, request, **kwargs):
    """Logs in the user after activation"""
    user.backend = 'django.contrib.auth.backends.ModelBackend'
    login(request, user)

@receiver(user_activated)
def post_activation_hdlr(sender, **kwargs):
    user = kwargs.get('user')
    request = kwargs.get('request')
    if user.groups.filter(name='experts').count() == 0:
        user_add_group(user, 'clients')
        if not request.session.get("container"):
            request.session["container"] = {}

user_activated.connect(Profile.create_profile)
user_activated.connect(post_activation_hdlr)
assign.connect(Profile.create_profile)

#user_logged_in.connect(user_log_in)
#user_logged_out.connect(user_log_out)
