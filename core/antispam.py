# coding: utf-8
from django.forms import ValidationError

AKISMET_API_KEY = '22b6724edf4d'

def check_text(user, body):
    try:
        from akismet import Akismet
    except:
        return

    ak = Akismet(
        key = AKISMET_API_KEY,
        blog_url = 'http://kabinet1.ru/'
    )
    if ak.verify_key():
        data = {
            'user_ip': '127.0.0.1',
            'user_agent': ''
        }
        if ak.check_comment(body.encode('utf-8'), data=data, build_data=True):
            raise ValidationError(u'Это сообщение выглядит как СПАМ')
