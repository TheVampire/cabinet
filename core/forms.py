#coding: UTF-8

from django import forms
from django.forms import ModelForm, Form, CharField

from registration.forms import RegistrationFormUniqueEmail

from models import Profile
from expert.models import Issue
#from pybb.models import TZ_CHOICES
import calendar
from expert.choices import COUNTRIES
from django.contrib.auth.models import User

TZ_CHOICES = [(float(x[0]), x[1]) for x in (
    (-12, u'-12 Кваджалейн'),
    (-11, u'-11 Американское Самоа'),
    (-10, u'-10 Гавайи'),
#    (-9.5, u'-09.5'),
    (-9, u'-09 Аляска'),
#    (-8.5, u'-08.5'),
    (-8, u'-08 Тихоокеанское время'),
    (-7, u'-07 Горное время'),
    (-6, u'-06 Центральное время'),
    (-5, u'-05 Североамериканское восточное время (США и Канада)'),
    (-4, u'-04  Атлантическое время (Канада)'),
    (-3.5, u'-03.5 Ньюфаундленд'),
    (-3, u'-03 Южноамериканское восточное время (Бразилиа, Буэнос-Айрес, Джорджтаун)'),
    (-2, u'-02  Среднеатлантическое время'),
    (-1, u'-01  Азорские острова, Кабо-Верде'),
    (0, u'00 Западноевропейское время (Дублин, Эдинбург, Лиссабон, Лондон)'),
    (1, u'+01 Центральноевропейское время (Амстердам, Берлин, Берн, Брюссель, Вена)'),
    (2, u'+02 Афины, Бухарест, Вильнюс, Киев, Кишинёв, Рига, София'),
    (3, u'+03 Калининград'),
    (3.5, u'+03.5 Тегеран'),
    (4, u'+04 Москва'),
    (4.5, u'+04.5 Афганистан'),
    (5, u'+05 Узбекистан'),
    (5.5, u'+05.5 Индия'),
    (6, u'+06 Екатеринбург'),
    (6.5, u'+06.5 Мьянма'),
    (7, u'+07 Омск'),
    (8, u'+08 Красноярск'),
    (9, u'+09 Иркутск'),
    (9.5, u'+09.5 Дарвин'),
    (10, u'+10 Якутск'),
#    (10.5, u'+10.5 '),
    (11, u'+11 Владивосток'),
#    (11.5, u'+11.5'),
    (12, u'+12 Магадан'),
    (13, u'+13 Самоа'),
    (14, u'+14 Кирибати'),
    )]


MONTHS = [u"Январь",u"Февраль",u"Март",u"Апрель",u"Май",u"Июнь",u"Июль",u"Август",u"Сентябрь",u"Октябрь",u"Ноябрь",u"Декабрь"]

class PrivateExpertForm(Form):
    email = forms.EmailField(max_length=1024, required=False)

    send_me_q = forms.BooleanField(required=False)

    old_password = forms.CharField(max_length=100, widget=forms.PasswordInput, required=False)
    new_password = forms.CharField(max_length=100, widget=forms.PasswordInput, required=False)
    repeat_new_password = forms.CharField(max_length=100, widget=forms.PasswordInput, required=False)

    timezone = forms.ChoiceField(choices=TZ_CHOICES, required=False)

    name = forms.CharField(max_length=100, required=False)
    surname = forms.CharField(max_length=100, required=False)
    middlename = forms.CharField(max_length=100, required=False)
    birth_day = forms.ChoiceField(choices=[(i, str(i)) for i in xrange(1,32)], widget=forms.Select(attrs={'class': 'private-data-day'}), required=False)
    birth_month = forms.ChoiceField(choices=[(i, MONTHS[i-1]) for i in xrange(1,13)], widget=forms.Select(attrs={'class': 'private-data-month'}), required=False)
    birth_year = forms.ChoiceField(choices=[(i, str(i)) for i in xrange(1955,2013)], widget=forms.Select(attrs={'class': 'private-data-year'}), required=False)

    country = forms.ChoiceField(choices=COUNTRIES, required=False)
    city = forms.CharField(max_length=100, required=False)

    phone = forms.CharField(max_length=100, required=False)

    photo = forms.ImageField(required=False)

    about = forms.CharField(widget=forms.Textarea, required=False)

    problems = forms.ModelMultipleChoiceField(queryset=Issue.objects.all(), widget=forms.CheckboxSelectMultiple, required=False)

    subscribe = forms.BooleanField(label=u'Получать рассылку на почту', initial=True, required=False)

    free_consult = forms.BooleanField(label=u'Проводу бесплатные консультации', initial=False, required=False)

    fix = forms.IntegerField(required=False, widget=forms.TextInput(attrs={'class': 'validate[required,min[500]]'}))
    consultation_time = forms.IntegerField(required=False, widget=forms.TextInput(attrs={'class': 'validate[required]'}))

class PrivateClientForm(Form):
    email = forms.EmailField(max_length=1024, required=False)

    old_password = forms.CharField(max_length=100, widget=forms.PasswordInput, required=False)
    new_password = forms.CharField(max_length=100, widget=forms.PasswordInput, required=False)
    repeat_new_password = forms.CharField(max_length=100, widget=forms.PasswordInput, required=False)

    timezone = forms.ChoiceField(choices=TZ_CHOICES, required=False)

    name = forms.CharField(max_length=100, required=False)
    surname = forms.CharField(max_length=100, required=False)
    middlename = forms.CharField(max_length=100, required=False)
    birth_day = forms.ChoiceField(choices=[(i, str(i)) for i in xrange(1,32)], widget=forms.Select(attrs={'class': 'private-data-day'}), required=False)
    birth_month = forms.ChoiceField(choices=[(i, MONTHS[i-1]) for i in xrange(1,13)], widget=forms.Select(attrs={'class': 'private-data-month'}), required=False)
    birth_year = forms.ChoiceField(choices=[(i, str(i)) for i in xrange(1955,2013)], widget=forms.Select(attrs={'class': 'private-data-year'}), required=False)

    country = forms.CharField(max_length=100, required=False)
    phone = forms.CharField(max_length=100, required=False)

    subscribe = forms.BooleanField(label=u'Получать рассылку на почту', initial=True, required=False)

class ProfileEditForm(ModelForm):

    class Meta:
        model = Profile
        exclude = ("user", "signature_html", "post_count")


class MyRegForm(RegistrationFormUniqueEmail):
    pass

class PayRegForm(ModelForm):

    class Meta:
        model = User
        fields = ('username', 'email')


class SearchForm(Form):
    query = CharField(required=False)


#add help texts to form fields
def patch_form(form, fields_help_texts_dict):

    for field in fields_help_texts_dict:
        if field in form.base_fields:
            form.base_fields[field].help_text = fields_help_texts_dict[field]

    form.base_fields['username'].widget.attrs['class'] = 'validate[required,custom[onlyAZdigchar,ajax[ajaxUsernameCall]]]'
    form.base_fields['email'].widget.attrs['class'] = 'validate[required,custom[email,ajax[ajaxUserEmailCall]]]'
    form.base_fields['password1'].widget.attrs['class'] = 'validate[required]'
    form.base_fields['password2'].widget.attrs['class'] = 'validate[required, equals[id_password1]]'
    return form


#help texts for registration form
HELPS = {
"username": u"""
При желании Вы можете использовать любой 
псевдоним, а не Ваше настоящее имя""",

"email": u"""
Ваш адрес не будет опубликован на сайте и ни при каких обстоятельствах не будет 
передан третьим лицам""",

"password1": u"""
Рекомендуем составить пароль не менее 7 знаков с использованием латинских букв 
и цифр""",
}