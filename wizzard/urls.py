from django.conf.urls import patterns, include, url


urlpatterns = patterns('wizzard.views',
    url(r"^$", 'wizzard_view', name='wizzard'),
    url(r"^create/user/$", 'create_user_by_order', name='wizzard-create-user'),
    url(r"^get_periods_for_choose/$", 'wizzard_get_periods_for_choose', name='wizzard_get_periods_for_choose'),
    url(r"^get_periods/$", 'wizzard_get_periods', name='wizzard_get_periods'),
    url("(?P<step>.+)/$", 'simple_wizzard_view', name='wizzard_step'),
)
