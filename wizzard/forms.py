from django import forms

from expert.choices import CONSULTATION_TYPES


class Step1Form(forms.Form):

    category = forms.IntegerField()


class Step2Form(forms.Form):

    issue = forms.IntegerField()


class Step3Form(forms.Form):

    expert = forms.IntegerField()


class Step4Form(forms.Form):

#    type_tag = forms.ChoiceField(choices=CONSULTATION_TYPES)
    value = forms.IntegerField()
    datetime = forms.DateTimeField(input_formats=('%d.%m.%Y-%H-%M',))


class Step5Form(forms.Form):

    some = forms.IntegerField()