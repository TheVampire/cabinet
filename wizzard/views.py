# coding: utf-8
import logging
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
from django.contrib.sites.models import RequestSite
from django.contrib.sites.models import Site
from registration.models import RegistrationProfile
from registration.backends.default.views import RegistrationView
from registration import signals

from django.conf import settings
from sprypay.forms import PaymentForm
from django.views.generic import TemplateView, View
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404, render_to_response, RequestContext
from django.contrib.formtools.wizard.views import SessionWizardView, NamedUrlSessionWizardView
from django.contrib.auth.models import User
from django.contrib.formtools.wizard.forms import ManagementForm
from onpay.views import IframeGenerator
from sprypay.models import Operation, Balance
from django.shortcuts import redirect
from django.contrib.auth.forms import AuthenticationForm
from django.core.mail import send_mail
from sms.models import SMSConsultationNotification
from expert.models import Issue, ExpertBlank, Consultation, WorkDay, WorkHour
from expert.forms import ExpertQuestionForm
from core.utils import get_or_none
from core.forms import MONTHS, PayRegForm
from core.models import UserMessage
import datetime, pytils
from forms import *
import calendar
from django.utils import timezone
from django.db.models import Q
import json, time, decimal
from django.forms import formsets, ValidationError
from sms.utils import send_message
from django.core.cache import cache
from registration.forms import RegistrationFormUniqueEmail


FORMS = [("category", Step1Form),
         ("issue",    Step2Form),
         ("experts",  Step3Form),
         ("time",     Step4Form),
         ("pay",      Step5Form)
         ]

TEMPLATES = {"category": "wizard_step_1.html",
             "issue":    "wizard_step_2.html",
             "experts":  "wizard_step_3.html",
             "time":     "wizard_step_4.html",
             "pay":      "wizard_step_5.html",
             }
SW_TEMPLATES = {"category": "simple_wizard_step_1.html",
             "issue":    "simple_wizard_step_2.html",
             "experts":  "simple_wizard_step_3.html",
             "time":     "simple_wizard_step_4.html",
             "pay":      "simple_wizard_step_5.html",
             }

def create_user_by_order(request):
    if request.POST:
        name = request.POST.get('name')
        oname = pytils.translit.translify(name)
        name = oname
        email = request.POST.get('email')
        ii = 1
        while User.objects.filter(username=name).exists():
            ii+=1
            name = u'{}_{}'.format(oname, ii)
        password = User.objects.make_random_password()
        user = User.objects.create_user(
            username = name,
            password=password,
            email=email
        )

        msg = u'Username: {}\n E-mail: {}\n Password: {}'.format(user.username, user.email, password)
        send_mail(u'Для вас создан пользователь на сайте kabinet1.ru', msg, 'info@kabinet1.ru', [user.email])

        return HttpResponse('OK')

def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)+1):
        yield start_date + datetime.timedelta(n)

def check_wd(wd, time):
    ex_pass = False
    if not wd.holliday:
        if wd.workperoids.count() == 0:
            ex_pass = True
        else:
            if time != 'all':
                if time == 'morning':
                    if wd.workperoids.filter(
                        start_time__lt=datetime.time(14, 00)
                    ).count() > 0:
                        ex_pass = True
                if time == 'day':
                    if wd.workperoids.filter(
                        start_time__lt=datetime.time(19, 00),
                        start_time__gte=datetime.time(14, 00)
                    ).count() > 0:
                        ex_pass = True
                if time == 'evening':
                    if wd.workperoids.filter(
                        start_time__gte=datetime.time(19, 00)
                    ).count() > 0:
                        ex_pass = True
            else:
                ex_pass = True
    return ex_pass

class SimpleWizzard(TemplateView):
    current_step = 'experts'
    prev_step = 'experts'
    http_method_names = ['get', 'post']

    def get_template_names(self):
        return SW_TEMPLATES[self.current_step]

    def check_available_step(self, request):
        step = self.kwargs.get('step')
        container = request.session.get('container', {})
        if container:
            if step == 'category':
                request.session.update({
                    'container': {}
                })
                self.current_step = 'category'
                return
            if step == 'issue':
                if container.get('category'):
                    self.current_step = 'issue'
                else:
                    self.current_step = 'category'
                return
            if step == 'experts':
                if container.get('category'):
                    self.current_step = 'experts'
                    if container.get('issue'):
                        self.prev_step = 'issue'
                else:
                    if 'issue' in container: del container['issue']
                    self.current_step = 'experts'
                return
            if step == 'time':
                self.prev_step = 'experts'
                if container.get('expert'):
                    self.current_step = 'time'
                else:
                    if container.get('category'):
                        if container.get('issue'):
                            self.current_step = 'experts'
                        else:
                            self.current_step = 'experts'
                    else:
                        self.current_step = 'experts'
                return
            if step == 'pay':
                self.prev_step = 'time'
                if container.get('expert'):
                    if container.get('datetime'):
                        self.current_step = 'pay'
                    else:
                        self.current_step = 'time'
                else:
                    if container.get('category'):
                        if container.get('issue'):
                            self.current_step = 'experts'
                        else:
                            self.current_step = 'experts'
                    else:
                        self.current_step = 'experts'
                return
        else:
            if step == 'experts':
                self.current_step = 'experts'
                return

            self.current_step = 'experts'
            return

    def get(self, request, **kw):
        self.check_available_step(request)
        return self.render_to_response(self.get_context_data())

    def post(self, request, **kw):
        session = request.session
        session.update({"container": session.get("container") or {}})
        self.check_available_step(request)
#        if session.get('current_step'):
#            self.current_step = session['current_step']

        if self.current_step == 'category':
            category = request.POST.get('category-category')
            if Issue.objects.filter(pk=category).exists():
                session['container']['category'] = get_or_none(Issue, id=category)
                return redirect(reverse('wizzard_step', kwargs={'step': 'issue'}))

        if self.current_step == 'issue':
            issue = request.POST.get('issue-issue')
            if Issue.objects.filter(pk=issue).exists():
                session['container']['issue'] = get_or_none(Issue, id=issue)
                return redirect(reverse('wizzard_step', kwargs={'step': 'experts'}))

        if self.current_step == 'experts':
            expert = request.POST.get('experts-expert')
            if User.objects.filter(pk=expert).exists():
                session['container']['expert'] = get_or_none(User, id=expert)
                return redirect(reverse('wizzard_step', kwargs={'step': 'time'}))

        if self.current_step == 'time':
            date = request.POST.get('time-datetime')
            value = request.POST.get('time-value')
            session['container']['value'] = value
            session['container']['datetime'] = datetime.datetime.strptime(date, '%d.%m.%Y-%H-%M')
            return redirect(reverse('wizzard_step', kwargs={'step': 'pay'}))

        if self.current_step == 'pay':
            if request.POST.get('username'):
                p = User.objects.make_random_password()
                form = PayRegForm(request.POST)
                if form.is_valid():
                    username=form.cleaned_data['username']
                    email = form.cleaned_data['email']

                    if Site._meta.installed:
                        site = Site.objects.get_current()
                    else:
                        site = RequestSite(request)
                    new_user = RegistrationProfile.objects.create_inactive_user(username, email,
                                                                                p, site)
                    signals.user_registered.send(sender=RegistrationView,
                                     user=new_user,
                                     request=request)

                    msg = u'Логин: {}\n Пароль: {}\n'.format(username, p)
                    send_mail(u'Кабинет №1 - данные для входа в Ваш личный кабинет', msg, 'info@kabinet1.ru', [new_user.email])

                    request.session['redirect_to_pay'] = True

                    return redirect(reverse('registration_complete') + '?email=' + new_user.email)

            if request.POST.get('pay-some'):
                if session.get('container') and \
                   session['container'].get('expert') and \
                    session['container'].get('datetime') and \
                    session['container'].get('value'):
                    return self.done()
        return self.get(request, **kw)

    def get_context_data(self, *args, **kwargs):
        ctx = super(SimpleWizzard, self).get_context_data(*args, **kwargs)
        session = self.request.session
        session.update({"container": session.get("container") or {}})

        ctx['expert_question_form'] = ExpertQuestionForm()

        if self.current_step == 'category':
            ctx['categories'] = Issue.objects.filter(parent=None)
            ctx['form'] = Step1Form()

        if self.current_step == 'issue':
            if self.request.GET.get('category', False):
                category = get_or_none(Issue, id=self.request.GET.get('category', None))
            else:
                category = session["container"].get("category")
            session["container"].update({"category": category})
            groups = {}
            if category and category.pk == 1:
                for iss in category.children.all():
                    if ' - ' in iss.name:
                        if iss.name.split(' - ')[0] not in groups:
                            groups[iss.name.split(' - ')[0]] = []
                        groups[iss.name.split(' - ')[0]].append(iss)
            if groups:
                for k,v in groups.items():
                    groups[k] = sorted(v, key=lambda x: x.name)
            ctx.update(
                {
                    "groups": groups,
                    "issues":   category.children.all(),
                    "category": session["container"]["category"]
                }
            )
            ctx['form'] = Step2Form()

        if self.current_step == 'experts':
            issue = session["container"].get("issue")
            min_date = None
            max_date = None
            is_holiday = False
            is_workday = False
            if self.request.GET.get("range_start"):
                ctx.update({
                    'range_start': self.request.GET.get("range_start")
                })
                min_date = datetime.datetime.strptime(self.request.GET.get("range_start"), '%d.%m.%Y')
            if self.request.GET.get("range_end"):
                ctx.update({
                    'range_end': self.request.GET.get("range_end")
                })
                max_date = datetime.datetime.strptime(self.request.GET.get("range_end"), '%d.%m.%Y')
            if self.request.GET.get("is_holiday"):
                ctx["is_holiday"] = self.request.GET.get("is_holiday")
                is_holiday = True
            if self.request.GET.get("is_workday"):
                ctx["is_workday"] = self.request.GET.get("is_workday")
                is_workday = True
            time = self.request.GET.get("time", 'all')
            ctx['time'] = time

            ctx['issues'] = Issue.objects.filter(parent=None)
            ctx['group_values'] = []

            category = session['container'].get('category')
            session["container"].update({"issue": issue,"category": category})
            if session["container"]["issue"] and session["container"]["issue"] != -1:
                experts = User.objects.filter(
                    expertblank__isnull=False,
                    id__in = session["container"]["issue"].expertblank_set.values_list('expert__id', flat=True),
                    groups__name='experts')
            else:
                experts = User.objects.filter(
                    expertblank__isnull=False,
                    groups__name='experts')

            experts = experts.exclude(expertblank__is_tested=True)
            if self.request.GET.get('group'):
                group_list = self.request.GET.getlist('group')
                ex_pk = []
                ctx['group_values'] = [int(p) for p in group_list]
                for g in group_list:
                    iss = Issue.objects.get(pk=g)
                    ex_pk.extend(iss.expertblank_set.values_list('expert__id', flat=True))
                ex_pk = list(set(ex_pk))
                experts = experts.filter(id__in=ex_pk)

            if self.request.GET.get('issue'):
                issue_values = self.request.GET.getlist('issue')
                e_ids = []
                for iv in issue_values:
                    iss = Issue.objects.get(pk=iv)
                    e_ids.extend(iss.expertblank_set.values_list('expert__id', flat=True))
                experts = experts.filter(id__in=e_ids)
                ctx['iss_value'] = [int(p) for p in issue_values]

            if self.request.GET.get('price_filter'):
                pf = self.request.GET.get('price_filter')
                ctx['price_filter'] = pf
                if pf[0] == '-' or pf[0] == '+':
                    limit = int(pf[1:])
                    if pf[0] == '-':
                        experts = experts.filter(expertblank__fix__lt=limit)
                    if pf[0] == '+':
                        experts = experts.filter(expertblank__fix__gt=limit)
                else:
                    min_limit,max_limit = pf.split('-')
                    experts = experts.filter(expertblank__fix__gte=min_limit, expertblank__fix__lte=max_limit)

            if self.request.GET.get('sex'):
                pf = self.request.GET.getlist('sex')
                if len(pf) == 1:
                    ctx['sex'] = pf
                    sex = True if pf[0] == '1' else False
                    experts = experts.filter(expertblank__sex=sex)

            if self.request.GET.get('free'):
                pf = self.request.GET.get('free')
                ctx['free'] = pf
                free = True if pf == '1' else False
                experts = experts.filter(expertblank__free_consult=free)

            for ex in experts:
                ex_pass = False
                if min_date and max_date:
                    for d in daterange(min_date, max_date):
                        if time != 'all':
                            if time == 'morning':
                                if WorkDay.is_worked_in_morning(ex, d, is_holiday, is_workday):
                                    ex_pass = True
                            if time == 'day':
                                if WorkDay.is_worked_in_day(ex, d, is_holiday, is_workday):
                                    ex_pass = True
                            if time == 'evening':
                                if WorkDay.is_worked_in_evening(ex, d, is_holiday, is_workday):
                                    ex_pass = True
                        else:
                            if WorkDay.is_worked(ex, d):
                                if is_holiday ^ is_workday:
                                    if is_holiday:
                                        if d.weekday() in [5,6]:
                                            ex_pass = True
                                    if is_workday:
                                        if d.weekday() in xrange(0, 5):
                                            ex_pass = True
                                else:
                                    ex_pass = True
                        if ex_pass:
                            continue
                else:
                    # if no date range
                    if is_holiday ^ is_workday:
                        if is_workday:
                            if ex.workday_set.filter(dayofweek__in=xrange(0, 5)).distinct().count() == 5:
                                for wd in ex.workday_set.filter(dayofweek__in=xrange(0, 5)).distinct():
                                    if check_wd(wd, time):
                                        ex_pass = True
                            else:
                                ex_pass = True
                        if is_holiday:
                            if ex.workday_set.filter(dayofweek__in=[5,6]).distinct().count() == 2:
                                for wd in ex.workday_set.filter(dayofweek__in=[5,6]).distinct():
                                    if check_wd(wd, time):
                                        ex_pass = True
                            else:
                                ex_pass = True
                    else:
                        if ex.workday_set.filter(dayofweek__isnull=False).distinct().count() == 7:
                        # check this
                            for wd in ex.workday_set.filter(dayofweek__isnull=False).distinct():
                                if check_wd(wd, time):
                                    ex_pass = True
                        else:
                            ex_pass = True

                if not ex_pass:
                    experts = experts.exclude(id=ex.pk)
            ctx.update(
                {
                    "experts":  experts,
                    "issue":    session["container"]["issue"],
                    "category": session["container"]["category"],
                    })
            ctx['form'] = Step3Form()

        if self.current_step == 'time':
            ndate = datetime.datetime.now()
            expert = session['container'].get('expert')
            self.year = int(self.request.GET.get('year') if self.request.GET.get('year', False) else datetime.datetime.now().year)
            self.month = int(self.request.GET.get('month') if self.request.GET.get('month', False) else datetime.datetime.now().month)
            rng = calendar.monthrange (self.year, self.month)[1]
            st_dow = datetime.date(self.year, self.month, 1).weekday()
            cal = []
            if st_dow != 0:
                for i in xrange(st_dow):
                    cal.append((u'', False, datetime.date(self.year, self.month, 1).strftime('%W')))
            for d in xrange(1, rng+1):
                nday = datetime.date(self.year, self.month, d)
                cal.append((d,
                            WorkDay.is_worked(user=expert, date=nday),
                            nday.strftime('%W'),
                            WorkDay.get_periods(expert, date=nday)))
            ctx['this_month'] = ndate.month == self.month and ndate.year == self.year

            ctx['calendar'] = cal
            ctx['now_day'] = ndate.day
            ctx['now_year'] = self.year
            ctx['now_month'] = self.month
            ctx['now_month_human'] = MONTHS[self.month-1]

            ctx['prev_year'] = self.year
            ctx['prev_month'] = self.month-1
            ctx['prev_month_human'] = MONTHS[self.month-2]
            if self.month == ndate.month and self.year == ndate.year:
                ctx['prev_month'] = self.month
                ctx['prev_month_human'] = MONTHS[self.month-1]
            if ctx['prev_month'] == 0:
                ctx['prev_year'] = self.year-1
                ctx['prev_month'] = 12
                ctx['prev_month_human'] = MONTHS[11]

            ctx['next_year'] = self.year
            ctx['next_month'] = self.month+1
            if ctx['next_month'] == 13:
                ctx['next_year'] = self.year+1
                ctx['next_month'] = 1
                ctx['next_month_human'] = MONTHS[0]
            else:
                ctx['next_month_human'] = MONTHS[self.month]
            ctx.update(
                {
                    "expert":   session["container"]["expert"],
                    "issue":    session["container"].get("issue"),
                    "category": session["container"].get("category"),
                    "value": expert.expertblank.consultation_time
                }
            )
            ctx['form'] = Step4Form()

        if self.current_step == 'pay':
#            session["container"].update({
#                "datetime": ctx.get("datetime"),
#                "value": ctx.get("value"),
#                })
            if self.request.user.is_authenticated():
                if Balance.objects.filter(user=self.request.user).exists():
                    balance = Balance.objects.get(user=self.request.user)
                else:
                    balance = Balance.objects.create(
                        user=self.request.user,
                        sum=0.0
                    )
#                if self.request.GET.get("updatemoney") == "1":
#                    balance.sum += decimal.Decimal(3000.0)
#                    balance.save()
#                sum_for_consl = float(int(session["container"]["value"])*session["container"]["expert"].expertblank.fix)/60.0
                sum_for_consl = session["container"]["expert"].expertblank.fix
                if balance.sum < sum_for_consl:
                    needmoremoney = sum_for_consl - float(balance.sum)
                    o = Operation.objects.create(
                        sum = needmoremoney,
                        user = self.request.user
                    )
                    self.request.session['redirect_to_pay'] = True
                    pForm = PaymentForm(data={
                        'spShopId': settings.SPRYPAY.get('shopID'),
                        'spCurrency': 'rur',
                        'spPurpose': u'Пополнение баланса',
                        'spIpnUrl': 'http://kabinet1.ru/sprypay/back/',
                        'spIpnMethod': 1,
                        'spFailMethod': 1,
                        'lang': 'ru',
                        'spShopPaymentId': o.pk,
                        # 'spUserDataUserId': self.request.user.pk,
                        'spSuccessUrl': u'http://kabinet1.ru{}'.format(reverse(viewname='wizzard')),
                        'spFailUrl': u'http://kabinet1.ru{}'.format(reverse(viewname='wizzard')),
                        'spAmount': o.sum
                    })
                    ctx.update({
                        'needmoremoney': needmoremoney,
                        'balance': balance,
                        "balance_payment_form": pForm
                    })
            else:
                form = AuthenticationForm()
                if self.request.POST.get('username'):
                    reg_form = PayRegForm(self.request.POST)
                else:
                    reg_form = PayRegForm()
                ctx.update({
                    'form': form,
                    'reg_form': reg_form
                })
            ctx.update(
                {
                    "expert":   session["container"]["expert"],
                    "issue":    session["container"].get("issue", ctx.get("issue")),
                    "category": session["container"].get("category", ctx.get("category")),
                    "datetime": session["container"]["datetime"],
                    "value": session["container"]["value"]
                }
            )
        ctx['current_step'] = self.current_step
        ctx['prev_step'] = self.prev_step
        session['current_step'] = self.current_step
        ctx['args'] = self.kwargs
        return ctx

    def done(self, **kwargs):
        container = self.request.session.get("container", None)
        if container:
            c = Consultation.objects.create(
                client=self.request.user,
                expert = container["expert"],
                issue = container.get("issue", None),
                dtstart = container["datetime"],
                length = container["value"],
                status=u'assigned'
            )
            msg = u"""
Новая консультация.
Клиент: {}\n""".format(
                u'{} {}'.format(c.client.first_name, c.client.last_name) if c.client.first_name and c.client.last_name else c.client.username,
            )
            if c.issue:
                msg += u'Проблема: {}\n'.format(c.issue.name)
            user_msg = msg
            user_msg +=\
            u"""{}
            Продолжительность: {} минут""".format(
                pytils.dt.ru_strftime(u"%d %B %Y в %H:%M", date=timezone.make_aware(c.dtstart, timezone.get_current_timezone())),
                c.length
            )
            msg +=\
            u"""Дата и время: {}
            Продолжительность: {} минут

            Вам необходимо подтвердить возможность ее проведения в течение 1 часа. В противном случае консультация будет автоматичеки отменена.
            Вы можете подтвердить консультацию в вашем личном кабинете либо с помощью СМС.

            Служба поддержки Кабинета №1
            support@kabinet1.ru
            www.kabinet1.ru
            """.format(
                pytils.dt.ru_strftime(u"%d %B %Y в %H:%M", date=timezone.make_aware(c.dtstart, timezone.get_current_timezone())),
                c.length
            )
            UserMessage.objects.create(
                to_user=c.expert,
                msg = user_msg.replace("\n", "<br/>")
            )
            sms_message = u'Посетитель сайта www.kabinet1.ru {} пополнил свой баланс и записался к Вам на консультацию на {}.' \
                          u' Первые 5 минут консультации - пробные, и клиентом не оплачиваются' \
                          u'. Для подтверждения отправьте в ответ слово "ДА{}". Для отказа отправьте "НЕТ{}".'.format(
                c.client.username,
                pytils.dt.ru_strftime(u"%d %B %Y в %H:%M", date=timezone.make_aware(c.dtstart, timezone.get_current_timezone()), inflected=True),
                c.pk,
                c.pk
            )
            # sms_message = u'У вас новый клиент - {}. '\
            #               u'Дата и время: {}. '\
            #               u'Для подтверждения отправьте в ответ слово "ДА{}". '\
            #               u'Для отказа отправьте "НЕТ{}".'.format(c.client.username,
            #     pytils.dt.ru_strftime(u"%d %B %Y в %H:%M", date=timezone.make_aware(c.dtstart, timezone.get_current_timezone()), inflected=True),
            #     c.pk, c.pk)
            rr = send_message([c.expert.profile.phone_number.replace(' ', '')], sms_message)
            if rr:
                SMSConsultationNotification.objects.create(
                    consultation=c,
                    send_id=rr
                )
            send_mail(u'Кабинет №1 - Новая заявка на консультацию', msg, u'Кабинет №1 <support@kabinet1.ru>', [c.expert.email])
            #            print c.expert.profile.phone_number
            cache_key = "messages-for-{}".format(c.expert.pk)
            user_msg = u'<a href="{}">Новая заявка</a>\n'.format(reverse(viewname="schedule-view")) + user_msg
            if cache.get(cache_key):
                cache_data = json.loads(cache.get(cache_key))
                cache_data.append(
                    {
                        int(100*time.time()): user_msg.replace(u'\n', u'<br/>')
                    }
                )
                cache_data = json.dumps(cache_data)
            else:
                cache_data = []
                cache_data.append(
                    {
                        int(100*time.time()): user_msg.replace(u'\n', u'<br/>')
                    }
                )
                cache_data = json.dumps(cache_data)
            #                cache_data = user_msg.replace("\n", "<br/>")
            cache.set(cache_key, cache_data, 3600)
            del self.request.session["container"]
        return render_to_response('done.html', {}, context_instance=RequestContext(self.request))


class Wizard(NamedUrlSessionWizardView):

    def get_template_names(self):
        return [TEMPLATES[self.steps.current]]

    def get(self, request, **kw):
        if request.path == "/wizzard/category/" and len(self.form_list) == 3:
            self.form_list.insert(0, u'issue', Step2Form)
            self.form_list.insert(0, u'category', Step1Form)
            if u'experts' in self.storage.data[u'step_data']:
                del self.storage.data[u'step_data'][u'experts']
#                self.storage.data[u'step_data'][u'experts'].update({
#                    u'experts-expert': []
#                })
#        if request.path == "/wizzard/experts/" and len(self.form_list) == 5:
#            self.form_list.pop(u'category')
#            self.form_list.pop(u'issue')
#        from pprint import pprint
#        print type(self.form_list)
#        pprint(self.storage.data[u'step_data'])
#        pprint(self.form_list)
        return super(Wizard, self).get(request, **kw)

    def post(self, *args, **kwargs):
        if self.request.path == "/wizzard/experts/" and\
           self.request.POST.get(u'category-category') and \
            self.request.POST.get(u'issue-issue'):
            c = self.request.session.get("container")
            if not c:
                self.request.session["container"] = {}
                c = self.request.session.get("container")
            c.update({
                'category': get_or_none(Issue, id=self.request.POST.get(u'category-category')),
                'issue': get_or_none(Issue, id=self.request.POST.get(u'issue-issue')),
            })

            if u'category' not in self.form_list:
                self.form_list.insert(0, u'issue', Step2Form)
                self.form_list.insert(0, u'category', Step1Form)

            # print self.form_list
            self.storage.set_step_data(u'category', self.process_step(self.get_form(step=u'category', data=self.request.POST, files=self.request.FILES)))
            self.storage.set_step_data(u'issue', self.process_step(self.get_form(step=u'issue', data=self.request.POST, files=self.request.FILES)))
            self.storage._set_current_step(u'experts')
        if self.request.path == "/wizzard/pay/":
#            print self.get_all_cleaned_data()
            self.storage._set_current_step(u'done')
        return super(Wizard, self).post(*args, **kwargs)

    def get_context_data(self, form, **kwargs):

        context = super(Wizard, self).get_context_data(form=form, **kwargs)
        context.update(self.get_all_cleaned_data())
        session = self.request.session
        session.update({"container": session.get("container") or {}})

        #first step of wizzard
        if self.steps.current == "category":
            context.update({"categories": Issue.objects.filter(parent=None)})

        #second step of wizzard
        elif self.steps.current == "issue":
            if self.request.GET.get('category', False):
                category = get_or_none(Issue, id=self.request.GET.get('category', None))
            else:
                category = get_or_none(Issue, id=context.get("category")) or session["container"].get("category")
            session["container"].update({"category": category})
            groups = {}
            if category and category.pk == 1:
                for iss in category.children.all():
                    if ' - ' in iss.name:
                        if iss.name.split(' - ')[0] not in groups:
                            groups[iss.name.split(' - ')[0]] = []
                        groups[iss.name.split(' - ')[0]].append(iss)
            if groups:
                for k,v in groups.items():
                    groups[k] = sorted(v, key=lambda x: x.name)
            context.update(
                {
                    "groups": groups,
                    "issues":   category.children.all(),
                    "category": session["container"]["category"]
                }
                )
        #third step of wizzard
        elif self.steps.current == "experts":
            issue = get_or_none(Issue, id=context.get("issue")) or session["container"].get("issue")
            min_date = None
            max_date = None
            is_holiday = False
            is_workday = False
            if self.request.GET.get("range_start"):
                context.update({
                    'range_start': self.request.GET.get("range_start")
                })
                min_date = datetime.datetime.strptime(self.request.GET.get("range_start"), '%d.%m.%Y')
            if self.request.GET.get("range_end"):
                context.update({
                    'range_end': self.request.GET.get("range_end")
                })
                max_date = datetime.datetime.strptime(self.request.GET.get("range_end"), '%d.%m.%Y')
            if self.request.GET.get("is_holiday"):
                context["is_holiday"] = self.request.GET.get("is_holiday")
                is_holiday = True
            if self.request.GET.get("is_workday"):
                context["is_workday"] = self.request.GET.get("is_workday")
                is_workday = True
            time = self.request.GET.get("time", 'all')
            context['time'] = time

            category = get_or_none(Issue, id=context.get("category"))
            session["container"].update({"issue": issue,"category": category})
            if session["container"]["issue"] and session["container"]["issue"] != -1:
                experts = User.objects.filter(expertblank__isnull=False, id__in = session["container"]["issue"].expertblank_set.values_list('expert__id', flat=True), groups__name='experts')
            else:
                experts = User.objects.filter(expertblank__isnull=False, groups__name='experts')

            experts = experts.exclude(expertblank__is_tested=True)

            for ex in experts:
                ex_pass = False
                if min_date and max_date:
                    for d in daterange(min_date, max_date):
                        if time != 'all':
                            if time == 'morning':
                                if WorkDay.is_worked_in_morning(ex, d, is_holiday, is_workday):
                                    ex_pass = True
                            if time == 'day':
                                if WorkDay.is_worked_in_day(ex, d, is_holiday, is_workday):
                                    ex_pass = True
                            if time == 'evening':
                                if WorkDay.is_worked_in_evening(ex, d, is_holiday, is_workday):
                                    ex_pass = True
                        else:
                            if WorkDay.is_worked(ex, d):
                                if is_holiday ^ is_workday:
                                    if is_holiday:
                                        if d.weekday() in [5,6]:
                                            ex_pass = True
                                    if is_workday:
                                        if d.weekday() in xrange(0, 5):
                                            ex_pass = True
                                else:
                                    ex_pass = True
                        if ex_pass:
                            continue
                else:
                    # if no date range
                    if is_holiday ^ is_workday:
                        if is_workday:
                            if ex.workday_set.filter(dayofweek__in=xrange(0, 5)).distinct().count() == 5:
                                for wd in ex.workday_set.filter(dayofweek__in=xrange(0, 5)).distinct():
                                    if check_wd(wd, time):
                                        ex_pass = True
                            else:
                                ex_pass = True
                        if is_holiday:
                            if ex.workday_set.filter(dayofweek__in=[5,6]).distinct().count() == 2:
                                for wd in ex.workday_set.filter(dayofweek__in=[5,6]).distinct():
                                    if check_wd(wd, time):
                                        ex_pass = True
                            else:
                                ex_pass = True
                    else:
                        if ex.workday_set.filter(dayofweek__isnull=False).distinct().count() == 7:
                        # check this
                            for wd in ex.workday_set.filter(dayofweek__isnull=False).distinct():
                                if check_wd(wd, time):
                                    ex_pass = True
                        else:
                            ex_pass = True

                if not ex_pass:
                    experts = experts.exclude(id=ex.pk)
            context.update(
                {
                    "experts":  experts,#User.objects.filter(groups__name='experts'),
                    "issue":    session["container"]["issue"],
                    "category": session["container"]["category"],
                })

        #fourth step of wizzard
        elif self.steps.current == "time":
#            print context.get("datetime")
            expert = get_or_none(User, id=context.get("expert"))
            self.year = int(self.request.GET.get('year') if self.request.GET.get('year', False) else datetime.datetime.now().year)
            self.month = int(self.request.GET.get('month') if self.request.GET.get('month', False) else datetime.datetime.now().month)
            rng = calendar.monthrange (self.year, self.month)[1]#.date(self.year, self.month+1, 1) - datetime.date(self.year, self.month, 1)).days
            st_dow = datetime.date(self.year, self.month, 1).weekday()
            cal = []
            if st_dow != 1:
                for i in xrange(st_dow):
                    cal.append((u'', False, datetime.date(self.year, self.month, 1).strftime('%W')))
            for d in xrange(1, rng+1):
                nday = datetime.date(self.year, self.month, d)
                cal.append((d,
                            WorkDay.is_worked(user=expert, date=nday),
                            nday.strftime('%W'),
                            WorkDay.get_periods(expert, date=nday)))
            context['calendar'] = cal
            context['now_day'] = datetime.datetime.now().day
            context['now_year'] = self.year
            context['now_month'] = self.month
            context['now_month_human'] = MONTHS[self.month-1]

            context['prev_year'] = self.year
            context['prev_month'] = self.month-1
            context['prev_month_human'] = MONTHS[self.month-2]
            if context['prev_month'] == 0:
                context['prev_year'] = self.year-1
                context['prev_month'] = 12
                context['prev_month_human'] = MONTHS[11]

            context['next_year'] = self.year
            context['next_month'] = self.month+1
            if context['next_month'] == 13:
                context['next_year'] = self.year+1
                context['next_month'] = 1
                context['next_month_human'] = MONTHS[0]
            else:
                context['next_month_human'] = MONTHS[self.month]
            session["container"].update({"expert": get_or_none(User, id=context.get("expert")),
                })
            context.update(
                {
                    "expert":   session["container"]["expert"],
                    "issue":    session["container"].get("issue"),
                    "category": session["container"].get("category"),
                    "value": get_or_none(User, id=context.get("expert")).expertblank.consultation_time
                }
                )

        #fifth step of wizzard
        elif self.steps.current == "pay":
            session["container"].update({
                "datetime": context.get("datetime"),
                "value": context.get("value"),
                })
            if self.request.user.is_authenticated():
                balance = Balance.objects.get(user=self.request.user)
                if self.request.GET.get("updatemoney") == "1":
                    balance.sum += decimal.Decimal(3000.0)
                    balance.save()
                sum_for_consl = float(int(session["container"]["value"])*session["container"]["expert"].expertblank.fix)/60.0
                if balance.sum < sum_for_consl:
                    needmoremoney = sum_for_consl - float(balance.sum)
                    o = Operation.objects.create(
                        sum = needmoremoney,
                        user = self.request.user
                    )
                    pForm = PaymentForm(data={
                        'spShopId': settings.SPRYPAY.get('shopID'),
                        'spCurrency': 'rur',
                        'spPurpose': u'Пополнение баланса',
                        'spIpnUrl': 'http://kabinet1.ru/sprypay/back/',
                        'spIpnMethod': 1,
                        'spFailMethod': 1,
                        'lang': 'ru',
                        'spShopPaymentId': o.pk,
                        # 'spUserDataUserId': self.request.user.pk,
                        'spSuccessUrl': u'http://kabinet1.ru{}'.format(reverse(viewname='wizzard')),
                        'spFailUrl': u'http://kabinet1.ru{}'.format(reverse(viewname='wizzard')),
                        'spAmount': o.sum
                    })
#                    iframe_generator = IframeGenerator()
#                    iframe_generator.width = 500
#                    iframe_generator.url_success = "http://dev2765.kabinet1.com/wizzard/"
#                    op = Operation.objects.create(
#                        user=self.request.user,
#                        sum=needmoremoney
#                    )
#                    onpay_tag = iframe_generator.iframe_tag(op.pk, needmoremoney)
                    context.update({
                        'needmoremoney': needmoremoney,
                        'balance': balance
                    })
                    context.update({
#                        "iframe": onpay_tag,
                        "balance_payment_form": pForm
                    })
            else:
                form = AuthenticationForm()
                context.update({
                    'form': form
                })
            context.update(
                {
                    "expert":   session["container"]["expert"],
                    "issue":    session["container"].get("issue", context.get("issue")),
                    "category": session["container"].get("category", context.get("category")),
                    "datetime": session["container"]["datetime"],
                    "value": session["container"]["value"]
                }
                )
#        context.update({
#            'container': session["container"],
#            'form_list': self.form_list,
#        })
        return context

    def get_form(self, step=None, data=None, files=None):
        if data:
           if data.get(u"experts-expert") and \
           not data.get(u"category-category"):
            if self.form_list.get(u'category'):
                self.form_list.pop(u'category')
            if self.form_list.get(u'issue'):
                self.form_list.pop(u'issue')

        logging.basicConfig(filename='/tmp/debug.log', level=logging.DEBUG)
        logging.debug(u'DATA: {}'.format(data))
        logging.debug(u'FORM_LIST: {}'.format(self.form_list))

        form = super(Wizard, self).get_form(step, data, files)
        return form


    def process_step(self, form):
        if self.steps.current == "time" and self.request.session.get("container", False):
            c = self.request.session.get("container")
            if c.get("category") == None and self.form_list.get(u'category'):
                self.form_list.pop(u'category')
                self.form_list.pop(u'issue')
        rt = self.get_form_step_data(form)
        return rt

    def done(self, form_list, **kwargs):
        container = self.request.session.get("container", None)
        if container:
            c = Consultation.objects.create(
                client=self.request.user,
                expert = container["expert"],
                issue = container.get("issue", None),
                dtstart = container["datetime"],
                length = container["value"],
                status=u'assigned'
            )
            msg = u"""
Новая консультация.
Клиент: {}\n""".format(
                u'{} {}'.format(c.client.first_name, c.client.last_name) if c.client.first_name and c.client.last_name else c.client.username,
            )
            if c.issue:
                msg += u'Проблема: {}\n'.format(c.issue.name)
            user_msg = msg
            user_msg +=\
            u"""{}
            Продолжительность: {} минут""".format(
                pytils.dt.ru_strftime(u"%d %B %Y в %H:%M", date=timezone.make_aware(c.dtstart, timezone.get_current_timezone())),
                c.length
            )
            msg += \
u"""Дата и время: {}
Продолжительность: {} минут

Вам необходимо подтвердить возможность ее проведения в течение 1 часа. В противном случае консультация будет автоматичеки отменена.
Вы можете подтвердить консультацию в вашем личном кабинете либо с помощью СМС.

Служба поддержки Кабинета №1
support@kabinet1.ru
www.kabinet1.ru
""".format(
    pytils.dt.ru_strftime(u"%d %B %Y в %H:%M", date=timezone.make_aware(c.dtstart, timezone.get_current_timezone())),
    c.length
)
            UserMessage.objects.create(
                to_user=c.expert,
                msg = user_msg.replace("\n", "<br/>")
            )
            # sms_message = u'У вас новый клиент - {}. ' \
            #               u'Дата и время: {}. ' \
            #               u'Для подтверждения отправьте в ответ слово "ДА{}". ' \
            #               u'Для отказа отправьте "НЕТ{}".'.format(c.client.username,
            #     pytils.dt.ru_strftime(u"%d %B %Y в %H:%M", date=timezone.make_aware(c.dtstart, timezone.get_current_timezone()), inflected=True),
            # c.pk, c.pk)
            sms_message = u'Посетитель сайта www.kabinet1.ru {} пополнил свой баланс и записался к Вам на консультацию на {}.' \
                          u' Первые 5 минут консультации - пробные, и клиентом не оплачиваются' \
                          u'. Для подтверждения отправьте в ответ слово "ДА{}". Для отказа отправьте "НЕТ{}".'.format(
                c.client.username,
                pytils.dt.ru_strftime(u"%d %B %Y в %H:%M", date=timezone.make_aware(c.dtstart, timezone.get_current_timezone()), inflected=True),
                c.pk,
                c.pk
            )
            rr = send_message([c.expert.profile.phone_number.replace(' ', '')], sms_message)
            if rr:
                SMSConsultationNotification.objects.create(
                    consultation=c,
                    send_id=rr
                )
            send_mail(u'Кабинет №1 - Новая заявка на консультацию', msg, u'Кабинет №1 <support@kabinet1.ru>', [c.expert.email])
#            print c.expert.profile.phone_number
            cache_key = "messages-for-{}".format(c.expert.pk)
            user_msg = u'<a href="{}">Новая заявка</a>\n'.format(reverse(viewname="schedule-view")) + user_msg
            if cache.get(cache_key):
                cache_data = json.loads(cache.get(cache_key))
                cache_data.append(
                    {
                        int(100*time.time()): user_msg.replace(u'\n', u'<br/>')
                    }
                )
                cache_data = json.dumps(cache_data)
#                cache_data = unicode(cache.get(cache_key)) + u"][" + user_msg.replace(u"\n", u"<br/>")
            else:
                cache_data = []
                cache_data.append(
                    {
                        int(100*time.time()): user_msg.replace(u'\n', u'<br/>')
                    }
                )
                cache_data = json.dumps(cache_data)
#                cache_data = user_msg.replace("\n", "<br/>")
            cache.set(cache_key, cache_data, 3600)
            self.form_list.insert(0, u'issue', Step2Form)
            self.form_list.insert(0, u'category', Step1Form)
            del self.request.session["container"]
        return render_to_response('done.html', {}, context_instance=RequestContext(self.request))

class GetPeroidsAjaxView(View):
    def get(self, request, *args, **kwargs):
        expert = request.GET.get('expert')
        day = time.strptime(request.GET.get('day'), '%d.%m.%Y')
        day = datetime.date(day.tm_year, day.tm_mon, day.tm_mday)
        if expert and day:
            expert = User.objects.get(pk=expert)
            if WorkDay.is_worked(expert, day):
                if WorkDay.get_periods(expert, day):
                    periods = u''
                    for wp in WorkDay.get_periods(expert, day):
                        periods += u'<div>{}:{} - {}:{}</div>'.format(str(wp.start_time.hour).zfill(2),
                        str(wp.start_time.minute).zfill(2), str(wp.end_time.hour).zfill(2), str(wp.end_time.minute).zfill(2))
                    return HttpResponse(periods)
                else:
                    return HttpResponse(u'Весь день')
            else:
                return HttpResponse(u'Выходной')
        return HttpResponse('Error')

class GetPeriodsForChooseAjaxView(View):
    def get(self, request, *a, **kw):
        expert_pk = request.GET.get('expert')
        day = time.strptime(request.GET.get('day'), '%d.%m.%Y')
        day = datetime.date(day.tm_year, day.tm_mon, day.tm_mday)
        if expert_pk and day:
            expert = User.objects.get(pk=expert_pk)
            if WorkDay.is_worked(expert, day):
                if WorkDay.get_periods(expert, day):
                    periods = []
                    wpd = WorkDay.get_periods(expert, day)
                    for hour in xrange(24):
                        if wpd.filter(start_time__lte=datetime.time(hour=hour, minute=0, second=0),
                            end_time__gt=datetime.time(hour=hour, minute=0, second=0)).count() > 0:
                            periods.append((hour, True))
                        else:
                            periods.append((hour, False))
                else:
                    periods = [(h, True) for h in xrange(24)]
                return render_to_response('wizzard_choose_time.html', {'hour_periods': periods,
                                                                       'date': pytils.dt.ru_strftime(u'%d %B', day, inflected=True),
                                                                       'wday': pytils.dt.ru_strftime(u'%A', day)})
            else:
                return HttpResponse(u'Выходной')
        return HttpResponse('Error')

wizzard_view = Wizard.as_view(
    FORMS, url_name='wizzard_step', done_step_name='finished')

wizzard_get_periods = GetPeroidsAjaxView.as_view()
#wizzard_view = Wizard.as_view(FORMS)
wizzard_get_periods_for_choose = GetPeriodsForChooseAjaxView.as_view()
simple_wizzard_view = SimpleWizzard.as_view()