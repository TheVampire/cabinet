#coding: UTF-8
#@todo: NEED TOTAL REFACTORING!

from datetime import datetime
from datetime import timedelta

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard
from admin_tools.utils import get_admin_site_name

import pybb

from feedback.models import FeedBack
from expert.models import ExpertBlank, Consultation

from custom_blocks import StaticticsTextModule, StaticticsGraphModule
from utils import aggregate_stats_text, aggregate_stats_graph


from news.models import News


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for cabinet.
    """

    columns = 2

    def __init__(self, **kwargs):
        Dashboard.__init__(self, **kwargs)

# БЛОК Баннеров
        self.children.append(
            modules.ModelList(title = u'Баннеры',
                models = (
                    'core.models.Banner',
                    ))
        )
#БЛОК "ТЕМАТИКИ ДЛЯ КОНСУЛЬТАЦИЙ"
        self.children.append(
            modules.ModelList(
                title=u'Доступные тематики для консультации',
                models=('expert.issues.*',)
            )
        )
#-----------------------------------------------------------------------------

#БЛОК "НАШИ ПОЛЬЗОВАТЕЛИ"
        self.children.append(modules.Group(
            title=u"Наши пользователи",
            display="tabs",
            children=[
                        modules.Group(
                            title=u'Все пользователи',
                            display="inline",
                            children=[
                                modules.ModelList(
                                    title=u'Управление клиентами',
                                    models = ('django.contrib.auth.models.User',)
                                    ),
                                modules.ModelList(
                                    title=u'Профили юзеров',
                                    models = ('core.models.Profile',)
                                    ),

                                StaticticsTextModule(
                                title=u'Статистика клиентов',
                                stats=aggregate_stats_text(
                                    User, "date_joined"
                                    )
                                    ),

                                    ]
                                    ),


                        modules.Group(
                            title=u'Эксперты',
                            display="inline",
                            children=[
                               modules.Group(
                                    title=u'Специализации экспертов',
                                    display="inline",
                                    children=[
                                        modules.ModelList(
                                            title=u'Управление специализациями',
                                            models = ('expert.models.Specialization',)
                                            ),
                                            ]
                                            ),

                            modules.Group(
                            title=u'Бланки экспертов',
                            display="inline",
                            children=[
                                modules.ModelList(
                                    title=u'Управление блинками',
                                    models = ('expert.models.ExpertBlank',)
                                    ),

                                StaticticsTextModule(
                                title=u'Статистика бланков',
                                stats=aggregate_stats_text(
                                    ExpertBlank, "datetime")),
                                    ]),

                                modules.Group(
                                    title=u'Отзывы об экспертах',
                                    display="inline",
                                    children=[
                                        modules.ModelList(
                                            title=u'Управление отзывами',
                                            models = ("expert.models.Recal",)),]),

                                modules.Group(
                                    title=u'Вопросы и ответы',
                                    display="inline",
                                    children=[
                                        modules.ModelList(
                                        title=u'Управление вопросами и ответами',
                                        models = ("target.models.TargetQA",)),]),                                                                                        
                                        ]
                                        ),

                        modules.Group(
                            title=u'Группы пользователей',
                            display="inline",
                            children=[
                                modules.ModelList(
                                title=u'Управление группами',
                                models = ('django.contrib.auth.models.Group',)),]),


                        modules.Group(
                            title=u'Фидбэки',
                            display="inline",
                            children=[
                                modules.ModelList(
                                    title=u'Управление фидбэками',
                                    models = ('feedback.models.FeedBack',)
                                    ),

                                StaticticsTextModule(
                                title=u'Статистика постов',
                                stats=aggregate_stats_text(
                                    FeedBack, "datetime"
                                    )
                                ),
                                    ]
                                    ),

                        modules.Group(
                            title=u'Консультации',
                            display="inline",
                            children=[
                                modules.ModelList(
                                    title=u'Управление консультациями',
                                    models = ("expert.models.Consultation",)
                                    ),
                                    ]
                                    ),

                        ]
                    ),
        )

#-----------------------------------------------------------------------------

#БЛОК "НОВОСТИ"
        self.children.append(modules.Group(
            title=u"Новости",
            display="tabs",
            children=[
                        modules.Group(
                            title=u'Новости',
                            display="inline",
                            children=[
                                modules.ModelList(
                                    title=u'Управление новостями',
                                    models = ('news.models.News',)
                                    ),

                                StaticticsTextModule(
                                title=u'Статистика новостей',
                                stats=aggregate_stats_text(
                                    News, "datetime"
                                    )
                                    ),
                                    ]
                                    ),
                        ]
                    ),
        )
#-----------------------------------------------------------------------------

#БЛОК "ФОРУМ"
        self.children.append(modules.Group(
            title=u"Форум",
            display="tabs",
            children=[
                        modules.Group(
                            title=u'Ветки',
                            display="inline",
                            children=[
                                modules.ModelList(
                                    title=u'Управление ветками',
                                    models = ('pybb.models.Forum',)
                                    ),
                                    ]
                                    ),

                        modules.Group(
                            title=u'Топики',
                            display="inline",
                            children=[
                                modules.ModelList(
                                    title=u'Управление топиками',
                                    models = ('pybb.models.Topic',)
                                    ),

                                StaticticsTextModule(
                                title=u'Статистика топиков',
                                stats=aggregate_stats_text(
                                    pybb.models.Topic, "created"
                                    )
                                ),
                                    ]
                                    ),

                        modules.Group(
                            title=u'Посты',
                            display="inline",
                            children=[
                                modules.ModelList(
                                    title=u'Управление постами',
                                    models = ('pybb.models.Post',)
                                    ),

                                StaticticsTextModule(
                                title=u'Статистика постов',
                                stats=aggregate_stats_text(
                                    pybb.models.Post, "created"
                                    )
                                ),

                                    ]
                                    ),
                        ]
                    ),
        )
#-----------------------------------------------------------------------------

#БЛОК "БИБЛИОТЕКА"
        self.children.append(modules.Group(
            title=u"Библиотека",
            display="tabs",
            children=[
                        modules.Group(
                            title=u'Секции',
                            display="inline",
                            children=[
                                modules.ModelList(
                                    title=u'Управление секциями',
                                    models = ('library.models.LibSection',)
                                    ),
                                    ]
                                    ),

                        modules.Group(
                            title=u'Записи',
                            display="inline",
                            children=[
                                modules.ModelList(
                                    title=u'Управление записями',
                                    models = ('library.models.LibItem',)
                                    ),
                                    ]
                                    ),

                        modules.Group(
                            title=u'Библиотечные файлы',
                            display="inline",
                            children=[
                                modules.ModelList(
                                    title=u'Управление файлами',
                                    models = ('library.models.ItemFile',)
                                    ),
                                    ]
                                    ),
                        ]
                    ),
        )



class CustomAppIndexDashboard(AppIndexDashboard):
    title = u'Привет мир!'

    def __init__(self, *args, **kwargs):
        AppIndexDashboard.__init__(self, *args, **kwargs)

        # append a model list module and a recent actions module
        self.children += [
            modules.ModelList(self.app_title, self.models),
            modules.RecentActions(
                _(u'Последние действия'),
                include_list=self.get_app_content_types(),
                limit=5
            )
        ]

    def init_with_context(self, context):
        return super(CustomAppIndexDashboard, self).init_with_context(context)
