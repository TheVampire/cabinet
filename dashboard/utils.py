import datetime

from django.contrib.auth.models import User

from qsstats import QuerySetStats


def aggregate_stats_text(model, parametr):

    queryset = model.objects.all()
    qs = QuerySetStats(
        queryset, parametr)

    return {
            "today": qs.this_day(),
            "thisweek": qs.this_week(),
            "thismonth": qs.this_month(),
            "untilnow": qs.until_now(),
            }


def aggregate_stats_graph(model, parametr):

    qs = model.objects.all()
    qss = QuerySetStats(qs, parametr)

    today = datetime.date.today()
    seven_days_ago = today - datetime.timedelta(days=7)

    return qss.time_series(seven_days_ago, today)
