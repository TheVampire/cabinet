from admin_tools.dashboard import modules


class StaticticsTextModule(modules.DashboardModule):
    def is_empty(self):
        return self.stats == {}

    def __init__(self, **kwargs):
        super(StaticticsTextModule, self).__init__(**kwargs)
        self.template = 'my_blocks/statistics_text.html'
        self.stats = kwargs.get("stats")


class StaticticsGraphModule(modules.DashboardModule):
    def is_empty(self):
        return self.stats == {}

    def __init__(self, **kwargs):
        super(StaticticsGraphModule, self).__init__(**kwargs)
        self.template = 'my_blocks/statistics_graph.html'
        self.stats = kwargs.get("stats")
