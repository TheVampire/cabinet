#coding: UTF-8

from django.db import models
from expert.models import Consultation

default_sender = "+79021100362"

class SMSConsultationNotification(models.Model):
    consultation = models.ForeignKey(Consultation, verbose_name=u'Консультация')
    send_id = models.CharField(verbose_name=u'Идентификатор отправленного сообщения', max_length=32)

class SMS(models.Model):

    class Meta:
        verbose_name = u"sms сообщение"
        verbose_name_plural = u"sms сообщения"

    sender = models.CharField(max_length=255)
    reciever = models.TextField()

    text = models.TextField()
