from django.contrib.admin import site, ModelAdmin

from models import SMS, SMSConsultationNotification


class SMSAdmin(ModelAdmin):

    list_display = ("sender", "reciever", "text")


site.register(SMS, SMSAdmin)
site.register(SMSConsultationNotification)
