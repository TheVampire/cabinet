# coding: utf-8
from django.views.generic.base import View
from django.http import HttpResponse
from utils import send_message
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.utils import timezone
from models import SMSConsultationNotification, Consultation
from core.utils import get_or_none
import pytils
from django.core.mail import send_mail

class SendMessage(View):

    def post(self, request):
        pass

import logging
logging.basicConfig(filename="/tmp/basic.log", level=logging.DEBUG)
class ResponseBySMS(View):
    def get(self, request):
        return HttpResponse(
            'OK'
        )
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ResponseBySMS, self).dispatch(*args, **kwargs)

    def post(self, request):
        logging.debug(u'Ответ по SMS - {}'.format(request.POST.get('id')))
        if  u'да' in request.POST.get('text').lower():
            if len(request.POST.get('text').lower()) > 2:
                c = Consultation.objects.get(pk = int(request.POST.get('text').lower().split(u'да')[1]))
                msg =\
u"""Здравствуйте.

Специалист {} {} подтвердил заказанную консультацию.
Дата и время: {}
Продолжительность: {} мин

Пожалуйста, войдите в свой личный кабинет на сайте и перейдите в раздел “Моя консультация” в назначенное время.

Служба поддержки Кабинета №1
support@kabinet1.ru
www.kabinet1.ru""".format(c.expert.first_name, c.expert.last_name,
                    pytils.dt.ru_strftime(u"%d %B %Y в %H:%M", date=timezone.localtime(c.dtstart), inflected=True),
                    c.length)
                send_mail(u'Кабинет №1 - Консультация подтверждена', msg, u'Кабинет №1 <support@kabinet1.ru>', [c.client.email])
                send_mail(u'Кабинет №1 - Консультация подтверждена', msg, u'Кабинет №1 <support@kabinet1.ru>', ['info@kabinet1.ru'])
                c.status=u'approved'
                c.save()
        if u'нет' in request.POST.get("text").lower():
            if len(request.POST.get('text').lower()) > 3:
                c = Consultation.objects.get(pk = int(request.POST.get('text').lower().split(u'нет')[1]))
                msg =\
u"""Здравствуйте.

Специалист {} {} не подтвердил заказанную консультацию.
Дата и время: {}
Продолжительность: {} мин

Вы можете попробовать выбрать другое время для консультации с этим специалистом либо выбрать другого специалиста на нашем сайте.

Служба поддержки Кабинета №1
support@kabinet1.ru
www.kabinet1.ru""".format(c.expert.first_name, c.expert.last_name,
                    pytils.dt.ru_strftime(u"%d %B %Y в %H:%M", date=timezone.localtime(c.dtstart), inflected=True),
                    c.length)
                msg += u'Специалист отменил консультацию\n' + msg
                send_mail(u'Кабинет №1 - Консультация отменена', msg, u'Кабинет №1 <support@kabinet1.ru>', [c.client.email])
                c.status = u'canceled'
                c.save()
        return HttpResponse("OK")

sms_response_view = ResponseBySMS.as_view()